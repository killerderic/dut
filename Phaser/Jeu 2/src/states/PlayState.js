class PlayState extends Phaser.State{

	create(){

        this.game.stage.backgroundColor = '#787878'

		this.map = this.game.add.tilemap('map-json');

		this.map.addTilesetImage('tileSet', 'map-tileSet1');

        this.map.ground = this.map.createLayer('ground');
        this.map.setCollision([1, 2], true, this.map.ground);

		player = this.game.add.sprite(this.game.world.centerX-256, this.game.world.centerY-256, 'dude');
		this.game.physics.arcade.enable(this.player);
		this.player.body.collideWorldBounds = true;

		this.cursors = this.game.input.keyboard.createCursorKeys();

		this.game.camera.follow(player);
	}

    update(){

    	this.game.physics.arcade.collide(this.player, this.map.ground);

    	this.player.body.velocity.x = 0;
        this.player.body.velocity.y = 0;

    	if (this.cursors.left.isDown)
    	{
        	this.player.body.velocity.x = -150;

    	}
    	else if (this.cursors.right.isDown)
    	{
        	this.player.body.velocity.x = 150;

    	}else if (this.cursors.up.isDown)
    	{
        	this.player.body.velocity.y = -150;

    	}else if (this.cursors.down.isDown)
    	{
        	this.player.body.velocity.y = 150;
	    }
    }
}

export default PlayState;