class LoadState extends Phaser.State{
	
	preload(){
		
		this.game.load.spritesheet('dude', 'image/dude.png', 20, 20);
	    this.game.load.tilemap('map-json', 'map/mapAdam.json', null, Phaser.Tilemap.TILED_JSON);
	    this.game.load.image('map-tileSet', 'map/grass.png');
	}

	create(){
    	this.game.state.start('play');

	}
}

export default LoadState;