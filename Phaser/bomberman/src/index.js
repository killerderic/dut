import BootState from './states/BootState';
import LoadState from './states/LoadState';
import PlayState from './states/PlayState';

class Game extends Phaser.Game {

	constructor() {
		super(990, 990, Phaser.AUTO, '', null);
		this.state.add('boot', BootState, false);
		this.state.add('load', LoadState, false);
		this.state.add('play', PlayState, false);
		this.state.start('boot');
	}

}

new Game();
