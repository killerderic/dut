class LoadState extends Phaser.State{
	
	preload(){

	    this.game.load.atlasJSONHash('bomberman', 'images/bomberman/sprite-bomberman.png', 'images/bomberman/sprite-bomberman.json');
	    this.game.load.atlasJSONHash('bomb', 'images/bomb/bomb-sprite-sheet.png', 'images/bomb/bomb-sprite-sheet.json');
	    this.game.load.atlasJSONHash('flame', 'images/flame/flame-spritesheet.png', 'images/flame/flame-spritesheet.json');
	    this.game.load.image('background', 'images/block/BackgroundTile.png');
	    this.game.load.tilemap('map-json', 'map/map.json', null, Phaser.Tilemap.TILED_JSON);
	    this.game.load.image('map-tileSet', 'map/tileSet.png');


	}

	create(){
    	this.game.state.start('play');

	}
}

export default LoadState;
