import Bomberman from '../objects/Bomberman';

class PlayState extends Phaser.State{

    create(){

        /**
         * ---------------------------
         *           THE MAP
         *----------------------------
         */

        // First get the tiled map
        this.map = this.game.add.tilemap('map-json');
        // Then set up the tiled map with the map's images (tiles)
        this.map.addTilesetImage('tileSet', 'map-tileSet');

        // Create and display the main and only layer
        this.map.background = this.map.createLayer('background');
        this.map.ground = this.map.createLayer('ground');
        // ground.debug = true; Enable this to show collision

        // Set up collision of blocks.
        this.map.setCollision([1, 2], true, this.map.ground);

        // Set up bomberman
        this.bomberman = new Bomberman(this.game, this.map, 125, 175);

    }

    
    update(){

        this.game.physics.arcade.collide(this.bomberman, this.map.ground);

        this.bomberman.update();

        for (var i = 0; i < this.bomberman.bombs.length; i++){
            this.bomberman.bombs[i].update();

            for(var j = 0; j <= this.bomberman.bombs[i].flames.length-1; j++){
                this.bomberman.bombs[i].flames[j].update();
                if(this.game.physics.arcade.overlap(this.bomberman.bombs[i].flames[j], this.bomberman)){
                    this.bomberman.takeDamage();
                }
            }
        }
    }
}

export default PlayState;
