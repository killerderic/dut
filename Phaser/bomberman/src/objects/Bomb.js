class Bomb extends Phaser.Sprite{

  constructor(game, map, x, y){

    super(game, (((map.getTileWorldXY(x,y).x+1)*2)-1) * BLOCK_SIZE/2, (((map.getTileWorldXY(x,y).y+1)*2)-1) * BLOCK_SIZE/2, 'bomb');

    this.game = game;
    this.map = map;
    this.lifetime = 3;
    this.lastTickDate = new Date();

    this.game.physics.arcade.enable(this);
    this.anchor.setTo(.5, .5);
    this.game.add.existing(this);

    this.flames = [];

  }

  explode(){

      // Pop flames
      // check the tiles around the bomb
      var tiles = [];
      var tile = this.map.getTileWorldXY(this.x,this.y);
      tiles.push(tile);

      var tileA = this.map.getTileAbove(this.map.ground.index, tile.x, tile.y);
      if(tileA === null){
          tileA = this.map.getTileAbove(this.map.background.index, tile.x, tile.y);
      }
      tiles.push(tileA);

      var tileB = this.map.getTileBelow(this.map.ground.index, tile.x, tile.y);
      if(tileB == null){
          tileB = this.map.getTileBelow(this.map.background.index, tile.x, tile.y);
      }
      tiles.push(tileB);

      var tileR = this.map.getTileRight(this.map.ground.index, tile.x, tile.y);
      if(tileR == null){
          tileR = this.map.getTileRight(this.map.background.index, tile.x, tile.y);
      }
      tiles.push(tileR);

      var tileL = this.map.getTileLeft(this.map.ground.index, tile.x, tile.y);
      if(tileL == null){
          tileL = this.map.getTileLeft(this.map.background.index, tile.x, tile.y);
      }
      tiles.push(tileL);

      for(var i=0; i <= tiles.length-1; i++){
          if(tiles[i].index != 1){
              this.map.removeTile(tiles[i].x, tiles[i].y, this.map.ground);
              var flame = new Flame(this.game, (tiles[i].x*BLOCK_SIZE)+BLOCK_SIZE/4, (tiles[i].y*BLOCK_SIZE)+BLOCK_SIZE/4);
              this.flames.push(flame);
          }
      }

      // Destroy the bomb
      //var index = bombs.indexOf(this);
      this.destroy();
  }

  tick(){

      this.lifetime -= 1;
      this.lastTickDate = new Date();
      this.frame += 1;
  }

  update(){

      // Get the current date
      var currentDate = new Date();
      // Get the date of the ancient tick
      var oldDate = this.lastTickDate;
      // do the difference
      var elapsed = currentDate.getTime() - oldDate.getTime();
      if(elapsed > BOMB_TICK_DELAY){
          this.tick();
          if(this.lifetime == 0){
              this.explode();
          }
      }
  }

}

export default Bomb;
