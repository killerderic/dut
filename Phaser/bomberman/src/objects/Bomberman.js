class Bomberman extends Phaser.Sprite{

    constructor(game, map, x, y){

      super(game, x, y, 'bomberman');

      this.map = map;
      this.life = 2;
      this.lastDamageTakenDate = new Date();

      this.lastPutBombDate = new Date(); // Care, kind of hack ( can not put bomb instantly after being create )

      this.game = game;
      this.game.add.existing(this);
      this.game.physics.arcade.enable(this);
      this.anchor.setTo(.5, 1);

      this.animations.add('side', [
          'Bman_S_f01.png',
          'Bman_S_f02.png',
          'Bman_S_f03.png',
          'Bman_S_f04.png',
          'Bman_S_f05.png',
          'Bman_S_f06.png',
          'Bman_S_f07.png'
      ], 30, true, false);

      this.animations.add('up', [
          'Back0.png',
          'Back1.png',
          'Back2.png',
          'Back3.png',
          'Back4.png',
          'Back5.png',
          'Back6.png',
          'Back7.png'
      ], 30, true, false);

      this.animations.add('down', [
          'Bman_F_f00.png',
          'Bman_F_f01.png',
          'Bman_F_f02.png',
          'Bman_F_f03.png',
          'Bman_F_f04.png',
          'Bman_F_f05.png',
          'Bman_F_f06.png',
          'Bman_F_f07.png'
      ], 30, true, false);

      this.bombs = [];

      /**
       * --------------------------------
       *        The controls
       *---------------------------------
       */
      this.cursors = this.game.input.keyboard.createCursorKeys();
      this.spacebar = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    }

    putBomb(){
      // Get the actual date
      var actualDate = new Date();
      var oldDate = this.lastPutBombDate;
      var elapsed = actualDate.getTime() - oldDate.getTime();
      if(elapsed > Properties.getBombCd()){
          var bomb =  new Bomb(this.game, this.map, this.x, this.y);
          this.bombs.push(bomb);
          this.lastPutBombDate = actualDate;
      }

    }

    takeDamage(){
        var actualDate = Date.now();
        var elapsed = actualDate - this.lastDamageTakenDate;
        if(elapsed > DAMAGE_TAKEN_DELAY){
            this.life -= 1;
            this.lastDamageTakenDate = actualDate;
            console.log("damage taken. Life :");
            console.log(this.life);
        }
    }

    update(){

        // Reset movement
        this.body.velocity.x = 0;
        this.body.velocity.y = 0;

        if(this.cursors.left.isDown){
            this.body.velocity.x = -220;
            this.scale.x = -1;
            this.animations.play('side');
        }
        else if(this.cursors.right.isDown){
            this.body.velocity.x = 220;
            this.scale.x = 1;
            this.animations.play('side');
        }
        else if(this.cursors.up.isDown){
            this.body.velocity.y = -220;
            this.animations.play('up');
        }
        else if(this.cursors.down.isDown){
            this.body.velocity.y = 220;
            this.animations.play('down');
        }
        else{
            this.animations.stop();
            this.frame = 9;
        }

        if(this.spacebar.isDown){
            this.putBomb();
        }
    }
}

export default Bomberman;
