class Flame extends Phaser.Sprite{

  constructor(game, x, y){
    super(this, game, x ,y, 'flame');

    this.creationDate = Date.now();

    game.add.existing(this);
    game.physics.arcade.enable(this);
  }

	update = function(){
	  var currentDate = Date.now();
	  var elapsed = currentDate - this.creationDate;
	  if(elapsed > FLAME_LIFETIME){
	      this.destroy();
	  }

	}
}
