class Properties{
  constructeur(){

    this.properties = {
      "PUT_BOMB_CD" : 1000
    };
    this.PUT_BOMB_CD = 1000;
    this.BOMB_TICK_DELAY = 1000;
    this.BLOCK_SIZE = 90;
    this.BLOCK_NBR = 11; // 11*90 = 990
    this.FLAME_LIFETIME = 1000;
    this.DAMAGE_TAKEN_DELAY = 1000;
  }

  static get(propertyName){
    return this.properties[propertyName];
    throw new Exception("Property " + propertyName + " does not exist");
  }
}
