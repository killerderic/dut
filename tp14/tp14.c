void affMonome(Monome m){
    printf("%dx^%d", m.coeff, m.degre);
}

Polynome insereEnTete(Polynome p, Monome m){

    Maillon * ml = (Maillon * ) malloc(sizeof(Maillon));
    if(ml == NULL){
        printf("Prb malloc \n");
    }
    ml->monome = m;

    ml->suivant = p;
    return ml;
}
/*
Polynome insereEnTete(Polynome p, Monome m){
    Polynome temp =p;

    p = (Maillon * ) malloc(sizeof(Maillon));
    if(p == NULL){
        printf("Prb malloc \n");
    }
    p->monome = m;

    p->suivant = temp;
    return p;
}*/
Polynome insererCroissant(Polynome p, Monome m){
    if(p == NULL) return insereEnTete(p,m);
    if(m.degre >= p->monome.degre){
        return insereEnTete(p, m);
    }
    p->suivant = insererCroissant(p->suivant, m);
    return p;
}

void affPolynome(Polynome p){
    if(p == NULL) return;
    affMonome(p->monome);
    affPolynome(p->suivant);
}
