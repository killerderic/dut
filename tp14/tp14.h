#ifndef TP14_H_INCLUDED
#define TP14_H_INCLUDED

typedef struct{
    int coeff;
    int degre;
}Monome;

typedef struct test{
    Monome monome;
    struct test * suivant;
}Maillon, * Polynome;

void affMonome(Monome m);
Polynome insereEnTete(Polynome p, Monome m);
void affPolynome(Polynome p);
#endif // TP14_H_INCLUDED
