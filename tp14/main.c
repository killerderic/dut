#include <stdio.h>
#include <stdlib.h>
#include "tp14.h"
#include "tp14.c"

int main()
{
    Polynome p = NULL;
    Monome m, m2, m3;
    m.coeff = 2;
    m.degre = 3;
    p = insereEnTete(p, m);

    m2.coeff = 5;
    m2.degre = 4;
    p = insererCroissant(p,m2);

    m3.coeff = 9;
    m3.degre = 2;
    p = insererCroissant(p,m3);
    affPolynome(p);
    return 0;
}
