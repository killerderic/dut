xdel(winsid());                 // ferme toutes les fenetres a chaque nouvel appel du script
clear;                         // nettoie toutes les variables a chaque nouvel appel du script

exec("tp3_fonctions.sci", -1);

//Exercice 1 :

if %f then
    //1)
    // w1(cos(theta), sin(theta))
    // w2(-sin(theta), cos(theta))
    V=[ 1 0 -1 0 0 -1 1; 0 1 0.5 0 -1 -2 -2];

    theta=%pi/3;
    A=[cos(theta) -sin(theta); sin(theta) cos(theta)];
    W=A*V;
    dessine_graphe(W,1);
    dessine_grille(V(1:2,1:2));
    
    //2)
    figure();
    theta = linspace(0,2*%pi,50);// 50 valeurs d’angle entre 0 et 2pi
    for t = theta
    drawlater() ; // retient l’affichage jusqu’à la commande "draw
    clf();
    // nettoie la figure
    a = gca();
    // récupère les axes actuels
    a.data_bounds = [-3 -3; 3 3] ; // bornes de représentation
    a.grid = [1 1];               // trace une grille
    A=[cos(t) -sin(t); sin(t) cos(t)];
    W=A*V;
    dessine_graphe(W,1); 
    dessine_grille(V(1:2,1:2));
    drawnow();                       // affiche le dessin, tout d’u
    xpause(50000);                   // petite pause (en microsec)
    end
end

//Exercice 2 :

if %f then
    V=[ 1 0 -1 0 0 -1 1 0 -1 1; 0 0 0 0 0 0 0 2 2 2; 0 1 0.5 0 -1 -2 -2 -1 -2 -2];
    E=[1 0 0; 0 0 1; 0 1 0];
    dessine_graphe(V,2);
    dessine_grille(E);
    
    figure();
    Ba=[ 1 0 1; 0 0 2; 0 1 -2];
    dessine_graphe(V,2);                    //Constitue une base car elle recouvre tout les points dans l'espace 3D
    dessine_grille(Ba);
    
    
    figure();
    Bb=[ 1 0 -1; 0 0 0; 0 1 0.5];
    dessine_graphe(V,2);                    //Ne constitue pas une base car ne recouvre pas tout l'espace 3D
    dessine_grille(Bb);
    
    figure();
    Bc=[ 1 0 1; 0 0 2; 0 0 -2];
    dessine_graphe(V,2);                    //Ne constitue pas une base car ne recouvre pas tout l'espace 3D
    dessine_grille(Bc);
end

//Exercice 3 :

if %f then
    //1) Elle doit avoir une taille de 2x3
    //2)
    V=[ 1 0 -1 0 0 -1 1 0 -1 1; 0 0 0 0 0 0 0 2 2 2; 0 1 0.5 0 -1 -2 -2 -1 -2 -2];
    P=[1 0 0;0 0 1];
    W=P*V;
    figure();
    dessine_graphe(W,2);
    //3)
    P=[0 1 0;0 0 1];
    W=P*V;
    figure();
    dessine_graphe(W,2);
    
    //4)
    P=[1 0 0;0 1 0];
    W=P*V;
    figure();
    dessine_graphe(W,2);
    //5) 
    
end

//Exercice 4 :

if %t then
    V=[ 1 0 -1 0 0 -1 1 0 -1 1; 0 0 0 0 0 0 0 2 2 2; 0 1 0.5 0 -1 -2 -2 -1 -2 -2];
    B=1/3*[1 -2 -2; 2 2 -1; 2 -1 2];
    dessine_graphe(V,2);
    dessine_grille(B);
    //C'est donc une base orthonormé car il y a une longueur de 1 et des angles droits, mais positionés dans une orientation différente.
    
    P= [0 1 0; 0 0 1]
    //Car la projection doit se faire parallèlement au vecteur (1;2;2).
    
    figure()
    C=P*inv(B)
    W=C*V
    dessine_graphe(W,2)
    disp W
    
    
