

// ========================================================================
//
//                FONCTIONS A UTILISER DANS LE TP2 ET TP3
//
// ========================================================================

// Afin de simplifier la lecture, les fonctions définies ici sont "vides".
// Le but est uniquement de présenter l'utilisation requise de chaque fonction.
// Les "vraies" fonctions sont définies dans la partie inférieure de ce fichier, 
// qu'il n'est pas nécessaire de regarder en détail (sauf par curiosité!).



function dessine_graphe(Sommets, Aretes, couleur, epaisseur, rayon)
    // ========================================================================
    //    Représentation graphique d'un graphe vectoriel (de type bonhomme)
    //
    // ARGUMENTS OBLIGATOIRES :
    // Sommets : matrice de taille (2,N), N étant le nombre total de sommets.
    //              => Sommets(:,i) donne les coordonnées du i-ème sommet.
    // Aretes : Définition de la structure d'arêtes entre les sommets
    //              => Aretes = 1 pour représenter le bonhomme
    //              => Aretes = 2 pour représenter le centaure
    //              => Cas général: Aretes = matrice booléenne de taille (N,N)
    //
    // ARGUMENTS OPTIONNELS
    // couleur : (nom de la) couleur à utiliser
    // epaisseur : epaisseur des aretes
    // rayon : rayon du cercle dessiné sur chaque sommet (uniquement en 2D).
    //              => Faire rayon = [] pour ne pas dessiner de cercle
    //
    // Remarque -- le code fonctionne pour des coordonnées 2D et 3D.
    // ========================================================================
    
    // Valeurs par défaut pour les arguments optionnels
    nbarguments = argn(2);
    if nbarguments < 3 then
        couleur = "red";
    end
    if nbarguments < 4 then
        epaisseur = 5;
    end
    if nbarguments < 5 then
        rayon = 10;
    end
    dessine_graphe_CODE(Sommets, Aretes, couleur, epaisseur, rayon);
endfunction


// ========================================================================
// ========================================================================


function dessine_grille(Base, Nrep, use_existing_bounds)
    // ========================================================================
    //       Trace une grille de coordonnées sur une base de vecteurs
    //
    // Base : matrice stockant les vecteurs de base
    //        => Base(:,i) donne les coordonnées du i-ème vecteur de base
    //
    // ARGUMENTS OPTIONNELS
    // Nrep : matrice de format [min1,max1 ; min2,max2 (; min3,max3)] 
    //        donnant le nombre de répétitions de la grille dans chaque dimension
    //        (Laisser vide pour un choix automatique)
    // use_existing_bounds : BOOLEEN
    //        %t pour couper le dessin aux bornes de représentation préexistantes
    //        par défaut : gestion automatique du paramètre
    //      
    // Remarque -- le code fonctionne pour des coordonnées 2D et 3D.
    // ========================================================================

    // Valeur par défaut pour Nrep
    if argn(2) < 2 then
        Nrep = [] ;
    end
    autoNrep = isempty(Nrep) ;
    if autoNrep
        Nrep = get_Nrep(Base);                  // essaye de définir Nrep de manière automatique
    end
    // Valeur par défaut pour use_existing_bounds :
    // FAUX, sauf si on a pu déterminer Nrep de façon automatique
    if argn(2) < 3
        use_existing_bounds = autoNrep & ~isempty(Nrep);
    end
    // En désespoir de cause, valeur par défaut pour Nrep:
    if isempty(Nrep) then                
        Nrep = [-1 1; -1 1; -1 1] ;
    end

    dessine_grille_CODE(Base, Nrep, use_existing_bounds);
endfunction





// ========================================================================
// ========================================================================
// ========================================================================
// ========================================================================
// ========================================================================
// ========================================================================
// ========================================================================
// ========================================================================

// ci-dessous, le code des fonctions (pas la peine de regarder en détail)





function dessine_graphe_CODE(Sommets, Aretes, couleur, epaisseur, rayon)

    // Raccourci pour les structures d'arêtes utilisées dans le TP
    select Aretes
    case 1 then     // Bonhomme
        Aretes = %f *ones(7,7);
        Aretes(4,1:5) = %t;
        Aretes(5,6:7) = %t;    
    case 2 then     // Centaure
        Aretes = %f *ones(10,10);
        Aretes(4,1:5) = %t;
        Aretes(5,6:8) = %t;
        Aretes(8,9:10) = %t;
    end

    // Constantes
    N = size(Sommets, 2);   // nombre de sommets
    D = size(Sommets, 1);   // dimensionalité
    ray = .01;              // Facteur de conversion pour le rayon (choix arbitraire)

    // S'assurer que le dessin sera fait en entier sans s'effacer au fur et à mesure
    a = gca();              // axes utilisés pour le dessin
    ac = a.auto_clear;      // savegarde la propriété, pour la rétablir à la fin
    firstplot = %t ;        // indicateur du premier appel à la fonction plot

    // Trace les arêtes
    Aretes = Aretes | Aretes' ;     // symétrise la matrice
    for i = 1:N
        for j = i+1:N
            if Aretes(i,j) then
                if D == 2 then
                    plot(Sommets(1,[i j]), Sommets(2,[i j]), "linewidth", epaisseur, "color", couleur);
                elseif D == 3 then
                    // param3d(Sommets(1,[i j]), Sommets(3,[i j]), Sommets(2,[i j]));
                    param3d(Sommets(1,[i j]), Sommets(2,[i j]), Sommets(3,[i j]));
                    e = gce() ;
                    e.foreground = color(couleur) ;
                    e.thickness = epaisseur ;
                else
                    error("Mauvaise dimension");
                end
                if firstplot then
                    set(gca(),"auto_clear","off");
                    firstplot = %f ;
                end
            end
        end
    end

    // Trace les sommets
    if ~isempty(rayon)  

        if D == 2 then      // VERSION 2D : petits cercles, à la main :(
            theta = linspace(0, 2*%pi, 20);
            ct = cos(theta);
            st = sin(theta);
            for i= 1:N
                xx = Sommets(1,i) + rayon*ray*ct ;
                yy = Sommets(2,i) + rayon*ray*st ;
                xfpoly(xx, yy);
                e = gce();
                e.background = color(couleur) ;
                e.foreground = color(couleur) ;
                if firstplot then
                    set(gca(),"auto_clear","off");
                    firstplot = %f ;
                end
            end

        elseif D == 3 then   // VERSION 3D : petites sphères, à la main :(
            function [x,y,z] = polarcoord(phi,theta)
                z = cos(theta);
                st = sin(theta);
                x = st.*cos(phi);
                y = st.*sin(phi);
            endfunction
            p1 = linspace(0, 2*%pi, 20);
            p2 = linspace(0, %pi, 20);
            [Xf,Yf,Zf] = eval3dp(polarcoord,p1,p2);
            for i= 1:N
                xx = Sommets(1,i) + rayon*ray*Xf ;
                yy = Sommets(2,i) + rayon*ray*Yf ;
                zz = Sommets(3,i) + rayon*ray*Zf ;
                plot3d(xx, yy, zz);
                e = gce();
                e.color_mode = color(couleur) ;
                e.foreground = color(couleur) ;
                if firstplot then
                    set(gca(),"auto_clear","off");
                    firstplot = %f ;
                end
            end
        end
    end
    
    a.auto_clear = ac;      // rétablit la propriété initiale
    a.isoview = "on";       // Impose une représentation isométrique
    
endfunction



// ========================================================================
// ========================================================================



function dessine_grille_CODE(Base, Nrep, use_old_bounds)

    // Récupère les constantes
    D = size(Base, 1);   // dimensionalité
    N = size(Base, 2);   // nombre de vecteurs

    // Vérifie qu'il s'agit bien d'une base
    isBase = %t ;
    if N ~= D then
        messagebox(["Il n''y a pas le bon nombre de vecteurs pour faire une base!" ...
        "Je trace quand même la grille, à des fins illustratives."], "AVERTISSEMENT");
    elseif abs(det(Base)) < 1e-6 then
        messagebox(["La famille ne forme pas une base car les vecteurs sont LIÉS." ...
        "Je trace quand même la grille, à des fins illustratives."], "AVERTISSEMENT");
    end

    // Gestion de l'axe
    a = gca();                  // axes utilisés pour le dessin
    ac = a.auto_clear;          // savegarde la propriété, pour la rétablir à la fin
    firstplot = %t ;            // indicateur du premier appel à la fonction plot
    old_bounds = a.data_bounds; // bornes de représentation préexistantes
    
    // Sous-fonction : tracé d'une ligne de la grille (suivant la dimension)
    function plotsegment(seg, col)
        select D
        case 2 then
            plot( seg(1,:), seg(2,:));
            e =gce();
            e = e.children ;    // arf
        case 3 then
            param3d(seg(1,:), seg(2,:), seg(3,:));
            e = gce();
        else
            error("Uniquement en dimension 2 et 3");
        end
        e.line_style = 9;               // style pointilĺé
        e.foreground = color(col);      // couleur (indice)
        e.thickness = 3;                // épaisseur
        if firstplot then
            a.auto_clear = "off";
            firstplot = %f ;
        end
    endfunction
    
    // Trace la grille
    grids = list();
    for i=1:size(Nrep,1)
        grids(i) = Nrep(i,1):Nrep(i,2);
    end
    // Couleurs utilisees
    cols =list() ;
    cols(1)="blue"; cols(2)="darkgreen"; cols(3)="maroon";

    select N
    case 1 then     // 'grille' de 1 vecteur
        seg = Base(:,1)*Nrep(1,:);
        plotsegment(seg, cols(1));

    case 2 then     // grille de 2 vecteurs            
        for i = 1:2
            rem = 3-i;
            for nx = grids(rem)
                seg = Base(:,rem)*[nx nx] + Base(:,i)*Nrep(i,:);
                plotsegment(seg, cols(i));
            end
        end

    case 3 then     // grille de 3 vecteurs
        for i = 1:3
            rem = setdiff(1:3, i);
            for nx = grids(rem(1))
                for ny = grids(rem(2))
                    seg = Base(:,rem(1))*[nx nx] + Base(:,rem(2))*[ny ny] + Base(:,i)*Nrep(i,:);
                    plotsegment(seg, cols(i));
                end
            end
        end

    else
        error("Grille: Uniquement des familles à 1, 2 ou 3 vecteurs");
    end
    
    // Revient aux propriétés d'axe souhaitées
    a.auto_clear = ac;      // rétablit la propriété initiale
    if use_old_bounds then
        a.data_bounds = old_bounds ;
    end
    a.isoview = "on";       // Impose une représentation isométrique
endfunction




// ========================================================================
// ========================================================================



//  Calcul automatique du nombre de répétitions d'une grille
// ==========================================================
// Récupère les bornes existantes dans l'axe courant, et définit Nrep en fonction

function Nrep = get_Nrep( Base )

    // Récupère les constantes
    D = size(Base, 1);   // dimensionalité
    N = size(Base, 2);   // nombre de vecteurs
    isBase = %t ;
    if N ~= D then
        isBase = %f ;
    elseif abs(det(Base)) < 1e-6 then
        isBase = %f ;
    end

    a = gca();                        // Axes courants
    bounds = a.data_bounds;           // [xmin,ymin(,zmin) ; xmax,ymax(,zmax)]
    
    // Laisse tomber si les axes sont vides, ou bien si 3D versus 2D
    if isempty(a.children) | size(bounds,2) ~= D then
        Nrep = [];
        return
    end

    // Carré (ou cube) de représentation
    if D == 2 then            // 2D, carré de représentation
        cube = zeros(2,4);
        count = 1;
        for x = 1:2
            for y = 1:2
                cube(:,count) = [bounds(x,1); bounds(y,2)] ;
                count = count+1 ;
            end
        end     
    elseif D == 3 then        // 3D, cube de représentation
        cube = zeros(3,8);
        count = 1;
        for x = 1:2
            for y = 1:2
                for z = 1:2
                    cube(:,count) = [bounds(x,1); bounds(y,2); bounds(z,3)] ;
                    count = count+1 ;
                end
            end
        end
    else
        error("Mauvais nombre de dimensions");
    end

    if isBase then
        // Cube transformé par la base : récupère le nombre de répétitions nécessaires
        cube_trans = Base \ cube ;                                          // (D, 2^D)
        Nrep = [floor(min(cube_trans,'c')), ceil(max(cube_trans,'c'))];     // (D, 2)

    else    
        // Solution alternative : cherche Nrep pour chaque vecteur indépendamment
        mod = warning('query');         // supprime temporairement les warnings
        Nrep = zeros(N,2);
        for i = 1:N
            cube_trans = pinv(Base(:,i)) * cube ;                                  // (1, 2^D)
            Nrep(i,:) = [floor(min(cube_trans,'c')), ceil(max(cube_trans,'c'))];   // (1, 2)
        end
        warning(mod);                       // remet le niveau de warning précédent
    end
endfunction


