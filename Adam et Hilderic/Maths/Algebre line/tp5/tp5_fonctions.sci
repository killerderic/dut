// Fichier de fonctions Scilab (.sci) pour le TP5



// ========================================================================
//          Construire une matrice de convolution
//
// PremiereLigne est la première ligne requise pour la matrice.
// Cette ligne est répétée sur toutes les lignes suivantes, en décalant à
// chaque fois la ligne d'un cran vers la droite (circulairement).
// ========================================================================

function M = build_conv_mat( PremiereLigne )
    N = length(PremiereLigne);
    M = zeros(N,N) ;
    for i = 1:N
        M(i,:) = [PremiereLigne($-i+2:$) PremiereLigne(1:$-i+1)];
    end
endfunction


// ========================================================================
//                      Visualiser un signal 1D
// ========================================================================


function show_signal( Signal , couleur )
    // Choix de couleur par défaut
    if argn(2) < 2 then
        couleur = "red" ;
    end
    drawlater();
    // Trace
    plot(Signal, "color", couleur, "linewidth", 3) ;
    // Tranforme en créneau
    e=gce();
    e.children(1).polyline_style=2;
    // Aspect de la figure
    f = gcf();
    f.axes_size = [1000 220] ;
    // Propriétés des axes
    a = gca() ;
    a.tight_limits = "on" ;
    // Affiche
    drawnow();
endfunction



// ========================================================================
//                      Visualiser une image
//
// L'image doit être constituée de nombres de type 'double'
// OPTIONNEL :
// - On peut préciser la valeur valmin (correspondant à la couleur noire)
//       et la valeur valmax (correspondant à la couleur blanche).
// - Par défaut, le choix de 'valmin' et 'valmax' est automatique.
// ========================================================================


function show_image( Im, valmin, valmax )
    // Les axes des X et des Y
    X = 1 : size(Im, 1) ;
    Y = 1 : size(Im, 2) ;
    // Récupère les bornes si nécessaire
    if argn(2) < 3 then
        valmin = min(Im) ;
        valmax = max(Im) ;
    else
        Im = max(Im, valmin);
        Im = min(Im, valmax);
    end
    // Renormalise l'image
    if valmax <> valmin then
        Im = round(255* (double(Im) - valmin) / (valmax - valmin) );
    end
    // Trace l'image
    drawlater();
    clf;
    grayplot(X, Y, Im' ) ;
    // Change la colorbar
    f = gcf();
    f.color_map = graycolormap(256);
    // Propriétés des axes
    a = gca() ;
    a.isoview = "on" ;
    a.x_location = "top" ;
    a.y_location = "left" ;
    a.tight_limits = "on" ;
    a.axes_reverse = ["off" "on" "off"] ;
    // Affiche
    drawnow();
endfunction



// ========================================================================
//    Représentation du contenu d'une matrice M (idem TPs précédents)
// ========================================================================

function my_Matplot(M)
    f = gcf();
    // Contenu de la matrice
    mn = min(M); mx = max(M);
    f.color_map = jetcolormap(102);
    colorbar(mn,mx) ;
    Matplot(flipdim( ceil(100*(M-mn+.000001)/(mx-mn)) , 1));
    // Propriétés de axes
    a = gca();
    a.axes_reverse=["off","on","off"];
    a.box = "on";
    a.x_location = "top";
endfunction

