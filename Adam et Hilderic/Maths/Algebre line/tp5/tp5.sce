
// Deux lignes utiles à mettre au début de chaque script :
xdel(winsid());                 // ferme toutes les fenetres a chaque nouvel appel du script
clear;                          // nettoie toutes les variables a chaque nouvel appel du script

// Charge les données du TP
exec("tp5_donnees.sce", -1);
// Charge les fonctions du TP
exec("tp5_fonctions.sci", -1);


// **************** Exercice 1 ******************

if %f then                      // (Passer le booléen à %f pour sauter cet exercice)
    show_signal(Signal);    
end

// **************** Exercice 2 ******************

if %f then                      // (Passer le booléen à %f pour sauter cet exercice)
    X=[0,0,0,4,0,0,0];
    Y=[0, 0, 0, 0]
    for i=2:6
        Y(i-1)=(1/2)*X(i) + (1/4)*X(i-1) + (1/4)*X(i+1)
    end
    
    disp(Y);
    //linéaire car uniquement composé d'addition et de multiplication simple. Lissage ca elle lisse la fonction
    
    //2)
    M=[0.5 0.25 0 0 0.25; 0.25 0.5 0.25 0 0; 0 0.25 0.5 0.25 0; 0 0 0.25 0.5 0.25; 0.25 0 0 0.25 0.5;]
    //On peut voir dans sa structure que les valeurs sont placés en diagonales
    X=[0;0;4;0;0];
    Y=M*X;
    disp(Y);
    //3) Les poids uilisés sont [0.25, 0.5, ,0.25]
end

// **************** Exercice 3 ******************

if %f then                      // (Passer le booléen à %f pour sauter cet exercice)
    M=zeros(1,160)
    M(1)=1/4
    M(2)=1/2
    M($)=1/4
    M = build_conv_mat(M);
    disp(M);
    S=Signal
    //2)
    for i=1:100;
        drawlater()
        show_signal(S)
        S=M*S;
        drawnow()
    end

    
    //3)
    M=zeros(1,512)
    M(1)=1/4
    M(2)=1/2
    M($)=1/4
    M = build_conv_mat(M);
    
    //4)
    I=Image;
    I=M*I;
    I=I*M';
    show_image(I);
    
    //5)
    for i=1:100;
        I=M*I;
        I=I*M';
     end
     show_image(I);
        
end

// **************** Exercice 4 ******************

if %f then                      // (Passer le booléen à %f pour sauter cet exercice)
    S=Signal;
    M=build_conv
end

// **************** Exercice 5 ******************

if %t then                      // (Passer le booléen à %f pour sauter cet exercice)
   F = exp(2*%pi*%i*(0:159)'*(0:159)/160);
   //160 car le singla est dans r160.
   
   rs=round(F'*F);
   disp(rs);
   
   FR = real(F);
   FI= img(F);
   for i:160:
       clf()
       show_signal(real(:i), "red");
       halt()
end
