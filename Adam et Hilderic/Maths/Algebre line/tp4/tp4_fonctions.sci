// Fichier de fonctions Scilab (.sci) pour le TP4



// Nom des stations de CVélo utilisées dans l'étude
// ================================================

Stations = ["Jaude", "Universités", "Delille", "Montpied", "Salins", "Gare", "Chamalières", "Campus", "Montferrand", "Estaing"] ;



// Matrice de transition entre les stations
// ========================================
     
Mvelo = [..
  0.38    0.05    0.26    0.04    0.29    0.13    0.19    0.11    0.24    0.12 ;..
  0.01    0.23    0.17    0.26    0.09    0.14    0.05    0.05    0.06    0.12 ;..
  0.09    0.11    0.13    0.02    0.02    0.11    0.04    0.08    0.08    0.02 ;..
  0.04    0.08    0.08    0.12    0.09    0.07    0.07    0.10    0.02    0.03 ;..
  0.21    0.21    0.03    0.19    0.14    0.09    0.15    0.12    0.04    0.13 ;..
  0.09    0.14    0.12    0.05    0.16    0.21    0.06    0.10    0.12    0.15 ;..
  0.08    0.06    0.03    0.08    0.09    0.06    0.24    0.02    0.08    0.00 ;..
  0.05    0.03    0.07    0.21    0.02    0.02    0.11    0.34    0.00    0.06 ;..
  0.02    0.06    0.06    0.01    0.04    0.07    0.07    0.03    0.31    0.06 ;..
  0.03    0.03    0.05    0.02    0.06    0.10    0.02    0.05    0.05    0.31 ];

  
  
// ========================================================================
//    Représentation du contenu d'une matrice de transition M
//
// M : matrice de transition (doit être carrée)
//
// ARGUMENTS OPTIONNELS
// labels = ["etat1", "etat2", ...] => nom associé à chaque état ( = station )
// ========================================================================
    
function my_Matplot(M, labels)


    if size(M,1) <> size(M,2) then
        error("Une matrice de transition doit etre carrée.")
    end
    N = size(M,1);

    f = gcf(); // !! ATTENTION BUG : Si on appelle "figure()" auparavant, cela produit un fond coloré !!!?

    // Contenu de la matrice
    mn = min(M); mx = max(M);
    f.color_map = jetcolormap(102);
    colorbar(mn,mx) ;
    Matplot(flipdim( ceil(100*(M-mn+.000001)/(mx-mn)) , 1));
    // Propriétés de axes
    a = gca();
    a.axes_reverse=["off","on","off"];
    a.box = "on";
    a.x_location = "top";

    // Labels (optionnels)
    if argn(2) == 2 then
        if size(labels,2) <> N then
            error("Les labels doivent avoir le même nombre de colonnes que la matrice M");
        end
        a.x_ticks = tlist(["ticks" "locations", "labels"], 1:N, labels);
        a.y_ticks = tlist(["ticks" "locations", "labels"], 1:N, labels);
    end
endfunction



// ========================================================================
//          Représentation d'une [ou plusieurs] répartition(s) de vélos
//
// V = matrice de taille (N,k) avec 
//          N = nombre de stations
//          k = nombre de répartitions à représenter en parallèle
// Ainsi, chaque colonne de V représente UNE répartition possible pour les vélos
//
// En option, 'labels' pour donner un nom à chaque état ( = station )
// ========================================================================

function show_repartition(V, labels)


    N = size(V,1);
    k = size(V,2);
    drawlater();
    // Les barres proprement dites
    bar(V);
    // Labels (optionnels)
    a = gca();
    if argn(2) == 2 then
        if size(labels,2) <> N then
            error("Les labels doivent avoir le même nombre d''élements que size(V,1)");
        end
        a.x_ticks = tlist(["ticks" "locations", "labels"], 1:N, labels);
    end 
    drawnow();
endfunction

