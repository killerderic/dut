
// Deux lignes utiles à mettre au début de chaque script :
xdel(winsid());                 // ferme toutes les fenetres a chaque nouvel appel du script
clear;                          // nettoie toutes les variables a chaque nouvel appel du script

// Charge les fonctions du TP
exec("tp4_fonctions.sci", -1);


// **************** Exercice 1 ******************

if %f then                      // (Passer le booléen à %f pour sauter cet exercice)
my_Matplot(Mvelo, Stations);
//2) p=0.07
res=sum(Mvelo,1);
//retourne la somme de tout les élements d'une colonne
disp(res);
end

// **************** Exercice 2 ******************

if %t then                      // (Passer le booléen à %f pour sauter cet exercice)
    //Tous les éléments de la matrice sont supérieur a 0
    //et la somme des elements de chaque colonnes est bien égal a 1
    
    //2) x1 = M * x0
    //3) xn = m^n * x0
    
end

// **************** Exercice 3 ******************

if %f then                      // (Passer le booléen à %f pour sauter cet exercice)
    //1)
    if %f then
        V = [10 10 10 10 10 10 10 10 10 10]';
        x=V;
        for i = 1:5
            [%v0,%v1,%v2,%v3,%v4] = xclick();w = bool2s(%v0>64);
            x = Mvelo * x;
            show_repartition(x, Stations);
        end
    end
    
    //2)
    
    if %f then
        V = [100 0 0 0 0 0 0 0 0 0]';
        x=V;
        for i = 1:10
            [%v0,%v1,%v2,%v3,%v4] = xclick();w = bool2s(%v0>64);
            clf(x);
            x = Mvelo * x;
            show_repartition(x, Stations);
        end
    end
    
    //3)
    if %f then
        V = [0 0 0 0 0 0 0 0 0 100]';
        x=V;
        for i = 1:10
            [%v0,%v1,%v2,%v3,%v4] = xclick();w = bool2s(%v0>64);
            clf();
            x = Mvelo * x;
            show_repartition(x, Stations);
        end
    end
    
    //4)
    //On remarque que les probabilités se statbilisent très rapidement
    
    if %f then
        V = [10 10 10 10 10 10 10 10 10 10; 100 0 0 0 0 0 0 0 0 0; 0 0 0 0 0 0 0 0 0 100]';
        x=V;
        for i = 1:10
            [%v0,%v1,%v2,%v3,%v4] = xclick();w = bool2s(%v0>64);
            clf();
            x = Mvelo * x;
            show_repartition(x, Stations);
        end
    end
    
    //5)
    // 21 a jaude, 10 a université, 7 a Dellile, 6 a montpied, ...
    r=[22, 12, 8, 8, 16, 14, 8, 8, 6, 8]';
    
    // r = Mr
    
    res=Mvelo*r
    show_repartition(res, Stations);
    //R est bioen le vecteur stationnaire de la matrice
    
    
end

// **************** Exercice 4 ******************

if %t then                      // (Passer le booléen à %f pour sauter cet exercice)
   [B,D] = spec(Mvelo);
   res=BDB^-1
   show_repartition(res, Stations);
end
