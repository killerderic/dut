
// Deux lignes utiles à mettre au début de chaque script :
xdel(winsid());                 // ferme toutes les fenetres a chaque nouvel appel du script
clear;                          // nettoie toutes les variables a chaque nouvel appel du script

// Charge les fonctions du TP
exec("tp4_fonctions.sci", -1);


// **************** Exercice 1 ******************

if %f then                      // (Passer le booléen à %f pour sauter cet exercice)
    // 1-
    my_Matplot(Mvelo)
    
    // 2-
    // La probabilité est de 7%
    
    // 3-
    sum(Mvelo,1)
    // On retrouve une matrice ligne remplie de 1.
    // sum() fait la somme des éléments de chaque colonne.
    // La somme des probabilités de chaque colonne fait 1.
    
    
end

// **************** Exercice 2 ******************

if %f then                      // (Passer le booléen à %f pour sauter cet exercice)
    // 1-
    // C'est bien une matrice de transition car la sommme des probabilités de chacune des colonnes fait bien 1.
    
    // 2 -
    // X1 = M * X0  (matrice * vecteur = vecteur)
    
    // 3-
    // Xn = M * X(n-1)

end

// **************** Exercice 3 ******************

if %f then                      // (Passer le booléen à %f pour sauter cet exercice)
    
    // 1-
    nbVelo=[10 ; 10 ; 10 ; 10 ; 10 ; 10 ; 10 ; 10 ; 10 ; 10];
    for i=0 : 7
       clf();
       show_repartition(nbVelo)
       halt('Appuyez sur une touche pour continuer')
       
       nbVeloSuiv = Mvelo * nbVelo
       nbVelo = nbVeloSuiv;
    end
    
    // 2-
    nbVelo=[100 ; 0 ; 0 ; 0 ; 0 ; 0 ; 0 ; 0 ; 0 ; 0];
    for i=0 : 7
       clf();
       show_repartition(nbVelo)
       halt('Appuyez sur une touche pour continuer')
       
       nbVeloSuiv = Mvelo * nbVelo
       nbVelo = nbVeloSuiv;
    end
    
    // 3-
    nbVelo=[0 ; 0 ; 0 ; 0 ; 0 ; 0 ; 0 ; 0 ; 0 ; 100];
    for i=0 : 7
       clf();
       show_repartition(nbVelo)
       halt('Appuyez sur une touche pour continuer')
       
       nbVeloSuiv = Mvelo * nbVelo
       nbVelo = nbVeloSuiv;
    end
    
    // 4-
    // On remarque que peu importe la répartition initiale quand N augmente, on tend vers la même répartition.
    
    nbVelo=[10 100 0 ; 10 0 0 ; 10 0 0 ; 10 0 0 ; 10 0 0 ; 10 0 0 ; 10 0 0 ; 10 0 0 ; 10 0 0 ; 10 0 100];
    for i=0 : 7
       clf();
       show_repartition(nbVelo)
       halt('Appuyez sur une touche pour continuer')
       
       nbVeloSuiv = Mvelo * nbVelo
       nbVelo = nbVeloSuiv;
    end
    
    // 5-
    // Ils doivent construire :
    // 21 10 7 7 15 12 8 8 7 7 emplacements (respectivement dans chaque station et il s'agit de valeurs approchées)
    
    // 6-
    // R = lim(M * Xn) pour n tend vers un infini positif
    // R = M * R
    
end

// **************** Exercice 4 ******************

if %t then                      // (Passer le booléen à %f pour sauter cet exercice)
    
    // 1-
    [B,D]=spec(Mvelo);
    abs(B*D*(B^-1))
    // On retrouve les valeurs de Mvelos
    disp(B)
    disp(D)
    disp(Mvelo)
    
    D=abs(D)
    
    //2
    //M*M = (B*D*B^(-1))*(B*D*B^(-1)) d'après (1)
    //M*M = B*D*B^(-1)*B*D*B^(-1)= B*D*I*D*B^(-1)
    //M*M = B*(D*D)*B^(-1)
    
    // Ainsi :
    //M^(n)= (B*D*B^(-1))*(B*D*B^(-1))*......*(B*D*B^(-1))*(B*D*B^(-1))
    //                                  (n fois)
    //ET de la même manière que ci-dessus, cela se simplifie en :
    //M^(n)=B*D^(n)*B^(-1)
    //3
    D2=D*D
    D3=D2*D
    D4=D3*D
    D5=D4*D
    D6=D5*D
    D7=D6*D
    D8=D7*D    
    D9=D8*D
    D10=D9*D
    my_Matplot(D)
    figure()
     my_Matplot(D2)
     figure()
       my_Matplot(D3)
       figure()
    my_Matplot(D4)
    figure()
    my_Matplot(D5)
         figure() 
         my_Matplot(D6)
         figure()
               my_Matplot(D7)
               figure()
                           my_Matplot(D8)
                           figure()
                             my_Matplot(D9)   
                              figure() 
                                                                                   my_Matplot(D10)
    
    //4 On voit d'après les instances de my_Matplot ci-dessus que M^(n) converge vers une matrice nulle (à l'exception de la première valeur de la première ligne, égale à 1), car lorsqu'un reel vaut entre 1 et 0, il tend vers 0 lors que n tend vers un infini positif.
    
    //5 Ainsi les vecteurs sont envoyés seulement sur b1, car la seule valeur non nulle de D^(n) est la première de la première ligne, permettant de "conserver" la première colonne de la matrice B.
    
    //6
    B=abs(B)
    figure()
    my_Matplot(B)
    disp(B)
    //b1 est le vercteur stationnaire de la matrice Mvelos.
    
    end
