import java.util.ArrayList;
import java.util.List;

public class Wagon {
	
	public static final int capacite = 20;
	private int numero;
	private List<Passager> lesPassagers = new ArrayList<>();
	
	
	public Wagon(int numero){
		
		this.numero = numero;
	}
	
	public void ajouter(Passager lePassager){
		lesPassagers.add(lePassager);
		lePassager.setWagonActuel(this);
	}
	
	public void enlever(Passager lePassager){
		lesPassagers.remove(lePassager);
		lePassager.setWagonActuel(null);
	}
	
	public String toString(){
		
		String message = "\nWagon N°" + numero + " : " 
				+ lesPassagers.size() + " passagers." 
				+ "\nReste " + (capacite-lesPassagers.size()) + " places.";
		
		message += "\nListe des passagers : \n";
		
		
		message  += lesPassagers.toString() + "\n";

		return message;
	}
}
