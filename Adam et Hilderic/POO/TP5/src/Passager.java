
/**
 * @author hisauretbo
 *
 */
public class Passager {
	
	private String nom;
	private String prenom;
	private Wagon wagonActuel;
	
	public Passager(String nom, String prenom){
		
		this.nom = nom;
		this.prenom = prenom;
	}
	
	public String toString(){
		
		String message = nom + " " + prenom;
		return message;
	}
	
	public Wagon getWagonActuel(){
		return wagonActuel;
	}

	void setWagonActuel(Wagon wagonActuel) {
		this.wagonActuel = wagonActuel;
	}
	
	
}
