
public class Main {
	
	public static void main(String[] args){
		
		Train t1 = new Train(3);
		Passager p1 = new Passager("Dupont", "Pierre");
		Passager p2 = new Passager("Durant", "Paul");
		Passager p3 = new Passager("Lagarde", "Claude");

		try{
			t1.monterDansLeTrain(1, p1);
			t1.monterDansLeTrain(1, p2);
			t1.monterDansLeTrain(1, p3);
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
		
		//System.out.println(t1.toString());
		
		try{
			t1.deplacerAuWagonSuivant(p1);
			t1.deplacerAuWagonSuivant(p3);
			t1.deplacerAuWagonSuivant(p3);
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
		
		System.out.println("-----------------------------");
		//System.out.println(t1.toString());
		
		try{
			t1.deplacerAuWagonPrecedent(p3);
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
		
		//System.out.println(t1.toString());

		t1.demarrer();
		try{
			t1.descendreDuTrain(p1);
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
		
	}
}
