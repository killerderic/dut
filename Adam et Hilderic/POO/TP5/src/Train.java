import java.util.LinkedList;
import java.util.List;

public class Train {
	
	private boolean roule = false;
	private List<Wagon> listeDeWagons = new LinkedList<>();
	
	public Train(int nombreWagons){
		
		for(int i=0; i < nombreWagons; i++){
			
			Wagon wagon = new Wagon(i+1);
			listeDeWagons.add(wagon);
		}
		
	}
	
	public void demarrer(){
		setRoule(true);
	}
	
	public void arreter(){
		setRoule(false);
	}
	
	public boolean isRoule(){
		return roule;
	}
	
	private void setRoule(boolean roule){
		this.roule = roule;
	}
	
	public void monterDansLeTrain(int numeroWagon, Passager lePassager) throws Exception{
		
		if(isRoule()){
			throw new Exception("Train en marche, impossible de monter");
		}
		
		Wagon wagon = listeDeWagons.get(numeroWagon-1);

		wagon.ajouter(lePassager);
	}
	
	public void descendreDuTrain(Passager lePassager) throws Exception{
		
		if(isRoule()){
			throw new Exception("Train en marche, impossible de monter");
		}
		
		Wagon wagon = lePassager.getWagonActuel();
		
		wagon.enlever(lePassager);
	}
	
	public String toString(){
		
		String message = "Train compose de " + listeDeWagons.size() + " wagons";
		
		message += listeDeWagons.toString();

		return message;
	}
	
	public void deplacerAuWagonSuivant(Passager lePassager) throws Exception{
		
		if(roule){
			throw new Exception("Impossible de changer de wagon, le train roule.");
		}
		
		int indexWSuiv = listeDeWagons.indexOf(lePassager.getWagonActuel()) + 1;
		
		if(indexWSuiv == 0){
			throw new Exception("Pas de wagon suivant");
		}
		
		lePassager.getWagonActuel().enlever(lePassager);
		listeDeWagons.get(indexWSuiv).ajouter(lePassager);
		
	}
	
	public void deplacerAuWagonPrecedent(Passager lePassager) throws Exception{
		
		if(roule){
			throw new Exception("Impossible de changer de wagon, le train roule.");
		}
		
		int indexWPrec = listeDeWagons.indexOf(lePassager.getWagonActuel()) - 1;
		
		lePassager.getWagonActuel().enlever(lePassager);
		listeDeWagons.get(indexWPrec).ajouter(lePassager);
	}
}
