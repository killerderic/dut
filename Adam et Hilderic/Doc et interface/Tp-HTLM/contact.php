<!doctype html>
<html>

<?php
// define variables and set to empty values
$nomErr = $emailErr = $numberErr = $raisonErr = $visiteErr = $messageErr = "";
$nom = $email = $number = $raison = $visite = $message = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {

   if (empty($_POST["nom"])) {
     $nomErr = "Le nom est requis";
   } else {
     // veirifier que le nom ne compte que des lettres et des espaces
     if (!preg_match("/^[a-zA-Z ]*$/",$nom)) {
       $nomErr = "Seul les lettres et les espaces sont acceptés";
     }
     $nom = $_POST["nom"];
   }

   if (empty($_POST["email"])) {
     $emailErr = "Email est requise";
   } else {
     $email = $_POST["email"];
     if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
       $emailErr = "Format d'email invalid";
     }
   }

   if (empty($_POST["number"])) {
    $numberErr= "Le numéro est requis";

  }else{
    $number = $_POST["number"];
  }

   if (empty($_POST["raison"])) {
     $raisonErr = "Veuiller préciser ce champ";

   }else{
    $raison = $_POST["raison"];
   }

   if (empty($_POST["visite"])) {
     $visiteErr = "Préciser une raison";
   }else{
    $visite = $_POST["visite"];
   }

   if(empty($_POST["message"])){
     $messageErr = "Veuillez préciser un message";
   }
   else{
     $message = $_POST["message"];
   }
}
?>

<head>
  <meta charset="UTF-8" />
  <!-- addedd -->
  <link rel="stylesheet" href="css/normalize.css" />
  <link rel="stylesheet" href="css/main.css" />
  <link rel="stylesheet" href="css/contact.css" />
  <link rel="stylesheet" media="screen and (max-width: 800px)" href="css/main-mobile.css" />
  <link rel="stylesheet" href="css/js-image-slider.css" />

  <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
  <script src="libs/js-image-slider.js" type="text/javascript"></script>
  <title>Contact </title>
</head>

<body>
  <div id="containeur">

    <header>

      <a href="http://www.facebook.com"><img class="fimage"src="image/facebook.png" width="80" /></a>
      <a href="http://www.twitter.com"><img class="timage"src="image/twitter.png" width="80" /></a>

<nav>

        <h1>Ingenieur Informaticien</h1>
        <div id="sliderFrame">
            <div id="slider">
                <img src="image/image-slider/img1.jpg" alt="Welcome" />
                <img src="image/image-slider/img2.jpg" alt="" />
                <img src="image/image-slider/img3.jpg" alt="" />
            </div>
            <div id="htmlcaption" style="display: none;">
                <em>HTML</em> caption. Link to <a href="http://www.google.com/">Google</a>.
            </div>
        </div>

        <div id="blue-bar"></div><div id="texture1"></div><div id="texture2"></div><div id="texture3"></div>
        <ul>
          <li><a href="main.html">Home</a></li>
            <li><a href="article.html">Article</a></li>
          <li><a href="">Media</a>
              <ul>
                <li><a href="galerie.html">Image</a></li>
                <li><a href="video.html">Video </a></li>
              </ul>
          </li>
          <li class="active"><a href="contact.php" >Contact</a></li>
           <!-- Todo lien mobile -->
        </ul>
      </nav>

    </header>

    <div id="body">
      <div id="conteneur">

        <form action="contact.php" method="post" >

          <div id="boxnom">
            <label for="nom" required >Nom : </label>
            <input class="nom" id="nom" type="text" name="nom" value="<?php echo $nom; ?>"/>
            <div class="error" ><?php echo $nomErr; ?></div>
          </div>

          <label for="email" required>Email : </label>
          <input class="email" id="email" type="email" name="email" value="<?php echo $email;?>" />
          <div class="error" ><?php echo $emailErr; ?></div>

          <label for="number">Numéro : </label>
          <input class="number" id="number" type="text" name="number" value="<?php echo $number;?>" />
          <div class="error" ><?php echo $numberErr; ?></div>

          <label for="choix_raison">Raison : </label>
          <input class="raison" id="choix_raison" list="raison" type="text" name="raison" value="<?php echo $raison;?>" />
          <datalist id="raison">
            <option value="bug" >
            <option value="amélioration" >
            <option value="autre" >
          </datalist>
          <div class="error" ><?php echo $raisonErr; ?></div>

          <label for="visite"> 1ère visite ?</label>
          <input type="radio" name="visite" value="1" > Oui
          <input type="radio" name="visite" value="2" > Non
          <div class="error" ><?php echo $visiteErr; ?></div>

          <textarea required class="message" id="message"  name="message" rows="4" cols="50" ><?php echo $message;?></textarea>
          <textarea required class="mobile" id="message"  name="message" rows="4" cols="20" ><?php echo $message;?></textarea>
          <div class="error" ><?php echo $messageErr; ?></div>

          <input type="submit"> </input>

        </form>

        <?php
        echo "<h2>Vous avez donné</h2>";
        echo "<p> Nom :  $nom </p>";
        echo "<br>";
        echo "<p> Email :  $email </p>";
        echo "<br>";
        echo "<p> Number :  $number </p>";
        echo "<br>";
        echo "<p> Raison :  $raison </p>";
        echo "<br>";
        echo "<p> Viste :  $visite </p>";
        echo "<br>";
        echo "<p> Message :  $message </p>";
        ?>

      </div>
    </div>


    <footer>
      <p>Hildéric Sauret ---- Adam Reyes</p><p>Groupe 8</p>
      <a href="Contact.php">Webmaster</a>
      <!-- addedd -->
    </footer>
  </div>
</body>
</html>

<?php

?>
