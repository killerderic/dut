#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "mots.h"
#define MAX 30

int loadFile(char mots[][MAX], int tMax){

    FILE *fs;
    fs = fopen("mots.txt", "r");
    char mot[30];
    int i=0;
    fscanf(fs, "%s", mot);
    while(feof(fs) == 0){
        if(i == tMax){
            printf("Error tab trop petit");
            return -1;
        }
        strcpy(mots[i], mot);
        fscanf(fs, "%s", mot);
        i++;
    }
    fclose(fs);
    return i;
}

int loadFileV2(char mots[][MAX], int tMax){

    FILE *fs;
    fs = fopen("mots.txt", "r");
    char mot[30];
    int i=0;
    fgets(mot, 28, fs);
    mot[ strlen(mot) - 1 ] = '\0';
    while(feof(fs) == 0){
        if(i == tMax){
            printf("Error tab trop petit");
            return -1;
        }
        strcpy(mots[i], mot);
        fgets(mot, 28, fs);
        mot[ strlen(mot) - 1 ] = '\0';
        i++;
    }
    fclose(fs);
    return i;
}

void displayTab(char mots[][MAX], int tLo){
    int i;
    for(i=0; i < tLo; i++)
        printf("%s \n", mots[i]);
}

int addWordToTab(char mots[][MAX], int tLo, int tMax){
    if(tLo+1 > tMax){
        printf("Tab trop petit");
        return tLo;
    }
    int i, res;

    char motToAdd[MAX];
    printf("Taper mot svp : ");
    scanf("%s", motToAdd);

    for(i=0; i < tLo; i++){
        res=strcmp(motToAdd, mots[i]);
        if(res == 0){
            printf("Mot deja present \n");
            return tLo;
        }
        if(res < 0){
            int j;
            for(j=tLo; j != i; j--){
                strcpy(mots[j], mots[j-1]);
            }
            strcpy(mots[i], motToAdd);
            return tLo+1;
        }
    }
    strcpy(mots[tLo], motToAdd);
    return tLo+1;
}

void saveTab(char mots[][MAX], int tLo){
    FILE *fs;
    int i;
    fs = fopen("mots.txt", "w");
    if(fs == NULL){
        printf("Prb fichier save");
        return;
    }

    for(i=0; i < tLo; i++){
        fprintf(fs, "%s \n", mots[i]);
    }

    fclose(fs);
}

int menu(){

    int choice=0;
    while(choice==0){
        printf("------------------ \n");
        printf("Gestion des mots\n");
        printf("1 Afficher mots trie \n");
        printf("2 Ajouter mots \n");
        printf("9 quitter \n");
        printf("------------------ \n");

        printf("\nQue voulez-vous faire ? ");
        scanf("%d", &choice);
        printf("\n");
        if(choice != 1 && choice != 2 && choice !=3 && choice !=9){
            choice=0;
        }
    }

    return choice;
}

void global2Version(){

    int choice=0;
    char mots[MAX][MAX];
    int tLo;

    tLo = loadFileV2(mots, MAX);
    while(choice != 9){

        choice = menu();

        if(choice == 1){
            displayTab(mots, tLo);
        }else if(choice == 2){
            tLo = addWordToTab(mots, tLo, MAX);
            saveTab(mots, tLo);
        }
    }
}

void initialiser(char mot[], int n){
    int i;
    for(i=0; i < n; i++){
        mot[i] = '-';
    }
    mot[n] = '\0';
}

void placer(char mot[], char c, char mot2[]){
    int l;
    l = strlen(mot);// len mot 1 = len mot 2
    int i;
    for(i=0; i < l; i++){
        if(mot[i] == c){
            mot2[i] = c;
        }
    }
}

void penduGame(){

    char mot[30];
    int etape, l;

    printf("P1 proposer un mot : ");
    scanf("%s", mot);
    printf("P1 proposer un nbr d'etapes : ");
    scanf("%d", &etape);

    l = strlen(mot);
    printf("mot de %d lettres a trouver en %d etapes \n", l, etape);


    int i;
    char tt, rep, test[30];
    char mot2[30];
    initialiser(mot2, l);
    printf("%s \n", mot2);

    for(i=0;i<etape;i++){

        printf("Proposer une lettre : ");
        scanf("%c%*c", &tt);
        placer(mot, tt, mot2);
        printf("%s \n", mot2);

        printf("Avez vous reconnu le mot (o/n)? \n");
        scanf("%c%*c", &rep);

        if(rep == 'o'){

            printf("mot \n?");
            scanf("%s", test);

            if(strcmp(mot, test) == 0){
                printf("Bravo vous avez gagne \n");
                printf("mot trouve en %d etape \n", i+1);
                i = etape;
            }else{
                printf("Desole ... \n");
            }
        }

    }


}
