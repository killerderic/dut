#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tp9.h"

typedef struct{
    char reference[6];
    float prix;
    int stock;
    char type[50];

} Article;

Article lireArt(FILE *fs){
    Article article;
    fscanf(fs, "%s %f %d", article.reference, &article.prix, &article.stock);
    fgets(article.type, sizeof(article.type), fs);
    article.type[strlen(article.type) - 1] = '\0';

    return article;
}

int chargeArt(Article artT[], int max){
    FILE *fs;
    fs = fopen("fart.txt", "r");
    if(fs == NULL){
        printf("prb fichier");
        return -1;
    }
    int nbr, i;
    fscanf(fs, "%d", &nbr);
    if(nbr > max){
        printf("tab trop petit");
        return -2;
    }
    for(i=0; i < nbr; i++){
        Article article = lireArt(fs);
        artT[i] = article;
    }

    return nbr;
}

void displayArt(Article artT[], int nbr){
    int i;
    for(i=0; i < nbr; i++){
        printf("%s %f %d %s \n", artT[i].reference, artT[i].prix, artT[i].stock, artT[i].type);
    }
}
void global(){
    Article artT[50];
    char ref[6];
    int artNbr = chargeArt(artT, 50);

    displayArt(artT, artNbr);

    printf("entrer une ref \n");
    scanf("%s%*c", ref);
    int res;
    res = rechArt(ref, artT, artNbr);
    printf("%s %f %d %s \n", artT[res].reference, artT[res].prix, artT[res].stock, artT[res].type);
}

void global2(){
    Article artT[50];
    char ref[6];
    int artNbr = chargeArt(artT, 50);

    displayArt(artT, artNbr);

    suppArt(artT, &artNbr);

    displayArt(artT, artNbr);
}

int rechArt(char ref[], Article tarT[], int nbart){
    int i, res;
    for(i=0; i<nbart; i++){
        res = strcmp(ref, tarT[i].reference);
        if(res == 0){
            return i;
        }
    }
    return -1;
}

void eraseA(Article artT[], int *nbart, int index){
    int i;
    printf("nbart : %d, index : %d \n", *nbart, index);
    int a = *nbart;
    for(i=index; i < a-1; i++){
        artT[i] = artT[i+1];
    }
}


void suppArt(Article artT[], int *nbart){

    char refToSup[6];
    char on;
    int erase = 1, index, tryToErase=0, i;
    while(erase == 1){

        printf("Entrer une ref a supprimer svp \n");
        scanf("%s%*c", &refToSup);
        index = rechArt(refToSup, artT, *nbart);
        if(index == -1){
            printf("Erreur : Cet article n'existe pas \n");
        }else if(artT[index].stock != 0){
            printf("Erreur : Cet article ne peut etre supprime : stock  %d \n", artT[index].stock);
        }
        else{
            tryToErase = 1;
        }

        printf("Valider vous cette suppression ? (O/n) \n");
        scanf("%c%*c", &on);
        if(on == 'o'){
            //Erase the article
            eraseA(artT, nbart, index);
            printf("Article supprimer \n");
            erase = 0;
        }
    }

}

void eraseArt(){
    int artNb=0;
    char artT[100];
    char rep;
    artNb = chargeArt(artT, 100);
    int sup=1;

    displayArt(artT, artNb);
    while(sup == 1){
        suppArt(artT, &artNb);
        displayArt(artT, artNb);
        printf("Voulez vous continuer a supprimer (o/n) \n");
        scanf("%c%*c", &rep);
        if(rep == 'n'){
            sup = 0;
        }else if(artNb == 0){
            printf("Tableau vide \n");
            sup = 0;
        }
    }

    save(artT, artNb);
}

void save(Article artT[], int nbr){
    FILE * fs;
    int i;
    fs = fopen("final.txt", "w");
    fprintf(fs, "%d", nbr);
    for(i=1; i < nbr; i++){
        fprintf(fs, "%s %f %d %s", artT[i].reference, artT[i].prix, artT[i].stock, artT[i].type);
    }
}
