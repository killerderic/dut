
#ifndef TEST_H_INCLUDED
#define TEST_H_INCLUDED


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
    char reference[6];
    float prix;
    int stock;
    char type[50];

} Article;

int loadArticle(char * nomFich, Article tart[], int taillemax);
void affichArt(Article * tArt, int nbArt);
Article lireArt(FILE *fs);


int rchArticle(Article * artT, int n, Article a);

void tribulle(Article * tab, int n);
void permut(Article * t, int i, int i2);

int  fusionArt(Article * tart1, int nb1, Article * tart2, int nb2, Article * tarfus);

#endif // TEST_H_INCLUDED
