#include "tp13.h"

int loadArticle(char * nomFich, Article tart[], int taillemax){
    FILE * fs;
    fs = fopen(nomFich, "r");
    int nbart, i;
    Article art;
    fscanf(fs,"%d", &nbart);
    for(i=0; i < nbart; i++){
        art = lireArt(fs);
        tart[i] = art;
    }

    return nbart;
}

Article lireArt(FILE *fs){
    Article article;
    fscanf(fs, "%s %f %d", article.reference, &article.prix, &article.stock);
    fgets(article.type, sizeof(article.type), fs);
    article.type[strlen(article.type) - 1] = '\0';

    return article;
}

void affichArt(Article * tArt, int nbArt){
    int i;
    for(i=0; i < nbArt; i++){
        printf("%s %f %d %s \n", tArt[i].reference, tArt[i].prix, tArt[i].stock, tArt[i].type);
    }
}

void testload(){
    Article tart[100];
    Article tart2[100];
    int nbart, nbart2;

    Article tarfus[100];
    int nbfus;

    nbart = loadArticle("fart.txt", tart, 100);
    affichArt(tart, nbart);

    tribulle(tart, nbart);

    printf("-------------- \n");
    affichArt(tart, nbart);


    printf("-------------- \n");
    nbart2 = loadArticle("fart-tp12.txt", tart2, 100);
    affichArt(tart2, nbart2);
    tribulle(tart2, nbart2);


    nbfus = fusionArt(tart, nbart, tart2, nbart2, tarfus);

    printf("Fusion : \n");
    affichArt(tarfus, nbfus);


}

int  fusionArt(Article * tart1, int nb1, Article * tart2, int nb2, Article * tarfus){
    int i,k=0, j;
    for(i=0; i < nb1; i++){
        j=i;
        while(strcmp(tart1[i].type, tart2[j].type) < 0 && j < nb2){
            j++;
        }
        if(j == i){
            tarfus[k] = tart2[j];
        }
        else{
            tarfus[k] = tart1[i];
        }

        k++;
    }

    return k;
}
int rchArticle(Article * artT, int n, Article a){
    int i;
    for(i=0; i < n; i++){
        if(strcmp(artT[i].reference, a.reference) < 0){
            return i;
        }
    }

    return i;
}

void tribulle(Article * tab, int n){
    int i,k;
    for(k=0; k < n; k++){
            for(i=n-1;  i > k; i--){
                if(strcmp(tab[i].type, tab[i-1].type) < 0 ){
                    permut(tab, i, i-1);
                }
            }
    }
}

void permut(Article * t, int i, int i2){
    Article save = t[i];
    t[i] = t[i2];
    t[i2] = save;
}


