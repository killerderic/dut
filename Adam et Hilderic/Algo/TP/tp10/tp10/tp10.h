typedef struct{
    char reference[6];
    float prix;
    int stock;
    char designation[30];
}Article;

Article lireArt(FILE *fs);
Article * chargeArticle(FILE *fs, int * nbart);
Article sairsirArticle();
void testLoad();
void testInsert();
void displayArt(Article *artT, int nbart);
int rechArt(char ref[], Article tarT[], int nbart);
Article * insertArticle(Article article, Article artT[], int *nbart, int *tmax);
Article * updateWithFile(Article artT[], int *nbart, int *tmax);
void saveAsBin(Article *artT, char *fileName, int nbart);
Article * restoreFromBin(char *fileName, int nbart);
