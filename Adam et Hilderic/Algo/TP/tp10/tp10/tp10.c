#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tp10.h"


Article lireArt(FILE *fs){
    Article article;
    fscanf(fs, "%s %f %d", article.reference, &article.prix, &article.stock);
    fgets(article.designation, sizeof(article.designation), fs);
    article.designation[strlen(article.designation) - 1] = '\0';

    return article;
}

Article * chargeArticle(FILE *fs, int * nbart){
    fscanf(fs, "%d", nbart);
    Article *artT;
    artT = (Article *) malloc(sizeof(Article) * *nbart);
    if(artT == NULL){
        printf("Probleme allocation tableau \n");
    }

    int i;
    Article article;
    for(i=0; i < *nbart; i++){
        article = lireArt(fs);
        artT[i] = article;
    }


    return artT;
}

Article sairsirArticle(){
    Article article;
    printf("Reference, prix, stock \n");
    scanf("%s%*c %f%*c %d%*c", article.reference, &article.prix, &article.stock);
    printf("Designation : \n");
    char designation[30];
    fgets(designation, sizeof(designation), stdin);
    strcpy(article.designation, designation);

    return article;
}

//TEST FUNCTION
void testLoad(){
    FILE *fs;
    fs = fopen("fart.txt", "r");
    if(fs == NULL){
        printf("Probleme ouverte fichier");
    }
    int nbart;
    Article *artT;
    artT = chargeArticle(fs, &nbart);
    displayArt(artT, nbart);

    fclose(fs);
}

void testInsert(){
    FILE *fs;
    fs = fopen("fart.txt", "r");
    if(fs == NULL){
        printf("Probleme ouverte fichier");
    }
    int nbart;
    Article *artT;
    artT = chargeArticle(fs, &nbart);
    int tmax = nbart;
    displayArt(artT, nbart);

    Article article = sairsirArticle();
    artT = insertArticle(article, artT, &nbart, &tmax);
    displayArt(artT, nbart);

    fclose(fs);

}

void globalTest(){
    //Load the fart.txt in a table
    FILE *fs;
    fs = fopen("fart.txt", "r");
    if(fs == NULL){
        printf("Probleme ouverte fichier");
    }
    int nbart;
    Article *artT;
    artT = chargeArticle(fs, &nbart);
    int tmax = nbart;
    displayArt(artT, nbart);

    printf("------------------------- \n");
    //Update if with the majart.txt
    artT = updateWithFile(artT, &nbart, &tmax);
    displayArt(artT, nbart);

    //Save the file in fart.bin
    saveAsBin(artT, "fart.bin", nbart);

    //Restore from fart.bin
    artT = restoreFromBin("fart.bin", nbart);
    printf("------------------------- \n");
    displayArt(artT, nbart);
}

void displayArt(Article *artT, int nbart){
    int i;
    for(i=0; i < nbart-1; i++){
        printf("%s %.2f %d %s \n", artT[i].reference, artT[i].prix, artT[i].stock, artT[i].designation);
    }
}

int rechArt(char ref[], Article tarT[], int nbart){
    int i, res;
    for(i=0; i<nbart; i++){
        res = strcmp(ref, tarT[i].reference);
        if(res == 0){
            return i;
        }
    }
    return -1;
}

Article * insertArticle(Article article, Article artT[], int *nbart, int *tmax){

    int index;
    index = rechArt(article.reference, artT, *nbart);
    //If the article is already in the table
    if(index != -1){
        artT[index].prix = article.prix;
        artT[index].stock = article.stock;
        return artT;
    }else{
        //Else this is a new article
        //Check if this the table is large enough to add a new article
        if(*nbart + 1 > *tmax){
            //If not, create a larger table
            Article * newTab;
            newTab = (Article *) realloc(artT, (*tmax + 3) * sizeof(Article) );
            if(newTab == NULL){
                printf("Probl�me avec realloc \n");
                return artT;
            }
            *tmax = *tmax + 3;
            artT = newTab;
        }
        artT[*nbart-1] = article;
        *nbart  = *nbart + 1;
        return artT;
    }

}

Article * updateWithFile(Article artT[], int *nbart, int *tmax){
    FILE * fs;
    fs = fopen("majart.don", "r");
    if(fs == NULL){
        printf("Proleme avec fichier majart \n");
        return artT;
    }

    Article article = lireArt(fs);
    while(feof(fs) == 0){
        artT = insertArticle(article, artT, nbart, tmax);
        article = lireArt(fs);
    }

    fclose(fs);
    return artT;

}

void saveAsBin(Article *artT, char *fileName, int nbart){
    FILE *fbin;
    fbin = fopen(fileName, "wb");
    if(fbin == NULL){
        printf("Problme avec fichier binaire \n");
        return;
    }

    fwrite(&nbart, sizeof(int), 1, fbin);
    fwrite(artT, sizeof(Article), nbart, fbin);

    fclose(fbin);
}

Article * restoreFromBin(char *fileName, int nbart){
    FILE * fbin;
    fbin = fopen(fileName, "rb");
    if(fbin == NULL){
        printf("Problme avec fichier binaire \n");
        return NULL;
    }

    fread(&nbart, sizeof(int), 1, fbin);
    Article * artT;
    artT = (Article *) malloc(sizeof(Article) * nbart);
    if(artT == NULL){
        printf("Probleme avec tableau depusi bin \n");
    }
    fread(artT,sizeof(Article), nbart, fbin);

    fclose(fbin);
    return artT;
}

