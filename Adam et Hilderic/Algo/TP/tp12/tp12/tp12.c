Article lireArt(FILE *fs){
    Article article;
    fscanf(fs, "%s %f %d", article.reference, &article.prix, &article.stock);
    fgets(article.type, sizeof(article.type), fs);
    article.type[strlen(article.type) - 1] = '\0';

    return article;
}

int chargeFarticle(char *nomFich, Article ** tArt, int tmax){
    FILE *fs;
    int nbArt, i;
    Article * art;
    fs = fopen(nomFich, "r");
    if(fs == NULL) return -1;
    fscanf(fs, "%d", &nbArt);
    for(i=0; i < nbArt; i++){
        if(i+1 > tmax){
            printf("Taille max tableau depasse \n");
            return -3;
        }
        art = (Article *) malloc(sizeof(Article));
        if(art == NULL){
            printf("Probleme malloc\n");
            return -2;
        }
        *art= lireArt(fs);
        tArt[i] = art;
    }
    return nbArt;
}

void affichArt(Article ** tArt, int nbArt){
    int i;
    for(i=0; i < nbArt; i++){
        printf("%s %f %d %s \n", tArt[i]->reference, tArt[i]->prix, tArt[i]->stock, tArt[i]->type);
    }
}

void testLoad(){
    int nbArt, index;
    int tArt[100];
    nbArt = chargeFarticle("fart.txt", tArt, 100);
    affichArt(tArt, nbArt);

    index = rechDich("B0052", tArt, nbArt);
    printf("index : %d \n", index);

    nbArt = supprime(tArt, nbArt);
    affichArt(tArt, nbArt);
}

int rechDich(char * code, Article ** tArt, int nbart){
    int pos, m;
    if(nbart == 0) return 0;
    if(nbart == 1){
        if(strcmp(code, tArt[0]->reference) < 0){
            return 0;
        }else{
            return 1;
        }
    }
    m=(nbart - 1)/2;
    if(strcmp(code, tArt[m]->reference) <= 0){
        pos = rechDich(code, tArt, m+1);
    }else{
        pos = m+rechDich(code, tArt, nbart-(m+1));
    }

    return pos;
}

int supprime(Article ** tart, int nbart){
    char ref[50], rep;
    int index, i;
    printf("Ref de l'article a supprimer : ");
    scanf("%s%*c", ref);
    index = rechDich(ref, tart, nbart);
    printf("Supprimer %s ? \n", tart[index]->type);
    scanf("%c%*c", &rep);
    if(rep == 'o'){
        free(tart[index]);
        for(i=index; i < nbart; i++){
            tart[i] = tart[i+1];
        }

        return nbart-1;
    }
    return nbart;
}
