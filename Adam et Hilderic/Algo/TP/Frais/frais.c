#include <stdio.h>
#include "frais.h"

 /*
  programme : frais.c
  auteur : Hilderic Sauret Bourin et Adam Reyes
  date : jeudi 17 septembre 2015
  finalité : calculé les frais de remboursement d'hébergement/de transport/de repas
 */

void testG()
{
	float billet, montantT;
    int jD, jA, heureD, heureA, ville, trsp, nbrK, pui;
    saisirRAndH(&jD, &jA, &heureD, &heureA, &ville);
    saisirT(&trsp, &billet, &nbrK, &pui);

    calculT(trsp, nbrK, pui, billet, &montantT);
    printf("Montant transport : %.2f \n", montantT);
    calculRAndH(jD, jA, heureD, heureA, &montantT, &ville);

    printf("Total : %.2f \n", montantT);
}

/**float deplacement(int jD, int jA, int heureD, int heureA, int ville, int trsp, int nbrK, int pui, float billet){
	float montant, montantT, montantR;
	montant=calculH(jD, jA, ville);
	calculT(nbrK, trsp, pui, billet, &montantT);
	calculR(jD, jA, heureD, heureA, &montantR);

	return montant;
}
*/

float calculH(int jD, int jA, int ville)
{
	int nbrJ, montantH;
	nbrJ = jA - jD;
	if(ville==1){
		montantH = nbrJ * 65; //calcul des frais
	}else{
		montantH = nbrJ * 45;
	}
	return montantH;
}

void testH(void){


}

void voiture(int nbrK, int pui, float *montantTv){
	float augm7;
	augm7=0;
	if(pui>7){
		augm7=0.1;
	}
	if(nbrK<=100){
		*montantTv = nbrK *0.2;
		*montantTv = *montantTv + (augm7 * *montantTv);
	}else if(nbrK<=250){
		*montantTv = nbrK *0.22;
		*montantTv = *montantTv + (augm7 * *montantTv);
	}else if(nbrK<=300){
		*montantTv = nbrK *0.25;
		*montantTv = *montantTv + (augm7 * *montantTv);
	}else{
			*montantTv = nbrK *0.3;
			*montantTv = *montantTv + (augm7 * *montantTv);
    }

}

void calculT(int trsp, int nbrK, int pui, float billet, float *montantT){
    if(trsp == 1 || trsp == 2){
        *montantT =billet;
    }else{
        voiture(nbrK, pui, montantT);
    }
}

void saisirT(int *trsp, float *billet, int *nbrK, int *pui){
    printf("Moyen de Transport : \n ");
    scanf("%d", trsp);
    if(*trsp == 1 || *trsp ==2){
        printf("Prix du billet \n");
        scanf("%f", billet);
    }
    else{
        printf("Nbk et puissance \n");
        scanf("%d %d", nbrK, pui);
    }
}

void testT(void){
	int trsp, nbrK, pui;
	float billet, montantT;

    saisirT(&trsp, &billet, &nbrK, &pui);
    calculT(trsp, nbrK, pui, billet, &montantT);

    printf("%.2f", montantT);
}

void calculRAndH(int jD, int jA, int heureD, int heureA, float *montant, int *ville){
	int nbrJ, total;

	if(heureD < 12){
		total += 2 * 13;
	}
	else{
		total += 13;
	}
	if(heureA < 12){
		total += 6;
	}
	else if(heureA < 19){
		total += 6 + 13;
	}
	else{
		total += 6 + 2 * 13;
	}

	nbrJ=jA-jD+1;

	if(nbrJ >= 2){
		total += (nbrJ - 2) * (6 + 2 * 13);
	}

	if(ville==1){
		total += nbrJ * 65; //calcul des frais
	}else{
		total += nbrJ * 45;
	}

	*montant = *montant + total;

}
void saisirRAndH(int *jD, int *jA, int *heureD, int *heureA, int *ville){
    printf("Jour depart et jour arrivee \n");
    scanf("%d %d", jD, jA);

    printf("Heure depart et heure arrivee \n");
    scanf("%d %d", heureD, heureA);

    printf("Ville \n");
    scanf("%d", ville);
}
void testRAndH(void){
    int jD, jA, heureD, heureA, montant, ville;

	saisirRAndH(&jD, &jA, &heureD, &heureA, &ville);
	calculRAndH(jD, jA, heureD, heureA, &montant, &ville);

	printf("%d", montant);
}

void readDeplt(){
    FILE *fs;
    fs = fopen("dv1.txt", "r");
    if(fs == NULL){
        printf("Probleme ouverture fichier fichier");
        return;
    }
    int eNbr, i=0;
    int aNbr, jD, jA, ville, fH;
    float fT;
    scanf("%d", &eNbr);

    fscanf(fs, "%d %d %d %d", &aNbr, &jD, &jA, &ville);
    while(feof(fs) == 0){

        if(aNbr == eNbr){
            fH = calculH(jD, jA, ville);
            fT += fH;
            i++;
            printf("%d %d %d %d %d\n", aNbr, jA, jD, ville, fH);
        }
        fscanf(fs, "%d %d %d %d", &aNbr, &jD, &jA, &ville);

    }

    fclose(fs);
    float moy;
    moy = fT / i;
    if(moy != 0){
        printf("Moy : %.2f", moy);
    }else{
        printf("Pas d'employee avec ce numero");
    }
}

void saisieD(int *eNbr, int *jA, int *jD, int *ville){

    printf("Numero employe : ");
    scanf("%d", eNbr);

    printf("Jour depart : ");
    scanf("%d", jA);
    if(*jA > 366 || *jA < 0){
        printf("erreur jA, doit etre inferieur a 366\n");
        while(*jA > 366 || *jA < 0){
            printf("Jour depart : ");
            scanf("%d", jA);
            if(*jA > 366 || *jA < 0){
                printf("erreur jA, doit etre inferieur a 366\n");
            }
        }
    }

    printf("Jour de retour :");
    scanf("%d", jD);
    if(*jD > 366 || *jD < 0){
        printf("erreur jD, doit etre posterieur au depart\n");
        while(*jD > 366 || *jD < 0){
            printf("Jour retour");
            scanf("%d", jD);
            if(*jD > 366 || *jD < 0){
                printf("erreur jD, doit etre posterieur au depart\n");
            }
        }
    }

    printf("Ville :");
    scanf("%d", ville);
    if(*ville != 0 && *ville != 1){
        printf("Numero de ville incorect, reesayer \n");
        while(*ville != 0 && *ville != 1){
            scanf("%d", ville);
            if(*ville != 0 && *ville != 1){
                printf("Numero de ville incorect, reesayer \n");
            }
        }
    }

}
void enregDepltV2(){


    FILE *fs;
    fs = fopen("dv1.txt", "a");

    int retour=1;
    while(retour==1){
        int eNbr, jA, jD, ville;
        saisieD(&eNbr, &jA, &jD, &ville);
        printf("%d %d %d %d \n", eNbr, jA, jD, ville);
        printf("Voulez-vous valider la saisie ? 1 pour oui, autre chose pour non \n");
        int y=0;
        scanf("%d", &y);
        if(y == 1){
            fprintf(fs, "%d %d %d %d", eNbr, jA, jD, ville);
        }
        printf("Voulez vous continuer ? 1 oui, 2non\n");
        scanf("%d", &retour);
    }
}
void enregDepltV1(){
    int n=0;
    FILE *fs;
    fs=fopen("dv1.txt", "a");

    while (n == 0){
        int eNbr, jD, jA, ville;
        scanf("%d %d %d %d", &eNbr, &jD, &jA, &ville);
        if(eNbr != 0){
            fprintf(fs, " \n%d %d %d %d", eNbr, jD, jA, ville);
        }else{
            n = 1;
        }
    }
}
/*
void readDeplt(){
    FILE *fs;
    fs = open("dv1.txt", "w");
    int eNbr, n=0, i=0, fT;
    scanf("%d", &eNbr);

    while (feof(fs) == 0){
        int aNbr, fT;
        fscanf(fs, "%d", &aNbr);
        if(aNbr == eNbr){
            int jD, jA, ville, fH;
            fscanf(fs, "%d %d %d \n", &jD, &jA, &ville);
            fH = calculH(jD, jA, ville);
            fT += fH;
            i++;
            printf(fs, "%d %d %d %d %d", jA, jD, ville, fH);
        }
        else{
            printf("Pas d'employe avec ce numero");
            fscanf(fs, "\n");
        }

    }
    int moy;
    moy = fT / i;
    printf("Moy : ", moy);
}*/
