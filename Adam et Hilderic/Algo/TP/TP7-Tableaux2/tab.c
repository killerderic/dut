#include <stdio.h>
#include <stdlib.h>
/*

int menu(){
    int choix=0;int loadvillePrice(int *villeT, float *prixT, int taille){
    int ville;
    float prix;
    FILE *fs2;
    fs2 = fopen("prixVille.txt", "r");
    if (fs2 == NULL) printf("Probleme lecture");
    fscanf(fs2, "%d %f", &ville, &prix);
    int i=0;
    while(feof(fs2) == 0){
        if(i+1 > taille) return -1;
        villeT[i] = ville;
        prixT[i] = prix;

        i += 1;
        fscanf(fs2, "%d %f", &ville, &prix);
    }

    fclose(fs2);
    return i+1;
}

void afficherPetV(int *ville, float *prix, int tailleLo){
    int i;
    printf("Nb de lieu : %d \n", tailleLo);
    for(i = 0; i < tailleLo-1; i++){
        printf(" %d %.2f \n", ville[i] , prix[i]);
    }
}
    while (choix == 0){
        printf("-------------------- \n");
        printf("Gestion des lieux \n");
        printf("1 liste des lieux \n");
        printf("2 ajout d'un nouveau lieux \n");
        printf("3 suppression d'un lieu \n");
        printf("9 quitter \n");
        printf("-------------------- \n");
        scanf("%d", &choix);
        if(choix != 1 && choix !=2 && choix != 3 && choix != 9){
            choix=0;
        }
    }

    return choix;
}

#define TAILLE 100
void gestionLieux(){
    int villeT[TAILLE], tailleLo, taillePhy = TAILLE, rs;
    float prixT[TAILLE];
    tailleLo = loadvillePrice(villeT, prixT, taillePhy);

    rs = menu();

    while (rs != 9){
        if(rs == 1){
            afficherPetV(villeT, prixT, tailleLo);
        }
        if(rs == 2){
            tailleLo = ajouterNetP(villeT, prixT, tailleLo, taillePhy);
            createTabFile(villeT, prixT, tailleLo);
        }

        if(rs == 3){
            tailleLo = supprByLieuNbr(villeT, prixT, tailleLo);
        }

        rs = menu();
    }

}

int rchIndexByLieuNbr(int *villeT, int tailleLo, int lieuNbr){
    int i;
    for(i=0; i < tailleLo; i++){
        if(villeT[i] == lieuNbr){
            return i;
        }
    }
    return -1;
}

int ajouterNetP(int *villeT, float *prixT, int tailleLo, int taillePhy){
    int villeNbr, rs=0;
    float prix;

    printf("Entree numero ville et prix : ");
    scanf("%d %f", &villeNbr, &prix);

    while(rs == 0){
        if( rchIndexByLieuNbr(villeT, tailleLo, villeNbr) != -1){// If the function has found something
            printf("Ville deja presente \n");
        }
        else if(tailleLo + 1 > taillePhy){
            printf("Taille du tableau depasse \n");
        }
        else{
            rs=1;
        }

        if( rs == 0){
            printf("Entree numero ville et prix : ");
            scanf("%d %f", &villeNbr, &prix);
        }
    }

    villeT[tailleLo-1]=villeNbr;
    prixT[tailleLo-1]=prix;
    return tailleLo + 1;
}

void createTabFile(int *villeT, float *prixT, int tailleLo){
    FILE *fs;
    fs = fopen("prixVille.txt", "w");
    if (fs == NULL){
        printf("Probleme lecture lecture de fichier");
        return;
    }

    int i;
    for(i=0; i < tailleLo; i++){
        fprintf(fs, "%d %.2f \n", villeT[i], prixT[i]);
    }

    fclose(fs);
}

int decalage(int *villeT, float *prixT, int tailleLo, int indexToErase){
    if(indexToErase > tailleLo){
        printf("Tantative d'effacer en dehors du tableau");
        return tailleLo;
    }

    int i;
    for(i=indexToErase; i < tailleLo-1; i++){
        villeT[i] = villeT[i+1];
        prixT[i] = prixT[i+1];
    }

    tailleLo -= 1;
}

int supprByLieuNbr(int *villeT, float *prixT, int tailleLo){
    int lieuNbrToErase, indexLieuNbrToErase;
    printf("Quel est le numero du lieu a supprimer ?");
    scanf("%d", &lieuNbrToErase);

    indexLieuNbrToErase = rchIndexByLieuNbr(villeT, tailleLo, lieuNbrToErase);
    if(indexLieuNbrToErase == -1){
        printf("Un lieu avec ce num�ro n'existe pas");
        return tailleLo;
    }

    tailleLo = decalage(villeT, prixT, tailleLo, indexLieuNbrToErase);

    return tailleLo;
}
*/

/* R�vision : */


int loadvillePrice(int *villeT, float *prixT, int taille){
    int ville;
    float prix;
    FILE *fs2;
    fs2 = fopen("prixVille.txt", "r");
    if (fs2 == NULL) printf("Probleme lecture");
    fscanf(fs2, "%d %f", &ville, &prix);
    int i=0;
    while(feof(fs2) == 0){
        if(i+1 > taille) return -1;
        villeT[i] = ville;
        prixT[i] = prix;

        i += 1;
        fscanf(fs2, "%d %f", &ville, &prix);
    }

    fclose(fs2);
    return i;
}

void afficherPetV(int *ville, float *prix, int tailleLo){
    int i;
    printf("Nb de lieu : %d \n", tailleLo);
    for(i = 0; i < tailleLo; i++){
        printf(" %d %.2f \n", ville[i] , prix[i]);
    }
}

int menu(){
    int choice=0;
    while(choice==0){
        printf("------------------ \n");
        printf("Gestion des lieux \n");
        printf("1 liste des lieux \n");
        printf("2 ajout d'un lieux \n");
        printf("3 suppression d'un lieux \n");
        printf("9 quitter \n");
        printf("------------------ \n");

        scanf("%d", &choice);
        if(choice != 1 && choice != 2 && choice !=3 && choice !=9){
            choice=0;
        }
    }

    return choice;
}

#define TAILLE 200
void gestionLieux(){
    int choice=0;
    int tlo, villeT[TAILLE];
    float priceT[TAILLE];
    tlo = loadvillePrice(villeT, priceT, TAILLE);
    while(choice != 9){

        choice = menu();

        if(choice == 1){
            afficherPetV(villeT, priceT, tlo);
        }

        if(choice == 2){
            tlo=addVilleAndPrice(villeT, priceT, tlo, TAILLE);
        }

        if(choice == 3){
            deleteByVilleNbr(villeT, priceT, &tlo);
        }

    }
    createPVFile(villeT, priceT, tlo);
}

int rchIndexOfValue(int *t, int tlo, int value){
    int i;
    for(i=0;i<tlo; i++){
        if(t[i] == value){
            return i;
        }
    }
    return -1;
}

int addVilleAndPrice(int *villeT, float *priceT, int tlo, int tp){
    if(tlo + 1 > tp) return -1;

    int villeNbr;
    float price;
    printf("Ville nbr : ");
    scanf("%d", &villeNbr);
    if(rchIndexOfValue(villeT, tlo, villeNbr) != -1){
        printf("Ville deja presente \n");
        return tlo;
    }

    printf("Price : ");
    scanf("%f", &price);
    villeT[tlo]=villeNbr;
    priceT[tlo]=price;

    return tlo+1;
}

void createPVFile(int *villeT, float *priceT, int tlo){
    FILE *fs;
    fs=fopen("prixVille.txt", "w");
    if(fs==NULL) printf("Probleme de fichier");

    int i;
    for(i=0;i<tlo;i++){
        fprintf(fs, "%d %f \n", villeT[i], priceT[i]);
    }
}

int decalage(int *prixT, float *priceT, int tlo, int startIndex){

    int i;
    for(i=startIndex; i < tlo; i++){
        prixT[i]=prixT[i+1];
        priceT[i]=priceT[i+1];
    }

    return tlo-1;
}

void deleteByVilleNbr(int *villeT, float *priceT, int *tlo){
    int villeNbrToErase;
    printf("Ville Nbr de la ville a supprimer : ");
    scanf("%d", &villeNbrToErase);
    int indexToErase=rchIndexOfValue(villeT, *tlo, villeNbrToErase);
    if(indexToErase == -1){
        printf("Cette ville n'existe pas \n");
    }

    *tlo=decalage(villeT, priceT, *tlo, indexToErase);
    printf("Ville efface \n");
}

/*

6 55.00
63 48.50
0 50.00
11 56.00
0 0.00

*/
