#include "head.h"
#include <stdio.h>

void lireDuree(int *d){
    FILE *fs;
    fs = fopen("fichier.txt", "r");
    if(fs == NULL) printf("Probleeme de lecture de fichier");

    int numE, jD, jR, lieu, dur;
    fscanf(fs, "%d %d %d %d", &numE, &jD, &jR, &lieu);
    while(feof(fs) == 0){
        dur = jR - jD;
        if(dur < 10){
            d[dur] += 1;
        }else{
            d[11] += 1;
        }
        fscanf(fs, "%d %d %d %d", &numE, &jD, &jR, &lieu);
    }
}
void afficher(int *d){
    int i;
    printf("Duree :          ");
    for(i=1; i < 11; i++){
        if(i<10){
            printf("%d ", i);
        }
        else{
            printf("plus de 10 \n");
        }
    }


    printf("Nb deplacements :");
    for(i=1; i < 11; i++){
        if(i<10){
            printf("%d ", d[i]);
        }
        else{
            printf("%d", d[11]);
        }
    }
}

void testLireD(){
    int d[12]= {0};
    lireDuree(d);
    afficher(d);
}

int loadvillePrice(int *villeT, float *prixT, int taille){
    int ville;
    float prix;
    FILE *fs2;
    fs2 = fopen("prixVille.txt", "r");
    if (fs2 == NULL) printf("Probleme lecture");
    fscanf(fs2, "%d %f", &ville, &prix);
    int i=0;
    while(feof(fs2) == 0){
        if(i+1 > taille) return -1;
        villeT[i] = ville;
        prixT[i] = prix;

        i += 1;
        fscanf(fs2, "%d %f", &ville, &prix);
    }
    return i+1;
}
void afficherPetV(int *ville, float *prix, int tailleLo){
    int i;
    printf("Nb de lieu : %d \n", tailleLo);
    for(i = 0; i < tailleLo-1; i++){
        printf(" %d %.2f \n", ville[i] , prix[i]);
    }
}

void testPetV(){
    int tailleMax = 200, tailleLo;
    int ville[200]={0};
    float prix[200]={0};

    tailleLo = loadvillePrice(ville, prix, tailleMax);
    afficherPetV(ville, prix, tailleLo);
}

float rechForfait(int *villeT, float *prixT, int tailleLo, int numLieu){
    int i;
    for(i=0; i < tailleLo; i++){
        if(villeT[i] == numLieu){
            return prixT[i];
        }
    }
    return -1;
}

void testRechForf(){
    int tailleP = 200, tailleL, numLieu = 6, forfait;
    float prixT[tailleP];
    int villeT[tailleP];

    tailleL = loadvillePrice(villeT, prixT, tailleP);
    forfait = rechForfait(villeT, prixT, tailleL, numLieu);

    printf("%d", forfait);
}

void afficherFraisTTH(int *villeT, float *prixT, int tailleLo){

    // Open file
    FILE *fs;
    fs = fopen("fichier.txt", "r");


    printf("N Employee   Nb Jour   Lieu   Forfait   Remboursement \n");

    // Calcule remboursement et affichage
    int eNbr, jD, jR, lieu;
    fscanf(fs, "%d %d %d %d", &eNbr, &jD, &jR, &lieu);
    while(feof(fs) == 0){
        int duree;
        float remboursement , forfait;
        duree = jR- jD;
        forfait = rechForfait(villeT, prixT, tailleLo, lieu);
        remboursement = duree * forfait;

        // Display result
        printf("%d    %d    %d    %.2f    %.2f \n", eNbr, duree, lieu, forfait, remboursement);
        fscanf(fs, "%d %d %d %d", &eNbr, &jD, &jR, &lieu);
    }

}
