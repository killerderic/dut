#include "tp11.h"

void repete(int n, char s[]){
    if(n == 0){
        return;
    }
    printf("%s", s);
    repete(n-1, s);
}

void testRepete(){
    char s[5] = "bla";
    repete(5, s);
}

void pyramide(int n, char s[]){
    if(n == 0 ){
        return;
    }
    pyramide(n-1, s);
    repete(n, s);
    printf("\n");
}

int zeroEnding(int nombre){

    float res;
    res = nombre / 10;
    if(res == nombre){
        return 0;
    }

    return zeroEnding(res) + 1;
}

int rq(int *a, int b){
    if(*a < b){
        return 0;
    }
    *a -= b;
    return rq(a, b) + 1;
}

int base(int n, int b){
    int res = n/10;
    if(res == 0){
        return n%10;
    }

    int taille = tailleN(n);

    return base(res, b) * b + n%10;
}



