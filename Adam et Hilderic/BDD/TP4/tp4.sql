--start /home/etud/info/adreyes1/Cours/BDD/TP4/tp4.sql

DROP TABLE ABSORPTION PURGE;
DROP TABLE BREUVAGE PURGE;

CREATE TABLE BREUVAGE(
	breuvage VARCHAR(30) CONSTRAINT keyB PRIMARY KEY,
	couleur VARCHAR(10),
	alcoolise CHAR(3) CONSTRAINT alcool CHECK(alcoolise IN('oui','non'))
);

INSERT INTO BREUVAGE VALUES('vin de framboise', 'rose','oui');
INSERT INTO BREUVAGE VALUES('sirop de menthe', 'vert','non');
INSERT INTO BREUVAGE VALUES('cointreau', 'orange','oui');
INSERT INTO BREUVAGE VALUES('liqueur de cacao', 'marron','oui');
INSERT INTO BREUVAGE VALUES('elixir de longue vie', 'incolore','non');
INSERT INTO BREUVAGE VALUES('philtre d amour', 'rouge','non');
INSERT INTO BREUVAGE VALUES('serum de vérite', 'incolore','non');
INSERT INTO BREUVAGE VALUES('cidre', 'brun','oui');

CREATE TABLE ABSORPTION(
	dragon VARCHAR(15) CONSTRAINT keyDrag references DRAGONS,
	breuvage VARCHAR(40) CONSTRAINT keyBreuv references BREUVAGE,
	litre NUMBER(2) 
);

INSERT INTO ABSORPTION VALUES('Smeagol', 'liqueur de cacao', 4);
INSERT INTO ABSORPTION VALUES('Smeagol', 'cointreau', 3);
INSERT INTO ABSORPTION VALUES('Smeagol', 'vin de framboise', 5);
INSERT INTO ABSORPTION VALUES('Smeagol', 'cidre', 2);
INSERT INTO ABSORPTION VALUES('Birdurh', 'cointreau', 1);
INSERT INTO ABSORPTION VALUES('Negueth', 'philtre d amour', 1);
INSERT INTO ABSORPTION VALUES('Negueth', 'elixir de longue vie', 2);
INSERT INTO ABSORPTION VALUES('Tarak', 'philtre d amour', 1);
INSERT INTO ABSORPTION VALUES('Tarak', 'vin de framboise', 4);
INSERT INTO ABSORPTION VALUES('Bolong', 'sirop de menthe', 6);
INSERT INTO ABSORPTION VALUES('Nessie', 'liqueur de cacao', 4);

SELECT dragon, SUM(litre)"Quantite bu par Dragon" FROM ABSORPTION
GROUP BY dragon;

(SELECT dragon, SUM(litre)"Quantite bu par Dragon" FROM ABSORPTION
GROUP BY dragon)
UNION
(SELECT nom, 0 FROM DRAGONS D1 WHERE  NOT EXISTS ( SELECT * FROM ABSORPTION WHERE D1.nom = dragon)
);

SELECT dragon FROM ABSORPTION A1 GROUP BY dragon HAVING SUM(litre) >5;

SELECT dragon, SUM(litre)"Quantite bu par Dragon" FROM ABSORPTION A1, BREUVAGE B1
WHERE A1.breuvage = B1.breuvage AND B1.couleur = 'incolore'
GROUP BY dragon;

SELECT dragon FROM ABSORPTION A1, BREUVAGE B1
WHERE A1.breuvage = B1.breuvage AND B1.alcoolise = 'oui'
GROUP BY dragon
HAVING COUNT(A1.breuvage) = ( SELECT COUNT(breuvage) FROM BREUVAGE B2 WHERE B2.alcoolise = 'oui');

(SELECT dragon FROM ABSORPTION)
MINUS
(SELECT A1.dragon FROM ABSORPTION A1, BREUVAGE B1 WHERE A1.breuvage = B1.breuvage AND B1.alcoolise <> 'oui');

SELECT SUM(litre)"Quantite bu par Dragon" FROM ABSORPTION;

SELECT dragon, breuvage FROM ABSORPTION ORDER BY dragon ASC;





commit work;

