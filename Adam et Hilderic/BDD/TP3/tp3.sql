
SELECT nom FROM DRAGONS WHERE cracheFeu='O';

SELECT nom FROM DRAGONS WHERE cracheFeu='O' and sexe='M';

SELECT nom FROM DRAGONS WHERE sexe='F' ORDER BY longueur DESC;

SELECT AVG(MOD(longueur, ecailles)) FROM DRAGONS WHERE sexe='M';
SELECT AVG(MOD(longueur, ecailles)) FROM DRAGONS WHERE sexe='F';

SELECT nomAimant FROM AMOURS;

SELECT nom FROM DRAGONS N1
WHERE NOT EXISTS ( SELECT nomAimant FROM Amours N2 WHERE N1.nom = N2.nomAimant );

SELECT nomAimant, nomAime FROM AMOURS WHERE force='passionnement';

SELECT nomAimant, nomAime FROM AMOURS WHERE force='passionnement' OR force='a la folie';

SELECT nomAimant, nomAime FROM AMOURS A1 
WHERE EXISTS ( SELECT nomAime FROM AMOURS A2 WHERE A1.nomAimant = A2.nomAime );

SELECT nomAimant, nomAime FROM AMOURS A1 
WHERE EXISTS ( SELECT nomAime FROM AMOURS A2 WHERE A1.nomAimant = A2.nomAime AND A2.nomAimant != A1.nomAime );

SELECT nom FROM DRAGONS D1 WHERE sexe = 'F' AND EXISTS(
	SELECT * FROM AMOURS A1 WHERE D1.nom = A1.nomAime AND EXISTS(
		SELECT nom FROM DRAGONS D2 WHERE enAmour = 'macho' AND D2.nom = A1.nomAimant )
);

SELECT D1.nom FROM DRAGONS D1, DRAGONS D2, AMOURS
WHERE D1.sexe = 'F'
	AND nomAime = D1.nom
	AND nomAimant = D2.nom
	AND D2.enAmour = 'timide'
GROUP BY D1.nom
HAVING COUNT(nomAimant) = (SELECT COUNT(*) FROM AMOURS
				WHERE D1.nom = nomAime);

SELECT * FROM NOURRITURES WHERE calories < 10;

SELECT nom FROM DRAGONS D1 WHERE EXISTS(
	SELECT * FROM REPAS WHERE produit = 'oeuf' AND nomD=D1.nom 
);

SELECT nom FROM DRAGONS D1 WHERE sexe='F' AND EXISTS(
	SELECT * FROM REPAS WHERE produit = 'oeuf' AND nomD=D1.nom 
);

SELECT nom FROM DRAGONS D1 WHERE NOT EXISTS(
	SELECT * FROM REPAS WHERE D1.nom = nomD
);

SELECT produit FROM NOURRITURES N1 WHERE calories > 10 AND EXISTS(
	SELECT * FROM DRAGONS D1 WHERE cracheFeu = 'O' AND sexe = 'M' AND EXISTS(
		SELECT * FROM REPAS R1 WHERE R1.nomD=D1.nom )
);

SELECT nomD FROM REPAS GROUP BY nomD HAVING COUNT (produit) = (SELECT COUNT(produit) FROM NOURRITURES);

SELECT nomD, SUM(N.calories*R.quantite) FROM REPAS R, NOURRITURES N 
WHERE R.produit=N.produit 
GROUP BY nomD
ORDER BY SUM(N.calories*R.quantite) ASC;

 -- tt les var dans le select doivent être dans le group BY
SELECT D.nom, (SUM(N.calories*R.quantite)/D.longueur) "Longueur flamme" FROM DRAGONS D, NOURRITURES N, REPAS R 
WHERE D.cracheFeu = 'O' AND R.produit=N.produit AND D.nom = R.nomD
GROUP BY D.nom, D.longueur
ORDER BY (SUM(N.calories*R.quantite)/D.longueur) DESC;


(SELECT nom FROM DRAGONS D1 WHERE NOT EXISTS(
	SELECT nomD FROM REPAS WHERE D1.nom = nomD))
INTERSECT
(SELECT nom FROM DRAGONS N1
WHERE NOT EXISTS ( SELECT nomAimant FROM Amours WHERE N1.nom = nomAimant ));

SELECT R.produit FROM REPAS R WHERE EXISTS (
	SELECT nomAimant FROM AMOURS WHERE R.nomD = nomAimant AND force='a la folie')
GROUP BY R.produit;

SELECT D1.nom FROM DRAGONS D1 WHERE D1.sexe = 'F' 
	AND EXISTS( SELECT * FROM AMOURS A1 WHERE A1.nomAime = D1.nom AND 
		NOT EXISTS ( SELECT * FROM REPAS WHERE A1.nomAimant = nomD ));


-- QUESTION 24 :
--V1 : 
SELECT D1.nom FROM DRAGONS D1, AMOURS A1 WHERE D1.sexe = 'F'
	AND D1.nom=A1.nomAime
GROUP BY D1.nom
HAVING COUNT(A1.nomAimant)=(SELECT COUNT(*) FROM AMOURS A2 WHERE A2.nomAime=D1.nom AND A2.nomAimant NOT IN(SELECT nomD FROM REPAS));

--V2 : 
--Dragon aimant une D1
(SELECT D1.nom FROM DRAGONS D1, AMOURS A1 WHERE D1.sexe = 'F'
	AND D1.nom=A1.nomAime
	GROUP BY D1.nom)
MINUS
-- Dragon aimant une D1 ne faisant pas la grève de la fin
(SELECT D1.nom FROM DRAGONS D1 WHERE D1.sexe = 'F' 
	AND EXISTS( SELECT * FROM AMOURS A1 WHERE A1.nomAime = D1.nom AND 
		EXISTS ( SELECT * FROM REPAS WHERE A1.nomAimant = nomD ))
)

-- QUESTION 25 :
SELECT D1.nom, D2.nom FROM DRAGONS D1, DRAGONS D2
WHERE mod(D1.ecailles + D2.ecailles,2) = 0
AND D1.enAmour = D2.enAmour
AND D1.sexe = 'F' AND D2.sexe = 'M'
GROUP BY D1.nom, D2.nom;

