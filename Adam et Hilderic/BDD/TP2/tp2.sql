--start /home/etud/info/adreyes1/Cours/BDD/TP2/tp2.sql
DROP TABLE NOURRITURES;
DROP TABLE AMOURS;
DROP TABLE DRAGONS;

CREATE TABLE DRAGONS(
	nom VARCHAR(15) CONSTRAINT nomD PRIMARY KEY,
	sexe CHAR(1) CONSTRAINT sexeD CHECK(sexe IN('F','M')),
	longueur NUMBER(4) CONSTRAINT longueurD CHECK(longueur > 0),
	ecailles NUMBER(4) CONSTRAINT ecaillesD CHECK(ecailles >=0),
	cracheFeu CHAR(1) CONSTRAINT cracheFeuD CHECK(cracheFeu IN('O', 'N')),
	enAmour VARCHAR(10) CONSTRAINT enAmourD CHECK(enAmour IN('macho', 'timide', 'sincere', 'volage'))
);

INSERT INTO DRAGONS VALUES('Smeagol','M', 152, 1857, 'O', 'macho');
INSERT INTO DRAGONS VALUES('Birdurh','M', 258, 4787, 'N', 'timide');
INSERT INTO DRAGONS VALUES('Negueth','F', 128, 1582, 'O', 'sincere');
INSERT INTO DRAGONS VALUES('MissToc','F', 183, 2781, 'N', 'volage');
INSERT INTO DRAGONS VALUES('Bolong','M', 213, 2754, 'O', 'macho');
INSERT INTO DRAGONS VALUES('Miloch','M', 83, 718, 'O', 'timide');
INSERT INTO DRAGONS VALUES('Nessie','M', 168, 1721, 'N', 'macho');
INSERT INTO DRAGONS VALUES('Tarak','F', 123, 851, 'O', 'timide');
INSERT INTO DRAGONS VALUES('Solong','M', 173, 1481, 'O', 'timide');

CREATE TABLE NOURRITURES(
	produit VARCHAR(15),
	calories NUMBER(5) CONSTRAINT caloriesN CHECK(calories > 0)
);

INSERT INTO NOURRITURES VALUES('pomme', 7);
INSERT INTO NOURRITURES VALUES('cacahuete', 10);
INSERT INTO NOURRITURES VALUES('orange', 25);
INSERT INTO NOURRITURES VALUES('oeuf', 15);
INSERT INTO NOURRITURES VALUES('ver', 3);
INSERT INTO NOURRITURES VALUES('poisson', 35);

CREATE TABLE Amours(
	nomAimant VARCHAR(15) CONSTRAINT nomAimantA PRIMARY KEY,
	nomAime VARCHAR(15),
	force VARCHAR(15) CONSTRAINT forceA CHECK(force IN('un peu', 'beaucoup', 'passionnement', 'a la folie'))
);

INSERT INTO Amours VALUES('Smeagol', 'Tarak', 'passionnement');
INSERT INTO Amours VALUES('Birdurh', 'Negueth', 'beaucoup');
INSERT INTO Amours VALUES('Negueth', 'Miloch', 'a la folie');
INSERT INTO Amours VALUES('Miloch', 'Negueth', 'a la folie');
INSERT INTO Amours VALUES('Tarak', 'Bolong', 'un peu');
INSERT INTO Amours VALUES('Bolong', 'Tarak', 'beaucoup');
INSERT INTO Amours VALUES('Nessie', 'Tarak', 'un peu');

CREATE TABLE REPAS(
	nomD VARCHAR(15) CONSTRAINT nomRepas REFERENCES DRAGONS,
	produit VARCHAR(15) CONSTRAINT nomproduit REFERENCES NOURRITURES,
	quantite NUMBER(5) CONSTRAINT quantiteR CHECK(quantite > 0),
	CONSTRAINT keyR PRIMARY KEY(nomD, produit)
);

INSERT INTO REPAS VALUES('Smeagol','cacahuete', 1000);
INSERT INTO REPAS VALUES('Smeagol','pomme', 16);
INSERT INTO REPAS VALUES('Bolong','oeuf', 4);
INSERT INTO REPAS VALUES('Negueth','orange', 6);
INSERT INTO REPAS VALUES('Negueth','oeuf', 1);
INSERT INTO REPAS VALUES('Miloch','ver', 53);
INSERT INTO REPAS VALUES('Miloch','cacahuete', 100);
INSERT INTO REPAS VALUES('Nessie','poisson', 20);
INSERT INTO REPAS VALUES('Tarak','pomme', 10);
INSERT INTO REPAS VALUES('Tarak','orange', 10);
INSERT INTO REPAS VALUES('Solong','oeuf', 6);
INSERT INTO REPAS VALUES('Solong','poisson', 1);
INSERT INTO REPAS VALUES('Solong','orange', 2);



	 


