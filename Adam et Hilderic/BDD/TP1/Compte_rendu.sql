Scéance 1 :

3.1)

select * from cat; => no rows selected 
select * from user_catalog; => no rows selected 

3.2)

CREATE TABLE Client (
	referenceC CHAR(5) CONSTRAINT cle PRIMARY KEY,
	nom VARCHAR2(15) CONSTRAINT nom NOT NULL,
	rue VARCHAR2(20),
	codePostal NUMBER(5),
	ville VARCHAR2(20),
	typeClient NUMBER(1) CONSTRAINT typeClient1 NOT NULL CONSTRAINT typeClient2 CHECK(typeClient IN ('0','1','2','3')),
	codeTarif CHAR(1) CONSTRAINT codeTarif1 NOT NULL CONSTRAINT codeTarif2 CHECK(codeTarif IN('N','G'))
)

3.3)

select * from cat;
TABLE_NAME
--------------------------------------------------------------------------------
TABLE_TYPE
-----------
CLIENT
TABLE


desc client;

 Name								   Null?    Type
 ----------------------------------------------------------------- -------- --------------------------------------------
 REFERENCEC							   NOT NULL CHAR(5)
 NOM								   NOT NULL VARCHAR2(15)
 RUE									    VARCHAR2(20)
 CODEPOSTAL								    NUMBER(5)
 VILLE									    VARCHAR2(20)
 TYPECLIENT							   NOT NULL NUMBER(1)
 CODETARIF							   NOT NULL CHAR(1)

ORA-00955: name is already used by an existing object

3.4)
INSERT INTO Client VALUES('C0010', 'Linaeween', 'lol', 22700, 'Perros-Guirec', 4, 'G');
ERROR at line 1:
ORA-02290: check constraint (ADREYES1.TYPECLIENT2) violated
Car le typeclient ne peut être compris que entre 0 et 3.
INSERT INTO Client VALUES('C0010', 'Linaeween', 'lol', 22700, 'Perros-Guirec', 3, 'G');

INSERT INTO Client VALUES('C002', 'Emeldiz', 'penis', 22500, 'Paimpol', 0, 'N');
Ok.

INSERT INTO Client VALUES('C0030', 'Aegnor', 'mdr', 44490, 'Le Croisic', 1, 'N');
ok.

INSERT INTO Client VALUES(NULL, 'Amandil', 'ptdr', 29900, 'Concarneau', 0, 'G');
ERROR at line 1:
ORA-01400: cannot insert NULL into ("ADREYES1"."CLIENT"."REFERENCEC")
Réference est la clé donc elle doit être donné obligatoirement.
INSERT INTO Client VALUES('C001', 'Amandil', 'ptdr', 29900, 'Concarneau', 0, 'G');

INSERT INTO Client VALUES('C0010', 'Daeron', 'okay', 22500, 'Paimpol', 1, 'N');
ERROR at line 1:
ORA-00001: unique constraint (ADREYES1.CLE) violated
La référence doit être unique donc il faut la modifier.
INSERT INTO Client VALUES('C0011', 'Daeron', 'okay', 22500, 'Paimpol', 1, 'N');
Ok.

INSERT INTO Client VALUES('C0060', 'Linaeween', 'dac', 29750, 'Loctudy', 2, 'G');
Ok.

3.5)
UPDATE Client SET referenceC = 'C0099' WHERE referenceC = 'C0010';

DELETE FROM Client WHERE nom = 'Linaeween';

DELETE FROM Client WHERE codeTarif = 'N';

DELETE FROM Client;

DROP TABLE CLIENT PURGE;

start /home/etud/info/adreyes1/Cours/BDD/TP1/sorcier.sql

3.7)

select constraint_name, constraint_type, search_condition from user_constraints where table_name='CLIENT';

CONSTRAINT_NAM 											 C
-------------------------------------------------------------------------------------------------------------------------------- -
SEARCH_CONDITION
--------------------------------------------------------------------------------
CLE													 P


CODETARIF2												 C
codeTarif IN('N','G')

TYPECLIENT2												 C
typeClient IN ('0','1','2','3')

CODETARIF1												 C
"CODETARIF" IS NOT NULL

TYPECLIENT1												 C
"TYPECLIENT" IS NOT NULL

NOM													 C
"NOM" IS NOT NULL

6 rows selected.

ALTER TABLE CLIENT DISABLE CONSTRAINT typeClient1;
ALTER TABLE CLIENT DISABLE CONSTRAINT typeClient2;
CONSTRAINT_NAME 											 C
-------------------------------------------------------------------------------------------------------------------------------- -
SEARCH_CONDITION
--------------------------------------------------------------------------------
CODETARIF2												 C
codeTarif IN('N','G')

CODETARIF1												 C
"CODETARIF" IS NOT NULL

NOM													 C
"NOM" IS NOT NULL

CLE													 P


TYPECLIENT2												 C
typeClient IN ('0','1','2','3')

TYPECLIENT1												 C
"TYPECLIENT" IS NOT NULL


6 rows selected.

INSERT INTO Client VALUES('C006', 'Jean-Charles', 'dac', 6969, 'Loctudyr', 5, 'G');

ALTER TABLE CLIENT ENABLE CONSTRAINT typeClient1;
ALTER TABLE CLIENT ENABLE CONSTRAINT typeClient2;
ERROR at line 1:
ORA-02293: cannot validate (ADREYES1.TYPECLIENT2) - check constraint violated
On ne peut remettre la contrainte car elle n''est pas respecté dans la table actuelle.

ALTER TABLE client ADD dateArrivee DATE CONSTRAINT dateArrivee NOT NULL;
ORA-01758: table must be empty to add mandatory (NOT NULL) column
ALTER TABLE client ADD dateArrivee DATE CONSTRAINT dateArrivee NULL;

ALTER TABLE client DELETE dateArrivee;

CREATE TABLE ClientT AS SELECT referenceC, nom, rue, codePostal, ville, typeClient, codeTarif FROM CLIENT;
Drop TABLE CLIENT purge;
RENAME CLIENTT TO CLIENT;

ALTER TABlE client ADD CONSTRAINT nomCP UNIQUE(nom,codePostal);
Une contrainte U est apparu à la fin

ALTER TABLE client ADD CONSTRAINT tarifG CHECK(codeTarif='G' and typeClient IN ('1','2','3') OR codeTarif='N');

Scéance 2 :

4°)
ORA-00001: unique constraint (ADREYES1.NOMF) violated
ORA-12899: value too large for column "ADREYES1"."FAMILLE"."CODE" (actual: 6,
maximum: 5)
ORA-01400: cannot insert NULL into ("ADREYES1"."FAMILLE"."CODE")

CREATE TABLE temp AS SELECT referenceP, designation, stock, codeEtat, prixNormal, prixGrossiste FROM Produit;
DROP TABLE Produit Purge;
RENAME temp TO Produit;
DROP TABLE FAMILLE PURGE;


