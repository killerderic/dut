set pagesize 50;
set linesize 150;

DROP TABLE LIGNEVENTE PURGE;
DROP TABLE VENTE PURGE;
DROP TABLE PRODUIT PURGE;
DROP TABLE CLIENT PURGE;
DROP TABLE FAMILLE PURGE;
---------------------------------------------------------------------------------------
CREATE TABLE Client(
	referenceC CHAR(5) CONSTRAINT cleC PRIMARY KEY,
	nom VARCHAR2(15) CONSTRAINT nomC NOT NULL,
	rue VARCHAR2(20),
	codePostal NUMBER(5),
	ville VARCHAR2(20),
	typeClient NUMBER(1) CONSTRAINT typeClient1 NOT NULL CONSTRAINT typeClient2 CHECK(typeClient IN ('0','1','2','3')),
	codeTarif CHAR(1) CONSTRAINT codeTarif1 NOT NULL CONSTRAINT codeTarif2 CHECK(codeTarif IN('N','G')));

ALTER TABLE client ADD CONSTRAINT tarifG CHECK(codeTarif='G' and typeClient IN ('1','2','3') OR codeTarif='N');

INSERT INTO Client VALUES('C0010', 'Linaeween', 'lol', 22700, 'Perros-Guirec', 3, 'G');
INSERT INTO Client VALUES('C002', 'Emeldiz', 'penis', 22500, 'Paimpol', 0, 'N');
INSERT INTO Client VALUES('C0030', 'Aegnor', 'mdr', 44490, 'Le Croisic', 1, 'N');
INSERT INTO Client VALUES('C001', 'Amandil', 'ptdr', 29900, 'Concarneau', 1, 'G');
INSERT INTO Client VALUES('C0011', 'Daeron', 'okay', 22500, 'Paimpol', 1, 'N');
INSERT INTO Client VALUES('C0060', 'Linaeween', 'dac', 29750, 'Loctudy', 2, 'G');
select * from Client;

---------------------------------------------------------------------------------------
CREATE TABLE Famille(
	code CHAR(5) CONSTRAINT cleF PRIMARY KEY,
	nom VARCHAR(15) CONSTRAINT nomF UNIQUE
);

INSERT INTO Famille VALUES('F0040','Baguette');
INSERT INTO Famille VALUES('F0020','Grimoire');
INSERT INTO Famille VALUES('F0030', 'Encensoir');
INSERT INTO Famille VALUES('F0050', 'Athame');
INSERT INTO Famille VALUES('F0010', 'Coupe');
INSERT INTO Famille VALUES('F0060', 'Balai');
DELETE FROM Famille WHERE code='F0060';
select * from Famille;
---------------------------------------------------------------------------------------
CREATE TABLE Produit(
	referenceP CHAR(5) CONSTRAINT cleP PRIMARY KEY,
	designation VARCHAR(15) CONSTRAINT designation UNIQUE,
	stock NUMBER(10) CONSTRAINT stock CHECK(stock >= 0),
	codeEtat CHAR(1) CONSTRAINT codeEtat CHECK (codeEtat IN ('E','D')),
	prixNormal NUMBER(6,2),
	prixGrossiste NUMBER(6,2),
	codeFamille CHAR(5) CONSTRAINT codeFamille REFERENCES Famille
);
ALTER TABLE Produit ADD CONSTRAINT prixSup CHECK(prixNormal >= prixGrossiste);

INSERT INTO Produit VALUES('P0010', 'Baguette HarryP', 10, 'D', 175, 150, 'F0040');
INSERT INTO Produit VALUES('P0020', 'Baguette AlbusD', 0, 'E', 200.45, 175.50, 'F0040');
INSERT INTO Produit VALUES('P0030', 'Sortilege Al', 25, 'D', 35, 30, 'F0020');
INSERT INTO Produit VALUES('P0040', 'Coupe de feu', 5, 'D', 75, 65, 'F0010');
INSERT INTO Produit VALUES('P0050', 'Grimoire Gryff', 8, 'D', 50, 40, 'F0020');
INSERT INTO Produit VALUES('P0060', 'AthamePanoramix', 0, 'E', 67, 56, 'F0040');

select * from Produit;
--------------------------------------------------------------------------------------------

DELETE FROM Produit WhErE codeFamille = 'F0010';
DELETE FROM Famille WHERE code = 'F0010';

UPDATE PRODUIT SET prixGrossiste = prixGrossiste - 5; 

DELETE FROM Produit WhErE codeFamille = 'F0020';
DELETE FROM Famille WHERE code = 'F0020';

ALTER TABLE produit ADD CONSTRAINT etatStock CHECK(codeEtat='E' and stock=0 OR codeEtat='D');

--------------------------------------------------------------------------------------------
CREATE TABLE Vente(
	numeroV NUMBER(10) CONSTRAINT keyV PRIMARY KEY,
	dateV DATE,
	referenceC CHAR(5) CONSTRAINT Vente REFERENCES Client
);

CREATE TABLE LigneVente(
	referenceP CHAR(5) CONSTRAINT keyLV1 REFERENCES Produit,
	numero NUMBER(10) CONSTRAINT keyLV2 REFERENCES Vente,
	quantite NUMBER(10),
	CONSTRAINT keyLV PRIMARY KEY(referenceP, numero)
);

INSERT INTO VENTE VALUES(1, SYSDATE, 'C0060');


commit work;
