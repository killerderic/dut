#!/bin/bash
r=0
oldUserCount=$(users | wc -w)

while [ $r -ne 1 ]; do

	usercount=$(users | wc -w)
	
	if [ $oldUserCount != $usercount ]; then
	
		users=$(users)
		for user in $users; do
			if [ $user == $1 ]; then
				echo " User $1 connected "
				r=1
				break
			fi
			
		done
		
		oldUserCount=$userCount
		

	fi
	

	sleep 2

done

