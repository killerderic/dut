#! /bin/bash

langue='fr'
displayTime='false'
fullyear='false'

for arg; do
	case $arg in
		'-lang')
				if [ $2 == 'fr' ]; then
					 langue='fr'
				else
					langue='en'
				fi
				shift
				;;
		'-fullyear')
				fullyear='true'
				;;
		'-addTime')
				displayTime='true'
				;;
	esac
done

jour=$(date | cut -d" " -f2)
mois=$(date | cut -d" " -f3)
annee=$(date +%y)
hour=''
if [ $displayTime == 'true' ]; then
	hour=$(date | cut -d" " -f5)
fi
if [ $fullyear == 'true' ]; then
	annee=$(date | cut -d" " -f4)
fi


dateFR="$jour $mois $annee $hour"
dateEN="$mois $jour $annee $hour"

if [ $langue == 'en' ]; then
	result=$dateEN
else
	result=$dateFR
fi

echo $result
