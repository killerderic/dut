#!/bin/bash
n=0
until [ $n == 1 ]
	do
	clear
	echo "************************************************"
	echo ""
	echo "Saisissez une commande, taper Q poour quitter."
	echo ""
	echo "************************************************"
	echo ""
	read cmd
	if [ $cmd == Q ] 2> /dev/null
		then	
		exit
	else	
		echo -n "Resultat de la commande : "
		$cmd
		if [ ! $cmd ] 2> /dev/null
			then
			echo "ERREUR COMMANDE INTROUVABLE"
		fi
	fi
	echo -n "Appuyer sur entrée pour continuer. "
	read fin
done
