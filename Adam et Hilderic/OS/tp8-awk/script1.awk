BEGIN {FS=":"
print "Calcul moyenne des étudiants de l'option SI"
nbEtd=0
somme=0}
 
 
$3 ~ /SI/ { M=($4+$5)/2
         printf ("%s %s %.1f\n", $1, $2, M )
        nbEtd++
        somme+=M
        }
 
END {print "la moyenne du groupe SI est de", somme/nbEtd}
