BEGIN {
FS=":"
print "Elève ayant fait des progès  :"
n=0
somme=0
}
 
 
$3 ~ /GI2/ || /GI1/ {
	somme++
	if($5 > $4){
		n++
		printf ("%s %s\n", $1, $2)
	}
}
 
END {
r = (n / somme)*100
print "le % d'élève ayant progressé est ", r
}
