

A – Quelle commande permet de déplacer des fichiers ? illustrez.

	-"mv question1/fichier1.txt question2/"

B – Quelle commande permet de renommer un fichier ? illustrez.

	-"mv question1/fichier1.txt question2/.fichier1.txt"

C – Comment afficher le manuel de la commande C printf au lieu de celui de la commande Unix printf ? (regardez dans man man)

	-"man 3 printf"

D – Où se trouve le programme correspondant à la commande ls ? (commande which)

	-/bin/ls

E – Quel type de données contiennent les fichiers suivants ? /sbin/ifconfig et /etc/hosts ? (commande file)

	-Exécutable

F – Quelle est la taille occupée par votre répertoire home et ses sous répertoires ? (commandes « du » et « quota » )

	-9676
	

