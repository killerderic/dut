set echo off
set verify off
set feed off

/* start \\HINA\adreyes1\Cours\PLSQL\TP1\EXO3.sql */

drop table tligne purge;
create table tligne( ligne varchar2(200));

variable vrefLog CHAR(4)
variable vdate NUMBER
variable vnumC CHAR(6)

prompt Logement a modfifier :
accept vreflog

prompt Date (jjmm) :
accept vdate

prompt Numero client nouveau locataire :
accept vnumC

declare

dnbrCl NUMBER
dnbrLog NUMBER
dnoloc CHAR(6)

begin

SELECT COUNT (*) 
INTO dnbrCl
FROM tclient2016
WHERE nocli = '&vnumC';

SELECT COUNT (*)
INTO dnbrLog
FROM tlogt2016
WHERE reflog = '&vreflog';

IF dnbrCl > 0
THEN
	IF dnbrLog > 0
	THEN
		SELECT noloc
		INTO dnoloc
		FROM tlogt2016
		WHERE

exception

when no_data_found then 
INSERT INTO tligne VALUES('Aucune location pour ce client');

when too_many_rows then 
INSERT INTO tligne VALUES('Plusieurs location pour ce client');

END;
.
/

select * from tligne;

prompt Reference du logement et son loyer
print vreflog vloyer

set echo on
set verify on
set feed on