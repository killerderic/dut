set echo off
set verify off
set feed off

/* start U:\Cours\PLSQL\TP2\TP2exo1.sql */

DROP TABLE tligne PURGE;
CREATE TABLE tligne( ligne VARCHAR2(200));

variable vnoimm CHAR(4)

prompt Entrez le numero de l'immeuble :
accept vnoimm

declare

dnoimm CHAR(4);
dadrimm VARCHAR2(80);
dreflog CHAR(4);
dsuperf NUMBER;
dloyer NUMBER;
dmessage VARCHAR2(150);

CURSOR r IS

SELECT reflog, superf, loyer
FROM timm2016 I, tlogt2016 L
WHERE I.noimm = '&vnoImm'
AND I.noimm = L.noimm
AND etat = 'D';

begin

dmessage := 'Immeuble inconnue';

SELECT noimm, adrimm INTO dnoimm, dadrimm
FROM timm2016
WHERE noimm = '&vnoImm' ;

dmessage := 'Pas de logement libre dans cet immeuble';

OPEN r;
FETCH r INTO dreflog, dsuperf, dloyer;

IF(r%FOUND)
THEN
	INSERT INTO tligne VALUES('Immeuble : ' || dnoimm || ' ' || dadrimm);
	INSERT INTO tligne VALUES(' ');
	INSERT INTO tligne VALUES('RefLog          Superficie         Loyer');
	WHILE r%FOUND
	LOOP
		INSERT INTO tligne VALUES(dreflog|| ' 	  	  ' || dsuperf  || '	       	' ||  TO_CHAR(dloyer, '999,v99'));
		FETCH r INTO dreflog, dsuperf, dloyer;
		
	END LOOP;
	CLOSE r;
 
ELSE
	insert into tligne values(dmessage);
END IF;


exception 

when no_data_found then
	insert into tligne values(dmessage);

END;
.
/

SELECT * FROM tligne;

set echo on;
set verify on;
set feed on;