set echo off
set verify off
set feed off

/* start U:\Cours\PLSQL\TP2\TP2exo2.sql */

DROP TABLE tligne PURGE;
CREATE TABLE tligne( ligne VARCHAR2(200));

variable vnoprop CHAR(6)

prompt Entrez le numero du propriétaire :
accept vnoprop

DECLARE

dnoprop CHAR(6);
dnomprop VARCHAR2(20);
ddepartement CHAR(2);
dreflog CHAR(4);
dnoimm CHAR(4);
detatlog CHAR(1);
dnblog NUMBER;

CURSOR r IS

SELECT I.deptimm, I.noimm, L.reflog, L.etat
FROM timm2016 I, tlogt2016 L
WHERE L.noimm = I.noimm
	AND L.noprop = '&vnoprop'
ORDER BY I.deptimm, I.noimm;

BEGIN

SELECT COUNT(*) INTO dnblog
FROM tlogt2016 
WHERE noprop = '&vnoprop';

SELECT nomcli INTO dnomprop
FROM tclient2016
WHERE nocli = '&vnoprop';

OPEN r;
FETCH r INTO ddepartement, dnoimm, dreflog, detatlog;
	
INSERT INTO tligne VALUES('Propriétaire : ' || '&vnoprop' || ' ' || dnomprop);
INSERT INTO tligne VALUES(' ');
INSERT INTO tligne VALUES('Département 	num imm		reflog	etat log');
WHILE r%FOUND
LOOP
	INSERT INTO tligne VALUES(ddepartement || '		' || dnoimm || '		' ||  dreflog || '		' || detatlog);
	FETCH r INTO ddepartement, dnoimm, dreflog, detatlog;
		
END LOOP;
INSERT INTO tligne VALUES(' ');
INSERT INTO tligne VALUES('Nombre de logement possédés : ' || dnblog);
CLOSE r;

EXCEPTION

when no_data_found then
	insert into tligne values('Pas de logement');

END;
.
/

SELECT * FROM tligne;

set echo on;
set verify on;
set feed on;