set echo off
set verify off
set feed off

/* start \\HINA\adreyes1\Cours\PLSQL\TP1\EXO2.sql */

drop table tligne purge;
create table tligne( ligne varchar2(200));

variable vrefLog CHAR(4)
variable vloyer NUMBER
variable numC CHAR(6)


prompt Numero client :
accept numC

begin

SELECT reflog, loyer
INTO :vreflog, :vloyer
FROM tlogt2016
WHERE noloc = '&numC';

INSERT INTO tligne VALUES('Informations trouv�');

exception

when no_data_found then 
INSERT INTO tligne VALUES('Aucune location pour ce client');

when too_many_rows then 
INSERT INTO tligne VALUES('Plusieurs location pour ce client');

END;
.
/

select * from tligne;

prompt Reference du logement et son loyer
print vreflog vloyer

set echo on
set verify on
set feed on