set echo off
set verify off
set feed off

Drop Table terreur purge;
Create Table terreur(
	erreur VARCHAR2(500)
);



variable vnumcli CHAR(6)
prompt Entrez le num du client :
accept vnumcli

declare

	dnomcli VARCHAR2(20);
	derreur VARCHAR2(200);
	dnumcli CHAR(6);
	dnbLoc Number;
	dnbLog Number;
	prb_count EXCEPTION;
	dreflog CHAR(4);

	CURSOR r IS 
	SELECT noloc, reflog FROM tlogt2016 WHERE noloc = dnumcli;

begin
	dnumcli := '&vnumcli';

	derreur := 'Pas de client trouve';
	Select nomcli into dnomcli From tclient2016 where nocli = dnumcli;

	derreur := 'Le client est proprietaire';
	SELECT COUNT(*) into dnbLog FROM tlogt2016 WHERE noprop = dnumcli;
	IF dnbLog > 0 THEN
		RAISE prb_count;
	ELSE
		derreur := ' Client supprime en tant que locataire du logement : ';

		OPEN r;
		FETCH r INTO dnumcli, dreflog;
		WHILE r%FOUND
		LOOP
			UPDATE tlogt2016 SET etat = 'D', noloc = NULL WHERE reflog = dreflog;

			derreur := derreur || dreflog || ' ' ;
			FETCH r INTO dnumcli, dreflog;
		END LOOP;
		CLOSE r;
		INSERT INTO terreur VALUES(derreur);

	END IF;

	derreur := 'Client ayant aucune location en cours ou ancienne';
	SELECT Count(*) into dnbLoc FROM tlocation2016 Where noloc = dnumcli;

	IF dnbLoc = 0 THEN
		RAISE prb_count;
	ELSE
		DELETE FROM tlocation2016 Where noloc = dnumcli;
		INSERT INTO terreur VALUES (' Client supprime en tant que locataire');
	END IF;

	DELETE FROM tclient2016 WHERE nocli = dnumcli;

	derreur := 'Pas d''erreur';
	INSERT INTO terreur VALUES(derreur);
	
EXCEPTION
	WHEN NO_DATA_FOUND THEN
		INSERT INTO terreur Values (derreur);
	WHEN prb_count THEN
		INSERT INTO terreur Values (derreur);

END;
.
/

SELECT * from terreur;

/* start U:\cours\pl-sql\TP3.sql; */

/* start \\kirov\Ensinfo\BD2015-2016\gestionimmo2016.sql */