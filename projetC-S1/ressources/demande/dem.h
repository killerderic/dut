#include "../tools.h"

Demande lireDemande(FILE *fe);
ListeDem chargDemande(int *nbDem);
ListeDem inserEnTete(ListeDem lDem, Demande dem);

void affichDem(ListeDem lDem);
void affich1Dem(Demande dem);
void affichDemEtudiant(ListeDem lDem, int id);

void testDem(void);
Date getDateActuelle();
ListeDem suppressionEnTete(ListeDem l);

/**
    Fonction pour r�cup donn�e dans les demandes
*/
Etudiant ** getEtudInLDem(ListeDem lDem, Etudiant ** etudT, int nbe, int * nbeInListe, int * position);

/**
   Fonction pour get des demandes
*/
MaillonDem * getDemandeByEtudiantId(ListeDem lDem, int id);

/**
    Fonction pour mettre une demande en attente
**/

void miseAttenteDem(ListeDem lDem, Demande dem);

/**
    STRUCTURE ET FONCTIONS POUR TRI DEMANDES PAR ECHELON
**/

typedef struct //Permet de relier la demande d'un etudiant avec son echelon.
{
    Demande dem;
    int echelon;
}DemEchelon;

void affichDemEchelon(ListeDem lDem, Etudiant *tabEtud[], int nbEtud);
DemEchelon * getTabDemEchelon(ListeDem lDem, Etudiant *tabEtud[], int nbEtud, DemEchelon *tabDemEchelon, int *nbDem);

void triDemEchelon(DemEchelon tabDemEchelon[], int nbDem);
void permutationDem(DemEchelon tabDemEchelon[], int pos1, int pos2);

void sauvegardeDem(ListeDem listeDem, FILE *fs);

/**
    EMISSION D'UNE DEMANDE
**/

ListeDem enregistrDemande(ListeDem lDem, Etudiant user, int *nbDem, Logement tabL[], int nbL);
ListeDem creeDemande(ListeDem lDem, Etudiant etud, Logement log, int * nbDemande);

/**
    LIBERATION DE MEMOIRE
**/

ListeDem freeDem(ListeDem l);
