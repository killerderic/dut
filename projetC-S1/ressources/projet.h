#include "etudiant/etud.h"
#include "logement/log.h"
#include "demande/dem.h"
#include "gerant/gerant.h"
#include "tools.h"

void mainMenu();


char testEtudOuGerent();
ListeDem menuEtud(Etudiant ** tabEtud, int nbEtud, Etudiant user, Logement *tLog, int nbLog, ListeDem lDem, int *nbDem);
ListeDem menuGerant(Gerant tabGer[], int nbGer, Etudiant ** tabEtud, int nbEtud, Logement *tLog, int *nbLog, ListeDem listDem, int *nbDem);
void sauvegardGlobal(Etudiant *tabEtud[], int nbe, Gerant *tabGer, int nbGer,Logement *tLog, int nbLog, ListeDem listDem, int nbDem);



