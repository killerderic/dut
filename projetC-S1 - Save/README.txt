Pour r�cup�rer le projet : 
	Aller sur https://bitbucket.org/killerderic/dut/commits/all
	Dl le projet en cliquant sur download ( racourcis : appuyer sur R et D , puis cliquer sur dowload repository )

Ou est le projet ?
	dut/projetC-S1 - Save

Aller dans le dossier projetC-S1-SAVE (important d'aller dans save car projetC-S1 ne marche pas)

Code bloks avec minGw ( m�me compilateur qu'en cours ) : http://sourceforge.net/projects/codeblocks/files/Binaries/13.12/Windows/codeblocks-13.12mingw-setup.exe/download

Une fois CodeBlocks install�, ouvrir le projet en cliquant sur projetC-S1.cbp.
Pour compiler et �xecuter le programme presser F9, ou cliquer sur l'icone avec un engrenage et une flcche verte dans la seconde barre en dessous de la barre avec file, edit, ...

**** IMPORTANT : ****
Si lors du menu �tudiant, on tape un nom d'�tudiant inexistant, cela demande de s'inscrire.
Detection d'un bug faisant planter le programme (impossible de la debugger car plus de temps)
Il faut donc mettre N si l'on demande de s'inscrire
Le mieux �tant de mettre un nom d'�tudiant existant comme COIFFARD Pierre et lors de la demande de la date de naissance mettre 9 2 1997
SI jamais le programme ne veut afficher aucun menu lorsque l'on tape e, g ou d
	Fermer le programme aller dans le dossier donnees et copier les fichier du dossier saveFic dans le fichier donnees (ecraser les anciens fichiers)

Si jamais vous rencontrez le moindre probl�me lors de l'�xecution du code vous pouvez me contacter (Pierre COIFFARD) au 06.59.71.20.62


Guide pour faire fonctionner le projet :

Pour retrouver rapidement une d�claration/impl�mentation sur codeBlock, cliquer sur le nom de la fonction et faire find declaration ou find implementation.


pour se connecter en tant que g�rant : 
LOGIN : root
MOT DE PASSE : 123