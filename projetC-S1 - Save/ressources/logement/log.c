#include "log.h"
/**
    Menu
*/
void rchLogementMenu(Etudiant user){
    int nbLog, nbLogAdapte;
    char typepref[11];
    char citepref[6];
    Logement * allLogT;
    Logement * logAdapteT;

    //D'abord r�cup�rer tous les logements dans un tableau
    allLogT = chargLogement(&nbLog);

    //Ensuite regarder sur l'�tudiant veut un type de logement en particulier
    entreeTypeLogPrefMenu(typepref, 1);
    system("cls");
    entreeCitePrefMenu(citepref, 1);

    //Recuperer les logements adaptes a la demande
    logAdapteT = getLogAdapte(allLogT, nbLog, user, &nbLogAdapte, typepref, citepref);

    //Et les afficher
    if(nbLogAdapte == 0){
        printf("Aucun logement disponible pour vous \n");
    }else{
        system("cls");
        affichLog(logAdapteT, nbLogAdapte);
    }

}

void entreeTypeLogPrefMenu(char * type, int prefOK){ //la var prefOK prend la valeur 1 si on souhaite le cas "aucune pr�f�rences", 0 sino
    int choix=0;
    do{
        printf("\t\t\t\tRECHERCHER UN LOGEMENT\n\n");
        printf(" Avez vous un type de logement prefere ? \n");
        printf("\t1) Chambre\n");
        printf("\t2) Studio\n");
        printf("\t3) T1\n");
        printf("\t4) T2\n");
        if(prefOK)
            printf("\t5) Aucune preference\n");
        printf("\n Indiquez votre choix : ");
        scanf("%d%*c", &choix);

        switch(choix){
        case 1:
            strcpy(type, "Chambre");
            break;
        case 2:
            strcpy(type, "studio");
            break;
        case 3:
            strcpy(type, "T1");
            break;
        case 4:
            strcpy(type, "T2");
            break;
        case 5:
            if(prefOK)
                strcpy(type, "null");
            else {
                printf("reponse non comprise \n");
                choix = 0;}
            break;
        default:
            printf("reponse non comprise \n");
            choix = 0;
            break;
        }
    }while(choix == 0);

}

void entreeCitePrefMenu(char * cite, int prefOK){ //la var prefOK prend la valeur 1 si on souhaite le cas "aucune pr�f�rences", 0 sinon
    int choix=0;
    do{
        printf(" Avez vous une cite prefere ? \n");
        if(prefOK)
            printf("\t0) Aucune preferences\n");
        printf("\t1) STUD'CITY Residence Jules VERNE\n");
        printf("\t2) STUD'CITY Residence Sacha Guitry\n");
        printf("\t3) STUD'CITY Residence Saint Exupery\n");
        printf("\t4) STUD'CITY Residence George SAND\n");
        printf("\t5) STUD'CITY Residence Jacques PREVERT\n");
        printf("\t6) STUD'CITY Residence Boris VIAN\n");
        printf("\n Indiquez votre choix : ");
        scanf("%d%*c", &choix);

        switch(choix){
        case 0:
            if(prefOK)
                strcpy(cite, "null");
            else {
                printf("Reponse non comprise \n");
                choix = -1;
            }
            break;
        case 1:
            strcpy(cite, "c01");
            break;
        case 2:
            strcpy(cite, "c02");
            break;
        case 3:
            strcpy(cite, "c03");
            break;
        case 4:
            strcpy(cite, "c04");
            break;
        case 5:
            strcpy(cite, "c05");
            break;
        case 6:
            strcpy(cite, "c06");
            break;
        default:
            printf("Reponse non comprise \n");
            choix = -1;
            break;
        }
    }while(choix == -1);

}

void mainModifTravauxLog(Logement *tLog, int nbLog)
{
    char num[42];
    int exist, pos;

    printf("\t\t\tAJOUTER/ENLEVER TRAVAUX D'UN LOGEMENT\n\n");

    do{ //On demande le numero du logement tant que celui rentre n'existe pas.
        printf("\nEntrez le numero du logement a modifier (c0XXXXX) : ");
        scanf("%s", num);
        exist = logExiste(tLog, nbLog, num);
    }while(exist == 0);

    //On recherche la position du logement rentre dans le tableau.
    pos=posLogementTab(tLog, nbLog, num);

    //On verifie son etat actuel et on l'inverse.
    if(tLog[pos].etat == 1){
        tLog[pos].etat = 2;
    }else if(tLog[pos].etat == 2){
        tLog[pos].etat = 1;
    }else{
        printf("\nImpossible de mettre ce logement en travaux, il est occupe !\n");
    }
}

/**
    Fonction de chargement
*/
Logement * chargLogement(int * nb){
    FILE * fs;
    Logement * logT, log;
    int i;
    fs = fopen("donnees/logement.don", "r");
    if(fs == NULL) { printf("\nProbleme ouverture fichier"); exit(1);}
    fscanf(fs, "%d", nb);
    logT = (Logement * ) malloc(sizeof(Logement) * *nb);
    for(i = 0; i < *nb; i++){
        log = LireLog(fs);
        logT[i] = log;
    }
    fclose(fs);
    return logT;
}

Logement LireLog(FILE * fs){
    Logement log;
    fscanf(fs, "%s %d %d %s %d", log.num, &log.etat, &log.reserveH, log.type, &log.reserve);
    return log;
}

/**
    Affichage
*/
void affichLog(Logement * logT, int nb){
    int i;
    for(i=0; i < nb; i++){
        printf("Numero : %s \n", logT[i].num);
        printf("Etat : %s \n", getEtat(logT[i].etat));
        printf("Reserve Handicape ? : %s\n", reserveHTest(logT[i].reserveH));
        printf("Type : %s \n", logT[i].type);//TODO : faire fn getNumc
        //printf("Type : %d \n", logT[i].reserve);//TODO : faire un fn pour mieux afficher la reservation
        printf("-------------------\n");
    }
}

/**
    Ajout logement
**/

Logement * ajoutLogement(Logement *tLog, int *nbLog)
{
    Logement *nouvTab, nouvLog;

    nouvTab = (Logement *)realloc(tLog, *nbLog+1 * sizeof(Logement));
    if(nouvTab == NULL) {printf("\nERREUR : Probleme sur realloc de tab"), exit(1);}
    else tLog=nouvTab;

    int i, pos;

    nouvLog = entreeLog(tLog, *nbLog);

    pos=posInsertionLogement(tLog, *nbLog, nouvLog.num);

    for(i=*nbLog; i > pos; i--)
    {
        tLog[i]=tLog[i-1];
    }
    tLog[pos]=nouvLog;
    *nbLog = *nbLog + 1;
    printf("\nLogement ajoute !");
    return tLog;
}

int posInsertionLogement(Logement *tLog, int nbLog, char *num)
{
    int i;
    for(i=0; i < nbLog; i++)
        if(strcmp(tLog[i].num, num) > 0)
            return i;
    return i;
}

Logement entreeLog(Logement *tLog, int nbLog)
{
    Logement l;
    int existe = 0;

    do{
        printf("\nEntrez le numero du nouveau logement : ");
        scanf("%s", l.num);
        existe = logExiste(tLog, nbLog, l.num); //On verifie que le numero n'existe pas.
    }while(existe == 1);

    l.etat = 1;

    do{
        printf("\nAdapte pour les handicape (O/1): ");
        scanf("%d", &l.reserveH);
    }while(l.reserveH < 0 && l.reserveH > 1);

    do{
        printf("\nType du logement (Chambre / studio / T1 / T2) : ");
        scanf("%s%*c", l.type);
    }while(strcmp(l.type, "Chambre") != 0 && strcmp(l.type, "studio") != 0 && strcmp(l.type, "T1") != 0 && strcmp(l.type, "T2") != 0);

    l.reserve = 0;

    return l;
}

int logExiste(Logement *tLog, int nbLog, char *num)
{
    int i;
    for(i=0; i < nbLog; i++)
        if(strcmp(tLog[i].num, num) == 0)
            return 1;
    return 0;
}

/**
    Getters ( r�cup�rateur )
*/

char * getNumCiteByNumLog(char * numLog){
    //Cr�e une string de taille 3 ( taille du num�ro de cit� : c01 )
    char temp[4];

    //Copier le num�ro de chambre dans cette string, comme elle est plus petite elle ne conservera que le d�but, donc le num�ro de cit�
    strcpy(temp, numLog);

    //Pr�ciser la fin de la string pour �viter tout bug
    temp[3] = '\0';

    return temp;
}

char * reserveHTest(int handicapId){
    if(handicapId == 1){
        return "Oui";
    }else if(handicapId == 0){
        return "Non";
    }
    return "PROBLEME HANDICAP";
}

char * getEtat(int etatId){
    if(etatId == 0){
        return "occupee";
    }else if(etatId == 1){
        return "disponible";
    }else if(etatId == 2){
        return "en travaux";
    }
    return "PROBLEME ETAT";
}

//Recup�re les logements disponible. Prend en ecompte si il sont d�j� r�serv� ou non.
Logement * getLogDisponible(Logement * logT, int nbLog, int *nbLogD, int reserv){
    int i;
    Logement * logDisp;
    logDisp = (Logement *)malloc(nbLog*sizeof(Logement)); //le tab lgoDisp sera au maximum de la taille de logT
    *nbLogD = 0;

    for(i=0; i < nbLog; i++)
        if(logT[i].etat == 1 && logT[i].reserve == reserv)
        {
            logDisp[*nbLogD]=logT[i];
            *nbLogD = *nbLogD+1;
        }

    return logDisp;
}

//Cherche un logement adapt� dans un tableau de logement pour un �tudiant en fonction des ses pr�f�rences
Logement * getLogAdapte(Logement * logT, int nb, Etudiant e, int * logAdapteNb, char * typepref, char * citepref){
    int i;
    Logement * logAdapteT;
    *logAdapteNb = 0;

    logAdapteT = (Logement *) malloc(sizeof(Logement)*nb);//logAdapteT sera au maximum aussi grand que logT
    if (logAdapteT == NULL){printf("\nProbleme allocation tableau"); exit(1);}

    for(i=0; i<nb; i++){
        //Si le logement est dispo et est adapt�e au niveau d'handicap
        if(logT[i].reserveH == e.handic && logT[i].etat == 1){

             //Si l'�tudiant a une pref�rence de logement et une pr�f�rence de cit�.
            if(strcmp(typepref, "null") != 0 && strcmp(typepref, logT[i].type) == 0 && strcmp(citepref, "null") != 0 && logT[i].num[1] == citepref[1] && logT[i].num[2] == citepref[2]){
                logAdapteT[*logAdapteNb] = logT[i];
                *logAdapteNb = *logAdapteNb + 1;

            //Si l'�tudiant a une pref�rence de logement mais pas de pr�f�rence de cit�.
            }else if(strcmp(typepref, "null") != 0 && strcmp(typepref, logT[i].type) == 0 && strcmp(citepref, "null") == 0){
                logAdapteT[*logAdapteNb] = logT[i];
                *logAdapteNb = *logAdapteNb + 1;

            //Si l'�tudiant n'a pas de pref�rence de logement une pr�f�rence de cit�.
            }else if(strcmp(typepref, "null") == 0 && strcmp(citepref, "null") != 0 && logT[i].num[1] == citepref[1] && logT[i].num[2] == citepref[2]){
                logAdapteT[*logAdapteNb] = logT[i];
                *logAdapteNb = *logAdapteNb + 1;

             //Si l'�tudiant n'a pas une pref�rence de logement et pas de pr�f�rence de cit�.
            }else if(strcmp(typepref, "null") == 0 && strcmp(citepref, "null") == 0){
                logAdapteT[*logAdapteNb] = logT[i];
                *logAdapteNb = *logAdapteNb + 1;
            }

        }
    }

    return logAdapteT;
}

Logement * rchParTypeEtReservation(Logement * logT, int nbl, char * type, int reservation){
    int i;
    for(i = 0; i< nbl; i++){
        if(strcmp(logT[i].type, type) == 0 && logT[i].reserve == reservation){
            return &logT[i];
        }
    }

    return NULL;
}

//Fonction qui cherche un logement pour un �tudiant et essaye de lui atribuer.
    //Retour :
        // 1 Si trouv�, sinon 0
        // Un logement dans logAdap ( si trouv� )
int chercheLogForEtud(Etudiant e, Logement * tLog, int nbLog, Demande dem, Logement *logAdap)
{
    int i;
    char  numCiteL[4], numCiteD[4];

    for(i=0;i<nbLog;i++){

        //On test le handicap. Et aussi la disponibilit�, car un �tudiant peut auparavant avoir prit un des logements disponible.
        if(tLog[i].reserveH == e.handic && tLog[i].etat == 1){

            //Si la demande est par type ( Si la valeur de base X a �t� modifi� )
            if(strcmp(dem.type, "X") != 0){

                //On r�cup�re le num�ro de cit� du logement trouv� et celui voulu par l'�tudiant.
                strcpy(numCiteL,getNumCiteByNumLog(tLog[i].num));
                strcpy(numCiteD,getNumCiteByNumLog(dem.numLog));

                //On compare le type et la cit�
                if(strcmp(numCiteD, numCiteL) == 0 && strcmp(tLog[i].type, dem.type) == 0){
                    //Si c'est bon alors renvoyer le logement avec le pointeur
                    *logAdap=tLog[i];
                    return 1;
                }

            }
            //Si la demande est par num�ro de logement en particulier ( donc que la valeur de base X pr�sente dans le numLog a �t� modifi�e par un num bien pr�cis )
            else if(strchr(dem.numLog, 'X') != NULL){
                //On compare les num�ros de logemet
                 if(strcmp(tLog[i].num, dem.numLog) == 0){
                     //Si c'est bon alors renvoyer le logement avec le pointeur
                    *logAdap=tLog[i];
                    return 1;
                }
            }
        }
    }

    return 0;//pas de logement adapt� trouv�
}

int posLogementTab(Logement *tLog, int nbLog, char *num){

    int i;
    for(i=0; i < nbLog; i++)
        if(strcmp(tLog[i].num, num) == 0)
            return i;
}
/**
    Setters ( modificateur )
*/

 //Modifie la statut de la chambre (1 : libre ; 0 : occup� ; 2 : en travaux)
void modifierStatutLog(Logement tabL[], int nbLg, char *numLog, int statut){
    int i;
    for(i=0; i < nbLg; i++)
        if(strcmp(tabL[i].num, numLog) == 0)
            tabL[i].etat = statut;
}

void modifierCodeReservLog(Logement tabL[], int nbLg, char *numLog, int codeR)
{
    int i;
    for(i=0;i<nbLg;i++)
        if(strcmp(tabL[i].num, numLog) == 0)
            tabL[i].reserve=codeR;
}

/**
    SAUVEGARDE
**/

void sauvegardeLog(Logement *tLog, int nbLog)
{
    FILE *fs;
    int i;

    fs=fopen("donnees/logement.don", "w");
    if (fs == NULL){printf("\nProbleme ouverture fichier etudiant.don"); exit(1);}
    fprintf(fs, "%d\n", nbLog);

    for(i=0; i < nbLog; i++)
    {
        fprintf(fs, "%s\n", tLog[i].num);
        fprintf(fs, "%d\n", tLog[i].etat);
        fprintf(fs, "%d\n", tLog[i].reserveH);
        fprintf(fs, "%s\n", tLog[i].type);
        fprintf(fs, "%d\n\n", tLog[i].reserve);
    }
    fclose(fs);
}

/**
    R�cup�re le num d'un logement voulu par l'user (adapt� � lui)
**/

char * choixLogAdapte(Logement tabL[], int nbL, Etudiant user, char *citePref){

    Logement *logAdapt;
    char typePref[10], numLogVoulu[10];
    int nbLogAdap, logEx;

    entreeTypeLogPrefMenu(typePref, 1);

    logAdapt=getLogAdapte(tabL, nbL, user, &nbLogAdap, typePref, citePref);
    printf("\n\nLISTE DES LOGEMENTS POSSIBLES\n\n");
    affichLog(logAdapt, nbLogAdap);

    do{
        printf("\nQuelle chambre souhaitez-vous ? ");
        scanf("%s%*c", numLogVoulu);
        logEx=logExiste(logAdapt, nbLogAdap, numLogVoulu);//si le log saisi fait pas parti des log adapt�s a lui : 1 ; sinon 0
        if(logEx == 0)
            printf("Ce logement ne fait pas parti des logements qui vous sont adaptes\nVeuillez saisir un autre logement\n");
    }while(logEx == 0); //tant que le log saisi ne fait pas parti des log adapt�s a lui

    return numLogVoulu;
}
