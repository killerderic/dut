#include "../tools.h"
#include "../cite/cite.h"
/**
    Les logements sont stock�s dans un tableau de structure
*/
/**
    Menu
*/
void rchLogementMenu(Etudiant user);
void entreeTypeLogPrefMenu(char * type, int prefOK);
void entreeCitePrefMenu(char *cite, int prefOK);
void mainModifTravauxLog(Logement *tLog, int nbLog);

/**
    Fonction de chargement
**/
Logement * chargLogement(int * nb);
Logement LireLog(FILE * fs);

/**
    Affichage
*/
void affichLog(Logement * logT, int nb);

/**
    Ajout logement
**/

Logement * ajoutLogement(Logement *tLog, int *nbLog);
Logement entreeLog(Logement *tLog, int nbLog);
int logExiste(Logement *tLog, int nbLog, char *num);
int posInsertionLogement(Logement *tLog, int nbLog, char *num);

/**
    Getters ( r�cup�rateur )
*/
char * getNumCiteByNumLog(char * numLog);
char * getEtat(int etatId);
char * reserveHTest(int handicapId);
Logement * getLogDisponible(Logement * logT, int nbLog, int *nbLogD, int reserv);
Logement * getLogAdapte(Logement * logT, int nb, Etudiant e, int * logAdapteNb, char * typepref, char * citepref);
Logement * rchParTypeEtReservation(Logement * logT, int nbl, char * type, int reservation);
int posLogementTab(Logement *tLog, int nbLog, char *num);

int chercheLogForEtud(Etudiant e, Logement * tLog, int nbLog, Demande dem, Logement *logAdap);

/**
    Setters ( modificateur )
*/
void modifierStatutLog(Logement tabL[], int nbLg, char *numLog, int statut); //modifie la statut de la chambre (1 : libre ; 0 : occup� ; 2 : en travaux)
void modifierCodeReservLog(Logement tabL[], int nbLg, char *numLog, int codeR);

/**
    SAUVEGARDE
**/

void sauvegardeLog(Logement *tLog, int nbLog);

/**
    R�cup�re le num d'un logement voulu par l'user (adapt� � lui)
**/

char * choixLogAdapte(Logement tabL[], int nbL, Etudiant user, char *citePref);
