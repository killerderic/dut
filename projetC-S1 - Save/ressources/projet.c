#include "projet.h"

void mainMenu(){

    //D'abord tester si l'utilisateur est un g�rant ou un �tudiant
    char userType = testEtudOuGerent();

    int nbGer, rep, nbe, nbLog, nbDem;
    Etudiant user;
    Etudiant ** etudT;
    Gerant tabGer[TAILLEMAXGER];
    Logement *tLog;
    ListeDem listDem;

    etudT = chargEtudT(&nbe);
    nbGer = chargTabGerant(tabGer);
    tLog = chargLogement(&nbLog);
    listDem = chargDemande(&nbDem);

    srand(time(NULL));//pour nb aleatoire (pour l'id)

    //Si c'est un �tudiant
    if(userType == 'e'){

        char choix = 'N';

        printf("\t\t\t\tESPACE ETUDIANT\n\n");

        while(choix == 'N'){
            user = entreeNomPrenomEtud(user);//R�cup�rer nom et pr�nom de l'�tudiant
            printf ("\nConfirmer ? (O/N) : ");
            scanf("%c%*c", &choix);
        }

        //Tester si il existe d�j� dans la base de donn�e( fichier etud.don )
        user = etudExisteDeja(user, etudT, nbe, &rep); //si il existe permet aussi de r�cup�rer le reste des donn�es

        //Si oui alors aller dans le menu de gestion pour etudiant
        if(rep == 1){

            if(loginEtudiant(user)== 0)
                return;
            printf("\nBienvenue %s %s ! \n", user.nom, user.prenom);
            listDem=menuEtud(etudT, nbe, user, tLog, nbLog, listDem, &nbDem);

        }else if(rep == 0){

            //Si non alors terminer l'inscription
            printf("Nous ne vous connaissons pas, voulez-vous vous inscrire (O/N) : ");

            scanf("%c%*c", &choix);
            choix = toupper(choix);

            while(choix != 'O' && choix !=  'N'){
                printf("Erreur : Veuillez taper O ou N\n");
                scanf("%c%*c", &choix);
                choix = toupper(choix);
            }

            if(choix == 'O'){

                user = inscrireEtud(user, etudT, &nbe);
                printf("Inscription reussie ! \n");

                // Et lancer le menu pour les �tudiant
                listDem=menuEtud(etudT, nbe, user, tLog, nbLog, listDem, &nbDem);
            }

        }
    }
    else if(userType == 'g')
    {

        if(loginGerant(tabGer, nbGer)==0)
            return;
        listDem=menuGerant(tabGer, nbGer, etudT, nbe, tLog, &nbLog, listDem, &nbDem);

    }
    else if(userType == 'd')
    {
        int choix;

        do{
            do{
                printf("\n1. Menu Etudiant\n2. Menu Gerant\n9. Quitter\n?");
                scanf("%d%*c", &choix);
            }while(choix != 1 && choix != 2 && choix != 9);

            switch(choix)
            {
            case 1:
                strcpy(user.nom, "HILDE");
                strcpy(user.prenom, "Sauret");
                user = etudExisteDeja(user, etudT, nbe, &rep);

                printf("\n\t\t\t\tMODE DEBUG\n\n");
                listDem=menuEtud(etudT, nbe, user, tLog, nbLog, listDem, &nbDem);
                break;
            case 2:
                nbGer = chargTabGerant(tabGer);

                listDem=menuGerant(tabGer, nbGer, etudT, nbe, tLog, &nbLog, listDem, &nbDem);
                break;
            default:
                break;
            }
        }while(choix != 9);
    }
    sauvegardGlobal(etudT, nbe, tabGer, nbGer, tLog, nbLog, listDem, nbDem);
}

char testEtudOuGerent(){ //TODO : enlever le mode debug
    char rep[20];

    printf("\n\n\n\n\n\t\t\tVous etes :\n\t\t\t - Un etudiant, tapez e\n\t\t\t - Un gerant, tapez g\n\t\t\t- Debug, tapez d\n\t\t\tChoix : ");
    scanf("%s%*c", rep);
    system("cls");

    while(strcmp(rep, "e") != 0 && strcmp(rep, "g") != 0 && strcmp(rep, "d") != 0){
        printf("\n\t\t*** ERREUR : Veuillez entrer e ou g ***\n\n");
        printf("\n\n\t\t\tVous etes :\n\t\t\t - Un etudiant, tapez e\n\t\t\t - Un gerant, tapez g\n\t\t\tChoix : ");
        scanf("%s%*c", rep);
        system("cls");
    }
    if(strcmp(rep, "e") == 0)
        return 'e';
    if(strcmp(rep, "g") == 0)
        return 'g';
    if(strcmp(rep, "d") == 0)
        return 'd';
    return -1;
}

//Besoin de mettre les menus dans proj.c car on appelle rchLog qui est dans log.c
//TODO : a voir si le affichMenu() on le laisse dans etud.c ou si c'est plus cooherent de le ramener ici

ListeDem menuEtud(Etudiant ** tabEtud, int nbEtud, Etudiant user, Logement *tLog, int nbLog, ListeDem lDem, int *nbDem){
    int choix;
    char wait;

    choix = AffichMenuEtud();
    while(choix != 9){
        if(choix == 1){
            system("cls");
            rchLogementMenu(user);
        }else if(choix == 2){
            system("cls");
            lDem=enregistrDemande(lDem, user, nbDem, tLog, nbLog);
        }
        else if(choix == 3){
            system("cls");
            CiteMenu();
        }else if(choix == 4)
        {
            system("cls");
            affichLog(tLog, nbLog);
            nbEtud=EtdepartEtud(tabEtud, nbEtud, tLog, nbLog, user);
            affichLog(tLog, nbLog);
            affichTabEtud(tabEtud, nbEtud);
        }else if(choix == 5){
            system("cls");
            printf("\t\t\t\tMON PROFIL\n\n\n");
            affich1Etud(user);
            affichDemEtudiant(lDem, user.idEtud);
        }
        if (choix == 1 || choix == 2 || choix == 3 || choix == 4 || choix == 5){
            printf("\n\n\tAppuyez sur entree pour continuer.");
            scanf("%c", &wait);
            system("cls");
        }
        choix = AffichMenuEtud();
    }
    return lDem;
}

ListeDem menuGerant(Gerant tabGer[], int nbGer, Etudiant ** tabEtud, int nbEtud, Logement *tLog, int *nbLog, ListeDem listDem, int *nbDem)
{
    int choix;
    char wait;

    choix = affichMenuGerant();
    while(choix != 11)
    {
        if(choix == 1){
            system("cls");
            printf("\n\tAjout d'un gerant (Attention cela occtroie tous les droits a cet admin\n");
            nbGer=ajoutGerant(tabGer, nbGer);
        }else if(choix == 2){
            system("cls");
            affichTabGer(tabGer, nbGer);
        }else if(choix == 3){
            associaAuto(listDem, tabEtud, nbEtud, tLog, *nbLog);
        }else if(choix == 4){
            system("cls");
            printf("\t\tLISTE DES DEMANDE PAR ORDRE DECROISSANT DES ECHELONS\n\n");
            affichDemEchelon(listDem, tabEtud, nbEtud);
        }else if(choix == 5){
            system("cls");
            affichDem(listDem);
            mettreDemEnAttente(listDem);
        }else if(choix == 6){
            system("cls");
            affichTabEtud(tabEtud, nbEtud);
        }else if(choix == 7){
            system("cls");
            affichLogOccup(tabEtud, nbEtud);
        }else if(choix == 8){
            system("cls");
            ajoutLogement(tLog, nbLog);
        }else if(choix == 9){
            system("cls");
            mainModifTravauxLog(tLog, *nbLog);
        }else if(choix == 10){
            system("cls");
            printf("\t\t\tLISTE DES LOGEMENTS\n\n");
            affichLog(tLog, *nbLog);
        }
        if (choix == 1 || choix == 2 || choix == 3 || choix == 4 || choix == 5 || choix == 6 || choix == 7 || choix == 8 || choix == 9 || choix == 10)
        {
            printf("\n\n\tAppuyez sur entree pour continuer.");
            scanf("%c", &wait);
            system("cls");
        }
        choix = affichMenuGerant();
    }
    return listDem;
}

void sauvegardGlobal(Etudiant *tabEtud[], int nbe, Gerant *tabGer, int nbGer,Logement *tLog, int nbLog, ListeDem listDem, int nbDem)
{
    FILE *fs;
    fs=fopen("donnees/demande.don", "w"); //Oblige d'ouvrir le fichier ici car sauvegardeDem est une fonction recursive.
    if (fs == NULL){printf("\nProbleme ouverture fichier demande.don"); exit(1);}

    sauvegardeTabGer(tabGer, nbGer);
    sauvegardeEtudT(tabEtud, nbe);
    fprintf(fs,"%d\n", nbDem);
    sauvegardeDem(listDem, fs);
    sauvegardeLog(tLog, nbLog);

    fclose(fs);
    free(tabEtud);
    free(tabGer);
    free(tLog);
    listDem=freeDem(listDem); //free la liste des demandes
}


