#include "gerant.h"

int chargTabGerant(Gerant tabGer[])
{
    FILE *fs;
    int nbGer;

    fs=fopen("donnees/passwordGer.bin", "rb");
    if(fs == NULL){
        printf("Pb ouverture fichier passwordGer.bin");
        exit(EXIT_FAILURE);
    }

    fread(&nbGer, sizeof(int), 1, fs);
    if(nbGer > TAILLEMAXGER){
        printf("Tab Gerants pleins\nPb chargement\nFin du programme\n");
        exit(EXIT_FAILURE);
    }

    fread(tabGer, sizeof(Gerant), nbGer, fs);

    fclose(fs);
    return nbGer;
}

int loginGerant(Gerant tabGer[], int nbGer) //se co avec root (mdp : 123)
{
    int pos, nbTent=5;
    Gerant user;

    printf("Nom de gerant : ");
    scanf("%s", user.login);
    MajusculeChaine(user.login);
    pos=rechercheGerant(tabGer, nbGer, user.login);
    while(pos == -1)
    {
        printf("Nom de gerant inconnu, retapez le nom de gérant (ou Q pour annuler)\n");
        scanf("%s", user.login);
        MajusculeChaine(user.login);

        if(strcmp(user.login, "Q") == 0) return 0;

        pos=rechercheGerant(tabGer, nbGer, user.login);
    }

    printf("Mot de passe : ");
    scanf("%s", user.password);

    while(strcmp(user.password, tabGer[pos].password) != 0 && nbTent > 0)
    {
        printf("Mot de passe errone, %d tentatives restantes\n", nbTent);
        printf("Mot de passe ? ");
        scanf("%s", user.password);
        nbTent--;
    }
    if(nbTent == 0)
    {
        printf("Connexion refusee ! Nb de tentatives depassees\n");
        return 0;
    }
    printf("Connexion en tant que %s etablie\n", user.login);
    return 1;
}

int rechercheGerant(Gerant tabGer[], int nbGer, char login[])
{
    int i;

    for(i=0; i<nbGer; i++)
        if(strcmp(tabGer[i].login, login) == 0)
            return i;
    return -1;
}

void affichTabGer(Gerant tabGer[], int nbGer)
{
    int i;

    system("cls");
    printf("\t\t*** Liste des gerants ***\n\n");
    printf("Login\tMot de passe\n\n");

    for(i=0;i<nbGer;i++)
        printf("%s\t%s\n", tabGer[i].login, tabGer[i].password);
}

int affichMenuGerant(void)
{
    int choix;
     printf("\n\n\tMENU GERANT\n");
     printf("\n1) Ajouter un gerant");
     printf("\n2) Liste des gerants");
     printf("\n3) Attribution auto");
     printf("\n4) Liste des demandes par echelon");
     printf("\n5) Mettre une demande en attente");
     printf("\n6) Afficher tous les etudiants");
     printf("\n7) Afficher locations en cours");
     printf("\n8) Ajouter un logement");
     printf("\n9) Mettre/enlever les travaux d'un logement");
     printf("\n10) Afficher tous les logements");

     printf("\n\n11) Sauvegarder et quitter");

     printf("\n\nIndiquer votre choix : ");
     scanf("%d%*c", &choix);

     while(choix != 1 && choix != 2 && choix != 3 && choix != 4 && choix != 5 && choix != 6 && choix != 7 && choix != 8 && choix != 9 && choix != 10 && choix != 11)
     {
         printf("\nERREUR : Choix invalide ou inexistant");
         printf("\nVeuillez reindiquer votre choix : ");
         scanf("%d", &choix);
     }
     return choix;
}

int ajoutGerant(Gerant tabGer[], int nbGer)
{
    Gerant userAdd;
    char rep;

    if(nbGer >= TAILLEMAXGER)
    {
        printf("Pb taille tab gerant \nAjout impossible (contacter un IT)\n");
        return nbGer;
    }
    printf("Login du gerant a ajouter : ");
    scanf("%s%*c", userAdd.login);
    MajusculeChaine(userAdd.login);

    if(rechercheGerant(tabGer,nbGer,userAdd.login) != -1)
    {
        printf("Gerant deja existant\nSouaitez-vous modifier le mot de passe ? (O/N) ");
        scanf("%c%*c", &rep);
        rep=toupper(rep);

        while(rep != 'O' && rep != 'N')
        {
            printf("Veuillez taper O ou N ");
            scanf("%c%*c", &rep);
            rep=toupper(rep);
        }
        if(rep == 'N') //si veux pas modifier mdp ==> quitte fonction ajout
            return nbGer;
    }
    printf("Mot de passe ? ");
    scanf("%s%*c", userAdd.password);

    printf("\nLogin : %s\nMot de passe : %s\nValide ? (O/N) ", userAdd.login, userAdd.password);
    scanf("%c%*c", &rep);
    rep=toupper(rep);
    while(rep != 'O' && rep != 'N')
    {
        printf("Veuillez taper O ou N ");
        scanf("%c%*c", &rep);
        rep=toupper(rep);
    }

    if(rep == 'O') {
        tabGer[nbGer]=userAdd;
        printf("Nouveau gerant ajoute");
        return nbGer +1;
    }
    else {
        printf("Ajout annule\n");
        return nbGer;
    }
}

void sauvegardeTabGer(Gerant tabGer[], int nbGer)
{
    FILE *fb;

    fb=fopen("donnees/passwordGer.bin", "wb");

    fwrite(&nbGer, sizeof(int), 1, fb);

    fwrite(tabGer, sizeof(Gerant), nbGer, fb);

    fclose(fb);
}

/**
    Association logement etudiant Function
*/

void associaAuto(ListeDem lDem, Etudiant ** etudT, int nbe, Logement * logT, int nbLog){

    int nbeInListe = 0, position, nbLogDR, nbLogDNR, i;
    Logement * logDispR, * logDispNR, logAdap;
    MaillonDem * demande;
    Demande dem;

    //Récupérer les etudiants voulant un logement dans un tableaux
    Etudiant ** etudInL = getEtudInLDem(lDem, etudT, nbe, &nbeInListe, &position);

    //Trier ces étudiants par echelon
    triEtudEchelon(etudInL, nbeInListe);

    //Récupérer les logements disponible non reserve
    logDispNR=getLogDisponible(logT, nbLog, &nbLogDNR, 0);

    //Récupérer les logements disponible reserve
    logDispR=getLogDisponible(logT, nbLog, &nbLogDR, 1);

    //Pour chaque etudiant
    for(i=0; i < nbeInListe; i++){

        //Recupérer sa demande avec son identifiant
        demande = getDemandeByEtudiantId(lDem, etudInL[i]->idEtud);
        dem=demande->elem;

        //Chercher un logement adapté pour lui dans les logements non reserve
        if(chercheLogForEtud(*etudInL[i], logDispNR, nbLogDNR, dem, &logAdap) == 1){

            //Lui attribuer ce logement
            attribution1Logement( etudInL[i], logT, nbLog, logAdap);

            //Logger le changement en console
            printf("    Logement %s, attribue a : %s %s \n", logAdap.num, etudInL[i]->nom, etudInL[i]->prenom);

            //Et supprimer sa demande
            lDem=suppr1Dem(lDem, dem);
        }
        //Sinon rechercher dans les log réservés
        else if(chercheLogForEtud(*etudInL[i], logDispR, nbLogDR, dem, &logAdap) == 1){

            //Lui attribuer ce logement
            attribution1Logement( etudInL[i], logT, nbLog, logAdap);

            //Logger le changement en console
            printf("    Logement %s, attribue a : %s %s \n", logAdap.num, etudInL[i]->nom, etudInL[i]->prenom);

            //Et supprimer sa demande
            lDem=suppr1Dem(lDem, dem);

        }

        //Sinon, aucun logement n'est disponible pour lui
        else
        {
            printf("    Pas de logement pour %s %s \n", etudInL[i]->nom, etudInL[i]->prenom);
            miseAttenteDem(lDem, dem);
        }

    }
}

//Attribue un logement donné a un étudiant donné.
    // On doit passer la tableau de logement car le logement en entrée est un structure et non un pointeur sur structure
void attribution1Logement(Etudiant *etud, Logement *tabLog, int nbLog, Logement log){

        //La chambre devient occupée
    modifierStatutLog(tabLog, nbLog, log.num, 0);

    //La chambre n'est plus réservée (car attribuéé)
    modifierCodeReservLog(tabLog, nbLog, log.num, 0);

    //Atribuer la chambre a l'étudiant en lui donnant le numéro de chambre
    strcpy(etud->numCham, log.num);

}

