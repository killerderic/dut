#include "../tools.h"
#define TAILLEMAXGER 10

int chargTabGerant(Gerant tabGer[]);
int loginGerant(Gerant tabGer[], int nbGer);
int rechercheGerant(Gerant tabGer[], int nbGer, char login[]);
int ajoutGerant(Gerant tabGer[], int nbGer);
int affichMenuGerant(void);
void sauvegardeTabGer(Gerant tabGer[], int nbGer);
void affichTabGer(Gerant tabGer[], int nbGer);


/**
    Association logement etudiant Function
*/

void associaAuto(ListeDem lDem, Etudiant ** etudT, int nbe, Logement * logT, int nbLog);
void attribution1Logement(Etudiant *etud, Logement *tabLog, int nbLog, Logement log);
