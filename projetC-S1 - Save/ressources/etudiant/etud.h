#include "../tools.h"

/**
    MENU FUNCTION
**/
int AffichMenuEtud(void);

/**
    AFFICHAGE FUNCTION
**/
void affichTabEtud(Etudiant **tabEtud, int nbrEtud);
void affich1Etud(Etudiant e);
void affichLogOccup(Etudiant **tabEtud, int nbEtud);
void affichTabLogOccup(Etudiant **tabLogOccup, int nbLogOccup);

/**
    FONCTION DE GESTION DU TABLEAU
**/
Etudiant ** chargEtudT(int *nbrEtud);
Etudiant lireEtud(FILE *fe);
Etudiant ** ajoutEtud(Etudiant **tabEtu, int *nbrEtud, Etudiant e);
void sauvegardeEtudT(Etudiant **tabEtu, int nbrEtud);

/**
    Getters ( récupérateur )
**/
Etudiant * getEtudiantById(Etudiant ** etudT, int nbe, int id);
Etudiant * rchEtudNomPrenom(Etudiant ** etudT, int nbE, char * nom, char * prenom);
int posInsertionEtudiantTab(Etudiant **tabEtu, int nbrEtud, Etudiant e);
int recherchCite(Etudiant **tabLogOccup, int nbLogOccup, int val);
int rchPosEtud(Etudiant ** tabEtu, int nbEt, Etudiant e);

/**
    Setters ( modificateur )
**/
Etudiant entreeEtudiant(Etudiant e, Etudiant **tabEtud, int nbEtud);
Etudiant entreeNomPrenomEtud(Etudiant e);

/**
    TEST
**/
Etudiant etudExisteDeja(Etudiant e, Etudiant ** etudT, int nbE, int *rep);

/**
    CONNECTION FUNCTION
**/
int loginEtudiant(Etudiant e);
Etudiant inscrireEtud(Etudiant e, Etudiant ** etudT, int * nbEtud);
int generationIdEtud(Etudiant **tabEtud, int nbEtud);
int idDejaExistant(Etudiant **tabEtud, int nbEtud, int idEt);

/**
    TRI FUNCTION
**/
void triEtudEchelon(Etudiant *tEtud[], int nbEt);
int interclassementTetud(Etudiant **tab1, int nb1, Etudiant **tab2, int nb2, Etudiant **tab3);
void separationTetud(Etudiant *tEtud[], int nbEt, Etudiant *tab1[], int nb1, Etudiant *tab2[]);

void triEtudNom(Etudiant *tEtud[], int nbEt);
int rechercheMinNom(Etudiant *tEtud[], int debut, int nbEt);
void permutation(Etudiant *tEtud[], int pos1, int pos2);

void triEtudLogCite(Etudiant **tabLogOccup, int nbLogOccup);
int rechercheMinLog(Etudiant **tabLogOccup, int debut, int nbLogOccup);

/**
    DEPART ETUD FUNCTION
**/
int EtdepartEtud(Etudiant **tabEtu, int nbEt, Logement tabL[], int nbLg, Etudiant user);
int suppressionEtud(Etudiant ** tabEtud, int nbEt, int pos);
