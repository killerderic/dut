#include "etud.h"

 int AffichMenuEtud(void)
 {
     int choix;
     printf("\n\n\tMENU ETUDIANT\n");
     printf("\n1) Rechercher un logement");
     printf("\n2) Faire une demande");
     printf("\n3) Afficher les cites");
     printf("\n4) Quitter les cites");
     printf("\n5) Afficher mon profil et ma demande");
     printf("\n\n9)Quitter");

     printf("\n\nIndiquer votre choix : ");
     scanf("%d%*c", &choix);

     while(choix != 1 && choix != 2 && choix != 3 && choix != 4 && choix != 5 && choix != 9)
     {
         printf("\nERREUR : Choix invalide ou inexistant");
         printf("\nVeuillez reindiquer votre choix : ");
         scanf("%d", &choix);
     }
     return choix;
 }

 // Charge le fichier Etudiant dans un tableau.
Etudiant ** chargEtudT(int *nbrEtud)
{
    FILE *fe;
    Etudiant *e;
    Etudiant **tabEtu;
    int i;
    fe=fopen("donnees/etudiant.don", "r");
    if (fe == NULL){printf("\nProbleme ouverture fichier"); exit(1);}

    fscanf(fe,"%d%*c", nbrEtud);

    tabEtu = (Etudiant **)malloc(sizeof(Etudiant *) * *nbrEtud );

    if (tabEtu == NULL){printf("\nProbleme allocation tableau"); exit(1);}

    for(i=0; i < *nbrEtud ; i ++)
    {
        e=(Etudiant *)malloc(sizeof(Etudiant));
        *e=lireEtud(fe);
        tabEtu[i]=e;
    }
    fclose(fe);
    return tabEtu;
}

// Lire un etudiant dans le fichier
Etudiant lireEtud(FILE *fe)
{
    Etudiant e;
    fscanf(fe, "%d%*c", &e.idEtud);
    fgets(e.nom, sizeof(e.nom), fe);
    e.nom[strlen(e.nom)-1]='\0';
    fgets(e.prenom, sizeof(e.prenom), fe);
    e.prenom[strlen(e.prenom)-1]='\0';
    fscanf(fe, "%d %d %d", &e.naissance.jour, &e.naissance.mois, &e.naissance.annee);
    fscanf(fe, "%s", e.tel);
    fscanf(fe, "%d", &e.handic);
    fscanf(fe, "%d", &e.echelon);
    fscanf(fe, "%s%*c%*c", e.numCham);

    return e;
}

// Afficher tout les etudiants d'un tableau
void affichTabEtud(Etudiant **tabEtu, int nbrEtud)
{
    Etudiant e;
    int i;
    printf("\nNombre d'etudiant : %d", nbrEtud);
    for(i=0; i < nbrEtud; i++)
    {
        printf("\n\n\tEtudiant %d :\n", i+1);
        e=*tabEtu[i];
        affich1Etud(e);
    }
}

void affich1Etud(Etudiant e) // Affiche un etudiant
{
    printf("N� d'etudiant : %d\n", e.idEtud);
    printf("Nom et prenom : %s %s\n", e.nom, e.prenom);

    if(e.naissance.jour < 10 && e.naissance.mois > 10)
        printf("Date de naissance : 0%d/%d/%d\n", e.naissance.jour, e.naissance.mois, e.naissance.annee);
    else if(e.naissance.jour > 10 && e.naissance.mois < 10)
        printf("Date de naissance : %d/0%d/%d\n", e.naissance.jour, e.naissance.mois, e.naissance.annee);
    else if(e.naissance.jour < 10 && e.naissance.mois < 10)
        printf("Date de naissance : 0%d/0%d/%d\n", e.naissance.jour, e.naissance.mois, e.naissance.annee);
    else
        printf("Date de naissance : %d/%d/%d\n", e.naissance.jour,e.naissance.mois, e.naissance.annee);

    printf("Numero de telephone : %s\n", e.tel);
    if(e.handic == 1)
            printf("Handicap : Oui\n");
    else
        printf("Handicap : Non\n");
    printf("Echelon : %d\n", e.echelon);

    if(strcmp(e.numCham, "X") == 0)
        printf("Chambre : Aucune");
    else
        printf("Chambre : %s", e.numCham);

}
void affichTabLogOccup(Etudiant **tabLogOccup, int nbLogOccup)
{
    int i;
    printf("\t\t\t\tLOGEMENT OCCUPPES\n");
    for(i=0; i < nbLogOccup; i++)
    {
        printf("\n\nNumero du logement : %s", tabLogOccup[i]->numCham);
        printf("\nNom du locataire : %s %s", tabLogOccup[i]->nom, tabLogOccup[i]->prenom);
    }
}

void affichLogOccup(Etudiant **tabEtud, int nbEtud){

    int i, j, k, nbLogOccup, pos, deb;

    Etudiant **tabLogOccup;

    // Maximum = tout les etudiants
    tabLogOccup = (Etudiant **)malloc(nbEtud*sizeof(Etudiant *));
    if(tabLogOccup == NULL){printf("ERREUR ALLOCATION"); return;}

    Etudiant **tabLogTemp;
     // Maximum = tout les etudiants
    tabLogTemp = (Etudiant **)malloc(nbEtud*sizeof(Etudiant *));

    if(tabLogTemp == NULL){printf("ERREUR ALLOCATION"); return;}

    // On r�cup�re tout les �tudiant log�s, donc les logements occup�s.
    for(i=0, j=0; i < nbEtud; i++){
        if(strcmp(tabEtud[i]->numCham, "X") != 0){
            tabLogOccup[j]=tabEtud[i];
            j++;
        }
    }

    nbLogOccup = j;

     //On trie par num�ro de chambre, donc par cit�.
    triEtudLogCite(tabLogOccup, nbLogOccup);

    //On va parcourir les 6 cites.
    for(i=1, deb=0; i < 7; i++){

         //On recherche la position o� l'on passe de c0x � c0x+1 (ex : de c01 � c02)
        pos=recherchCite(tabLogOccup, nbLogOccup, i);

         //On recupere dans un tableau temporaire les logement occupe de la cite c0x
        for(j=deb, k=0; j < pos; j++, k++){
            tabLogTemp[k]=tabLogOccup[j];
        }

         //On trie ce tableau temporaire par nom d'etudiant.
        triEtudNom(tabLogTemp, k);

         //On replace dans le tableau de depart les logement occupe et trie par nom.
        for(j=deb, k=0; j < pos; j++, k++){
            tabLogOccup[j]=tabLogTemp[k];
        }

         // On decale le debut du tableau en ajoutant le nombre de logement de la cite c0x, ajout� precedemment.
        deb += pos;
    }

    affichTabLogOccup(tabLogOccup, nbLogOccup);

     // On lib�re les deux tableaux
    free(tabLogOccup);
    free(tabLogTemp);
}

//Ajoute un Etudiant au tableau
Etudiant ** ajoutEtud(Etudiant **tabEtu, int *nbrEtud, Etudiant e){

    Etudiant **nouvTab;
    Etudiant * etudiant = (Etudiant *)malloc(sizeof(Etudiant));

    int pos, i;
    //On recherche la postion d'insertion de l'etudiant.
    pos=posInsertionEtudiantTab(tabEtu, *nbrEtud, e);

    //On crer un pointeur sur etudiant.
    *etudiant = e;
    //Cr�e un nouveau tableau pouvant acceuillir un �tudiant suppl�mentaire.
    nouvTab = (Etudiant **)realloc(tabEtu, *nbrEtud+1 * sizeof(Etudiant *));
    if(nouvTab == NULL) {printf("\nERREUR : Probleme sur realloc de tab"), exit(1);}
    else tabEtu=nouvTab;

    //On decale toutes les adresses qui sont a gauche de la position ou l'etudiant va s'inserer.
    for(i=*nbrEtud; i > pos; i--)
    {
        tabEtu[i]=tabEtu[i-1];
    }
    //On insere l'etudiant a la bonen place.
    tabEtu[pos]=etudiant;
    *nbrEtud+=1;

    return tabEtu;
}

// Fonction pour entr�er un etudiant (connaissant son nom et prenom)
Etudiant entreeEtudiant(Etudiant e, Etudiant **tabEtud, int nbEtud){


    printf("\nDate de naissance (jj mm aaaa): ");
    scanf("%d %d %d", &e.naissance.jour, &e.naissance.mois, &e.naissance.annee);

    while(e.naissance.jour > 31 || e.naissance.jour < 1 || e.naissance.mois < 1 || e.naissance.mois > 12){
        printf("\nERREUR : Date de naissance invalide, retaper.");
        printf("\nDate de naissance (jj mm aaaa): ");
        scanf("%d %d %d", &e.naissance.jour, &e.naissance.mois, &e.naissance.annee);
    }

    printf("\nNumero de telephone (10 num): ");
    scanf("%s", e.tel);

    while(strlen(e.tel) != 10){
        printf("ERREUR : nombre de chiffres invalide, retapez\n");
        printf("\nNumero de telephone (10 num): ");
        scanf("%s", e.tel);
    }

    printf("\nHandicape (0 : non/1 : oui) : ");
    scanf("%d", &e.handic);

    while(e.handic != 0  && e.handic != 1){
        printf("\nERREUR : Numero invalide, retaper.");
        printf("\nHandicape (0 : non/1 : oui) : ");
        scanf("%d", &e.handic);
    }

    printf("\nEchelon (0/1/2/3/4/5/6/7): ");
    scanf("%d", &e.echelon);
    while(e.echelon != 0 && e.echelon != 1 && e.echelon != 2 && e.echelon != 3 && e.echelon != 4 && e.echelon != 5 && e.echelon != 6 && e.echelon != 7)
    {
        printf("\nERREUR : Echelon invalide, retaper.");
        printf("\nEchelon (0/1/2/3/4/5/6/7): ");
        scanf("%d", &e.echelon);
    }
    strcpy(e.numCham, "X");
    //e.numCham[1] = '\0';
    e.idEtud=generationIdEtud(tabEtud, nbEtud);
    return e;
}

int generationIdEtud(Etudiant **tabEtud, int nbEtud)
{
    int idEtud;

    //boucle tant que la fn retourne 1 (que l'id est deja pris)
    do{
        idEtud=(rand() % (99999 - 2)) + 1;
    }while(idDejaExistant(tabEtud, nbEtud, idEtud));

    return idEtud;
}

/*recherche l'existence d'un id dans les etudiants d�ja existants
    retourne 1 si iD d�ja existant 0 sinon
*/
int idDejaExistant(Etudiant **tabEtud, int nbEtud, int idEt)
{
    int i;
    for(i=0;i<nbEtud;i++)
        if(idEt == tabEtud[i]->idEtud)
            return 1;
    return 0;
}

void sauvegardeEtudT(Etudiant **tabEtu, int nbrEtud)
{
    FILE *fs;
    int i;

    triEtudNom(tabEtu, nbrEtud);

    fs=fopen("donnees/etudiant.don", "w");
    if (fs == NULL){printf("\nProbleme ouverture fichier etudiant.don"); exit(1);}
    fprintf(fs, "%d\n", nbrEtud);

    for(i=0; i < nbrEtud; i++)
    {
        fprintf(fs, "%d\n", tabEtu[i]->idEtud);
        fprintf(fs, "%s\n", tabEtu[i]->nom);
        fprintf(fs, "%s\n", tabEtu[i]->prenom);
        fprintf(fs, "%d %d %d\n", tabEtu[i]->naissance.jour, tabEtu[i]->naissance.mois, tabEtu[i]->naissance.annee);
        fprintf(fs, "%s\n", tabEtu[i]->tel);
        fprintf(fs, "%d\n", tabEtu[i]->handic);
        fprintf(fs, "%d\n", tabEtu[i]->echelon);
        fprintf(fs, "%s\n\n", tabEtu[i]->numCham);
    }
    fclose(fs);
}

//TODO : rendre le code + complexe avec un proto : entreeNomPrenomEtud(Etudiant * e)
Etudiant entreeNomPrenomEtud(Etudiant e){
    printf("\nNom : ");
    fgets(e.nom, sizeof(e.nom), stdin);
    e.nom[strlen(e.nom)-1]='\0';
    MajusculeChaine(e.nom);
    printf("Prenom : ");
    fgets(e.prenom, sizeof(e.prenom), stdin);
    e.prenom[strlen(e.prenom)-1]='\0';
    e.prenom[0] = toupper(e.prenom[0]);
    return e;
}

/**
    RECHERCHE FUNCTION
**/

int recherchCite(Etudiant **tabLogOccup, int nbLogOccup, int val)
{
    int i;
    for(i=0; i < nbLogOccup; i++)
        if(tabLogOccup[i]->numCham[2] > val)
            return i;
    return i;
}

int posInsertionEtudiantTab(Etudiant **tabEtu, int nbrEtud, Etudiant e) // Donne la position d'insertion de l'etudiant dans le tab tri�
{
    int i;
    for(i=0; i < nbrEtud; i++)
        if(strcmp(tabEtu[i]->nom, e.nom) > 0)
            return i;
    return i; //pour le mettre a la fin
}

Etudiant * getEtudiantById(Etudiant ** etudT, int nbe, int id){
    int i;
    for(i=0; i < nbe; i++){
        if(etudT[i]->idEtud == id){
            return etudT[i];
        }
    }
    return NULL;
}

//recherche un etudiant grace � son nom et pr�nom
Etudiant * rchEtudNomPrenom(Etudiant ** etudT, int nbE, char * nom, char * prenom){
    int i;
    for(i=0; i < nbE; i++){
        if(strcmp(etudT[i]->nom, nom) == 0 && strcmp(etudT[i]->prenom, prenom) == 0)
                return etudT[i];
    }
    return NULL;
}

int rchPosEtud(Etudiant ** tabEtu, int nbEt, Etudiant e)
{
    int i;
    for(i=0;i<nbEt;i++)
        if(e.idEtud == tabEtu[i]->idEtud)
            return i;
    return -1;
}

Etudiant etudExisteDeja(Etudiant e, Etudiant ** etudT, int nbE, int *rep){
    Etudiant * EtRech;

    EtRech = rchEtudNomPrenom(etudT, nbE, e.nom, e.prenom);
    if(EtRech == NULL)
    {
        *rep=0;
        return e; //�tudiant non trouv�
    }
    *rep=1;  //�tudiant trouv�
    return *EtRech; //retourne l'etudiant donn�
}

void MajusculeChaine(char chaine[]){
    int i;
    for(i=0; i < strlen(chaine); i ++)
    {
        chaine[i]=toupper(chaine[i]);
    }
}

/**
    CONNECTION FUNCTION
**/

Etudiant inscrireEtud(Etudiant e, Etudiant ** etudT, int * nbEtud){
    e = entreeEtudiant(e, etudT, *nbEtud);
    etudT = ajoutEtud(etudT, nbEtud, e);
    return e;
}

int loginEtudiant(Etudiant e){

    Date mdp;
    char newEssai;

    printf("\nMot de passe ? (Date de naissance : JJ MM AAAA) : ");
    scanf("%d %d %d%*c", &mdp.jour, &mdp.mois, &mdp.annee);

    while(mdp.jour != e.naissance.jour || mdp.mois != e.naissance.mois || mdp.annee != e.naissance.annee)
    {
        printf("Erreur de connection\nReessayer ? (O/N) : ");
        scanf("%c%*c", &newEssai);
        newEssai=toupper(newEssai); //pour mettre une lettre en MAJ
        while(newEssai != 'O' && newEssai != 'N'){
            printf("\nVeuillez saisir O ou N : ");
            scanf("%c%*c", &newEssai);
            newEssai=toupper(newEssai);
        }

        if(newEssai == 'N'){
            printf("Connexion annulee");
            return 0;
        }

        printf("\nMot de passe ? (Date de naissance : JJ MM AAAA) : ");
        scanf("%d %d %d%*c", &mdp.jour, &mdp.mois, &mdp.annee);
    }

    printf("\nConnexion reussie\n");

    return 1;
}


/**
    TRIS FUNCTION
**/

//Tri fusion
void triEtudEchelon(Etudiant *tEtud[], int nbEt)
{
    Etudiant **tab1, **tab2;
    int nb1, nb2, nb3;

    if(nbEt == 1) //si tab 1 �lem : fin recursivit� (tab tri�)
        return;

    nb1=nbEt/2;
    nb2=nbEt-nb1;

    tab1=(Etudiant **)malloc(sizeof(Etudiant *) * nb1);
    tab2=(Etudiant **)malloc(sizeof(Etudiant *) * nb2);
    if(tab1 == NULL ||tab2 == NULL)
        {printf("PB MALLOC\n"); exit(1);}

    separationTetud(tEtud, nbEt, tab1, nb1, tab2);

    triEtudEchelon(tab1, nb1);
    triEtudEchelon(tab2, nb2);

    nb3=interclassementTetud(tab1, nb1, tab2, nb2, tEtud);
    free(tab1);
    free(tab2);
    if(nb3 != nbEt)
        printf("PB tri fusion Etud par echelon\n");
}

void separationTetud(Etudiant *tEtud[], int nbEt, Etudiant *tab1[], int nb1, Etudiant *tab2[])
{
    int i,j;

    for(i=0; i < nb1 ; i++)
        tab1[i] = tEtud[i];
    for(j=0; i < nbEt ; j++, i++)
        tab2[j] = tEtud[i];
}

int interclassementTetud(Etudiant **tab1, int nb1, Etudiant **tab2, int nb2, Etudiant **tab3)
{
    int i,j,k;

    for(i=0, j=0, k=0; i < nb1 && j < nb2 ; k++)
    {
        if(tab1[i]->echelon <= tab2[j]->echelon)
        {
            tab3[k]=tab2[j];
            j++;
        }
        else
        {
            tab3[k]=tab1[i];
            i++;
        }
    }

    for(; i < nb1; i++, k++)
        tab3[k]=tab1[i];
    for(; j < nb2; j++, k++)
        tab3[k]=tab2[j];

    return nb1+nb2;
}

//Tri par selection
void triEtudNom(Etudiant *tEtud[], int nbEtu)
{
    int i, pos;
    for(i=0; i < nbEtu; i++)
    {
        pos=rechercheMinNom(tEtud, i, nbEtu);
        permutation(tEtud, i, pos);
    }
}

int rechercheMinNom(Etudiant *tEtud[], int debut, int nbEt)
{
    Etudiant *min;
    int i, pos;
    min=tEtud[debut];
    pos=debut;

    for(i=debut; i < nbEt; i++)
        if(strcmp(tEtud[i]->nom, min->nom) < 0)
            {
                min=tEtud[i];
                pos=i;
            }
    return pos;
}


//Tri les �tudiants par num�ro de cit� ( tri selection )
void triEtudLogCite(Etudiant *tabLogOccup[], int nbLogOccup)
{
    int i, pos;
    for(i=0; i < nbLogOccup; i++)
    {
        pos=rechercheMinNom(tabLogOccup, i, nbLogOccup);
        permutation(tabLogOccup, i, pos);
    }
}


int rechercheMinLog(Etudiant *tabLogOccup[], int debut, int nbLogOccup)
{
    Etudiant *min;
    int i, pos;
    min=tabLogOccup[debut];
    pos=debut;

    for(i=debut; i < nbLogOccup; i++)
        if(strcmp(tabLogOccup[i]->numCham, min->numCham) < 0)
            {
                min=tabLogOccup[i];
                pos=i;
            }
    return pos;
}

void permutation(Etudiant *tEtud[], int pos1, int pos2)
{
    Etudiant *val;
    val=tEtud[pos1];
    tEtud[pos1]=tEtud[pos2];
    tEtud[pos2]=val;
}

/**
    DEPART ETUD FUNCTION
**/

int EtdepartEtud(Etudiant **tabEtu, int nbEt, Logement tabL[], int nbLg, Etudiant user)
{
     char choix;
     int pos;

     printf("DEPART D'UN ETUDIANT\n\n");
     printf("Souhaitez-vous vraiment quitter nos cites ? Ce choix est irremediable \n");

     do{
         printf("O/N : ");
         scanf("%c%*c", &choix);
         choix=toupper(choix);
     }while(choix != 'O' && choix != 'N');
     if(choix == 'N')
        return nbEt;

    pos=rchPosEtud(tabEtu, nbEt, user);

    //passer la chambre au statut libre (1)
    modifierStatutLog(tabL, nbLg, user.numCham, 1);

    nbEt=suppressionEtud(tabEtu, nbEt, pos);

    return nbEt;

}

int suppressionEtud(Etudiant ** tabEtud, int nbEt, int pos){

    int i;

    free(tabEtud[pos]);

    for(i=pos; i < nbEt-1 ; i++)
        tabEtud[i]=tabEtud[i + 1];

    return nbEt-1;
}
