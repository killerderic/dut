#include "dem.h"

//Le nb de demandes n'est pas necessaires

ListeDem chargDemande(int *nbDem) //Fonction de chargement du fichier demande.don
{
    ListeDem lDem = NULL; //cr�ation de la liste;
    FILE *fe;
    int i;
    fe=fopen("donnees/demande.don", "r");
    if(fe == NULL){printf("Pb ouverture fichier demande.don\n"); exit(1);}

    fscanf(fe, "%d", nbDem);

    for(i=0; i < *nbDem; i++){
        lDem=inserEnTete(lDem, lireDemande(fe)); //element suivant dans l'emplacement suivant de la liste
    }
    fclose(fe);
    return lDem;
}

Demande lireDemande(FILE *fe) //lit une demande du fichier et renvoie le r�sultat
{
    Demande dem;

    fscanf(fe, "%d%*c", &dem.idEtud);
    fscanf(fe, "%s", dem.numLog);
    fscanf(fe, "%d%d%d", &dem.dateDem.jour, &dem.dateDem.mois, &dem.dateDem.annee);
    fscanf(fe, "%d%*c", &dem.statut);
    fscanf(fe, "%s%*c%*c", dem.type);

    return dem;
}
ListeDem suppr1Dem(ListeDem l, Demande dem)
{
    if(l == NULL)
        return l; //NULL
    if(l->elem.idEtud == dem.idEtud) // une Demande par etud donc idEtud discriminant
        return suppressionEnTete(l);
    l->restlist=suppr1Dem(l->restlist, dem);
    return l;
}
ListeDem suppressionEnTete(ListeDem l)
{
    ListeDem tete=l->restlist;
    free(l);
    return tete;
}

ListeDem inserEnTete(ListeDem lDem, Demande dem) //ins�re dem dans la t�te de la liste
{
    ListeDem templist = lDem; //sauve l'adresse de t�te de la liste

    lDem=(ListeDem) malloc(sizeof(MaillonDem)); //alloue un espace pour le maillon de demande � ins�rer
    if(lDem == NULL) {printf("Pb malloc liste demande\n"); exit(1);}

    lDem->elem=dem; //renseigne la demande dans le maillon qu'on ins�re

    lDem->restlist = templist; //positionne l'adresse de l'ancienne t�te de liste dans la nouvelle t�te de liste
    return lDem;
}

void affichDem(ListeDem lDem) //affiche une liste de demandes en partant de la queue de la liste
{
    if(lDem == NULL) //Si fin de liste
        return;
    affichDem(lDem->restlist); //se place en queue de liste

    affich1Dem(lDem->elem); //affiche les �l�ments en remontant la liste jusqu'� la t�te
    //c'est dans l'ordre du fichier car les �l�ments sont rang�s en partant de la fin dans la liste (1er �l�m fic --> fin liste ...)
}

void affich1Dem(Demande dem) // permet d'afficher 1 demande
{
    printf("\nId etudiant : %d\nLogement souhaite : ", dem.idEtud); //peut etre recup le nom et prenom de l'etudiant concern� grace � l'id
    if(strchr(dem.numLog, 'X') == NULL) //si veut un log particulier (si pas de X dans le numLog)
        printf("%s", dem.numLog);
    else if(strcmp(dem.type, "X") != 0) //si veut un type de logement
        printf("de type : %s", dem.type);
    else
        printf("Pas d'importance");
    printf("\nDate :");
    if(dem.dateDem.jour < 10)
        printf("0%d", dem.dateDem.jour);
    else
        printf("%d", dem.dateDem.jour);

    if(dem.dateDem.mois < 10)
        printf("/0%d/", dem.dateDem.mois);
    else
        printf("/%d/", dem.dateDem.mois);
    printf("%d\nStatut : ", dem.dateDem.annee);
    if(dem.statut == 0)
        printf("Pas de reponse\n\n");
    else if(dem.statut == 1){
        printf("En attente\n\n");
    }
}

void testDem(void)
{
    ListeDem listDem;
    int nbDem;

    listDem=chargDemande(&nbDem);

    printf("Nb de demandes : %d\n", nbDem);

    affichDem(listDem);

    printf("-----------------");
    //Test de la creation de demande
    Etudiant etud;
    etud.idEtud=56305;

    Logement log;
    strcpy(log.num, "c02B658");
    strcpy(log.type, "X");
    creeDemande(listDem, etud, log, &nbDem);

    affichDem(listDem);
}

Date getDateActuelle(){
    Date date;
    time_t secondes;//On cr�e un var secondes pouvant stocker une valeurs de temps
    struct tm tempsActuelle;//On cr�e la structure instant avec tm ( qui est la strcuture qui g�re le temps )
    time(&secondes);//On recup�re les secondes( qui se sont pass� depuis 1970 ) avec le fn time
    tempsActuelle = *localtime(&secondes);//on remplit la strcture tm avec la fn local time qui prend en entr�e les secondes recup�r�s. Atention localTime renvoie un pointeur.
    //On remplit avec les valeurs actuelles un objet date
    date.annee = tempsActuelle.tm_year+1900;
    date.jour = tempsActuelle.tm_mday;
    date.mois = tempsActuelle.tm_mon+1;

    //On retorne cette date
    return date;
}

/**
    Fonction pour r�cup des donn�es dans les demandes
*/

Etudiant ** getEtudInLDem(ListeDem lDem, Etudiant ** etudT, int nbe, int * nbeInListe, int * position){

    if(lDem == NULL){
        //Retourner un tableau de pointeur de la bonne taille
        Etudiant ** etudTL = (Etudiant **) malloc(sizeof(Etudiant *) * *nbeInListe);
        *position = *nbeInListe - 1;//Position est un compteur pour savoir a quel index du tableau positionner un etudiant
        if(etudTL == NULL){
            printf("Probl�me malloc \n");
        }
        return etudTL;
    }

    *nbeInListe += 1;//Un compteur qui sert a savoir la taille du tableau a cr�e a la fin de la r�cursivit�

    Etudiant ** etudTL = getEtudInLDem(lDem->restlist, etudT, nbe, nbeInListe, position);

    Etudiant * e = getEtudiantById(etudT, nbe, lDem->elem.idEtud);//R�cup�rer l'etudiant avec son id
    etudTL[*position] = e;
    *position = *position - 1;


    return etudTL;
}

/**
   Fonction pour get des demandes
*/

MaillonDem * getDemandeByEtudiantId(ListeDem lDem, int id){
    if(lDem == NULL){
        return NULL;
    }

    if(lDem->elem.idEtud == id){
        return lDem;
    }

    return getDemandeByEtudiantId(lDem->restlist, id);
}

/**
    Fonction pour afficher demande de l'etudiant connecter
**/

void affichDemEtudiant(ListeDem lDem, int id)
{
    lDem = getDemandeByEtudiantId(lDem, id);

    if(lDem == NULL)
        printf("\n\nVous n'avez fait aucune demande\n");
    else{
        printf("\n\nVoici votre demande : \n");
        affich1Dem(lDem->elem);
    }
}

/**
    Fonction pour mettre une demande en attente
**/

void mettreDemEnAttente(ListeDem lDem)
{
    int id;
    ListeDem x;
    char choix;

    printf("Mise en attente d'une demande \n");
    do
    {
        printf("Saisir l'id present dans le demande a mettre en attente\nID ? ");
        scanf("%d%*c", &id);
        x=getDemandeByEtudiantId(lDem, id);
        if(x == NULL)
            printf("Demande inconnue\n");
    }while(x == NULL);

    affich1Dem(x->elem);
    do
    {
        printf("Est-ce bien cette demande ? (O/N) : \n");
        scanf("%c%*c", &choix);
        choix=toupper(choix);
        if(choix != 'N' && choix != 'O')
            printf("Veuillez taper O ou N\n");
    }while(choix != 'O' && choix != 'N');

    if(choix == 'N')
        return;

    miseAttenteDem(lDem, x->elem);
    printf("Demande mise en attente\n");
}

void miseAttenteDem(ListeDem lDem, Demande dem)
{
    if(lDem == NULL)
        return;
    if(lDem->elem.idEtud == dem.idEtud)
    {
        lDem->elem.statut = 1;
        return;
    }
    miseAttenteDem(lDem->restlist, dem);
}

/**
    Fonction pour afficher demande par echelon.
**/

void affichDemEchelon(ListeDem lDem, Etudiant *tabEtud[], int nbEtud)
{
    DemEchelon *tabDemEchelon;
    int nbDem=0, i;

    tabDemEchelon=(DemEchelon *)malloc(nbEtud*sizeof(DemEchelon)); //Tout les etudiant ont une demande au maximum
    if(tabDemEchelon == NULL){printf("ERREUR ALLOCATION"); return;}

    tabDemEchelon=getTabDemEchelon(lDem, tabEtud, nbEtud, tabDemEchelon, &nbDem); // On r�cup�re dans un tableau les demandes et l'�chelon associe.

    triDemEchelon(tabDemEchelon, nbDem); //On tri par ordre d�croissant des echelons.

    for(i=0; i < nbDem; i++) //On affiche le tous.
    {
        affich1Dem(tabDemEchelon[i].dem);
    }
}

DemEchelon * getTabDemEchelon(ListeDem lDem, Etudiant *tabEtud[], int nbEtud, DemEchelon *tabDemEchelon, int *nbDem)
{
    if(lDem == NULL) return tabDemEchelon;
    int i;

    for(i=0; i < nbEtud; i++) //On parcours tout les etudiants.
    {
        if(lDem->elem.idEtud == tabEtud[i]->idEtud) //Si l'id de l'etudaint correspond � l'id de la demande :
        {
            tabDemEchelon[*nbDem].dem = lDem->elem; // On recupere la demande,
            tabDemEchelon[*nbDem].echelon = tabEtud[i]->echelon; //et on recupere l'echelon de l'etudiant.
            *nbDem = *nbDem+1;
        }
    }

    return getTabDemEchelon(lDem->restlist, tabEtud, nbEtud, tabDemEchelon, nbDem);
}

void triDemEchelon(DemEchelon tabDemEchelon[], int nbDem)
{
    int i, j;
    for(i=0; i < nbDem; i++)
    {
        for(j=nbDem-1; j > i; j--)
            if(tabDemEchelon[j-1].echelon < tabDemEchelon[j].echelon)
                permutationDem(tabDemEchelon, j, j-1);
    }
}

void permutationDem(DemEchelon tabDemEchelon[], int pos1, int pos2)
{
    DemEchelon val;
    val=tabDemEchelon[pos1];
    tabDemEchelon[pos1]=tabDemEchelon[pos2];
    tabDemEchelon[pos2]=val;
}

/**
    SAUVEGARDE
**/

void sauvegardeDem(ListeDem listeDem, FILE *fs)
{
    if(listeDem == NULL) return;

    sauvegardeDem(listeDem->restlist, fs);

    fprintf(fs, "%d\n", listeDem->elem.idEtud);
    fprintf(fs, "%s\n", listeDem->elem.numLog);
    fprintf(fs, "%d %d %d\n", listeDem->elem.dateDem.jour, listeDem->elem.dateDem.mois, listeDem->elem.dateDem.annee);
    fprintf(fs, "%d\n", listeDem->elem.statut);
    fprintf(fs, "%s\n\n", listeDem->elem.type);
}

/**
    EMISSION D'UNE DEMANDE
**/

ListeDem enregistrDemande(ListeDem lDem, Etudiant user, int *nbDem, Logement tabL[], int nbL)
{
    int choix;
    char citePref[4], typePref[10];
    Logement log;

    if(getDemandeByEtudiantId(lDem, user.idEtud) != NULL){//si il y a d�ja une demande pour cet �tudiant
        printf("Vous avez deja une demande en cours\nImpossible d'en faire une deuxieme\n");
        return lDem;}
    //demande de choix de cite
    entreeCitePrefMenu(citePref, 0);
    system("cls");
    //demande si veux faire une demande pour un log sp�cifique ou pour un type de log
    do
    {
        printf(" Votre demande concerne :\n");
        printf("\t1) Un type de logement (T1, T2, ..)\n");
        printf("\t2) Une chambre precise \n\n");
        printf(" Indiquer votre choix : ");
        scanf("%d%*c", &choix);

        switch(choix)
        {
        case 1:
            entreeTypeLogPrefMenu(typePref, 0);
            strcpy(log.type, typePref);
            strcpy(log.num, citePref);
            strcat(log.num, "X");
            lDem=creeDemande(lDem, user, log, nbDem);
            break;
        case 2:
            strcpy(log.num, choixLogAdapte(tabL, nbL, user, citePref));
            strcpy(log.type, "X");
            modifierCodeReservLog(tabL, nbL, log.num, 1); //mettre 1 dans reserve du log pour l'attrib auto
            lDem=creeDemande(lDem, user, log, nbDem);
            break;
        default :
           choix = -1;
           printf("\nVeuillez taper 1 ou 2\n");
           break;
        }
    }while(choix == -1);
    printf("\n\t\t\tDemande creer\n");
    return lDem;
}

ListeDem creeDemande(ListeDem lDem, Etudiant etud, Logement log, int * nbDemande){
    Demande dem;
    dem.idEtud=etud.idEtud;

    strcpy(dem.numLog, log.num);
    strcpy(dem.type, log.type);
    dem.statut = 0;//0 correspond a pas de reponse
    dem.dateDem = getDateActuelle();
    lDem = inserEnTete(lDem, dem);
    *nbDemande = *nbDemande+1;
    return lDem;
}

/**
    LIBERATION DE MEMOIRE
**/

ListeDem freeDem(ListeDem l)
{
    ListeDem tete=l;
    if(l == NULL)
        return l;
    l=freeDem(l->restlist);
    free(tete);
    return l; //=NULL
}
