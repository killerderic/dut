#include "cite.h"

void CiteMenu()
{
    Cite * tabCite;
    int nbrC;

    printf("\t\t\t\tLISTE DES CITES\n");
    tabCite=chargCite(&nbrC);
    affichTabCite(tabCite, nbrC);
}

Cite * chargCite(int *nbrC)
{
    Cite * tabCite, cite;
    FILE *fs;
    int i;

    fs = fopen("donnees/cite.don", "r");
    if(fs == NULL){printf("\nERREUR : OUVERTURE FICHIER\n"); exit(1);}

    fscanf(fs,"%d", nbrC);
    tabCite = (Cite *)malloc(*nbrC * sizeof(Cite));

    for(i=0; i < *nbrC; i++)
    {
        cite = LireCite(fs);
        tabCite[i] = cite;
    }

    fclose(fs);
    return tabCite;
}

Cite LireCite(FILE *fs)
{
    Cite c;
    fscanf(fs,"%*c%s%*c", c.num);
    fgets(c.nom, 42, fs);
    c.nom[strlen(c.nom)-1]= '\0';
    fscanf(fs, "%d%*c", &c.numRue);
    fgets(c.rue, 42, fs);
    c.rue[strlen(c.rue)-1]= '\0';
    fscanf(fs, "%d%*c", &c.codePostal);
    fgets(c.ville, 22, fs);
    c.ville[strlen(c.ville)-1]= '\0';
    fscanf(fs, "%s%*c", c.tel);

    return c;
}

void affichTabCite(Cite *tabCite, int nbrC)
{
    int i;

    for(i=0; i < nbrC; i++)
    {
        affich1Cite(tabCite[i]);
    }
}

void affich1Cite(Cite c)
{
    printf("\n\n Numero de la cite : %s", c.num);
    printf("\n Nom de la cite : %s", c.nom);
    printf("\n Adresse : %d %s %d %s", c.numRue, c.rue, c.codePostal, c.ville);
    printf("\n Telephone : %s", c.tel);
}
