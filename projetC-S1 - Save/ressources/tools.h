#ifndef TOOLS_H_INCLUDED
#define TOOLS_H_INCLUDED

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

typedef struct{
	int jour;
	int annee;
	int mois;
}Date;

/**
    STRUCTURES DEMANDES
**/
typedef struct
{
    int idEtud;
    char numLog[8]; // soit numLog si veut un log particulier (01A658) sinon X
    Date dateDem;
    int statut; // 0 : sans r�ponse / 1 : en attente
    char type[11]; //type de logement souhait� ou X si veut un logement particulier
}Demande;

typedef struct maillonDem
{
    Demande elem;                   //Valeur d'un �l�ment de la liste
    struct maillonDem * restlist;   //pointeur vers l'�l�ment suivant de la liste
}MaillonDem;

typedef MaillonDem * ListeDem; //type pointeur vers maillon = type de la liste

/**
    STRUCTURES LOGEMENTS
**/

typedef struct{
	char num[8];// EX: c01 A 693 -> numC Bati Num CH ( pas d'espace )
	int etat; // occup�e (0) ; libre (1)  ; en travaux (2)
	int reserveH; // chambre r�serv�e pour une personne handic : oui (1) ; non (0)
	char type[11];
	int reserve;// Pour savoir si le logement a d�j� �t� r�serv� par un �l�ve.
}Logement;

/**
    STRUCTURES ETUDIANTS
**/

typedef struct{
	int idEtud;
	char nom[42];
	char prenom[32];
	Date naissance;
	char tel[11];
	int handic; // oui (1) ; non (0)
	int echelon; //0 1 2 3 4 5 6 7
	char numCham[8];
}Etudiant;

/**
    STRUCTURES GERANTS
**/

typedef struct{
    char login[42];
    char password[42];
}Gerant;

/**
    STRUCTURES CITE
**/

typedef struct{
    char num[4]; //c01, c02, ..., c10
	char nom[42];
	int numRue;
	char rue[42];
	int codePostal;
	char ville[22];
	char tel[12];

}Cite;

void MajusculeChaine(char chaine[]);


#endif // TOOLS_H_INCLUDED
