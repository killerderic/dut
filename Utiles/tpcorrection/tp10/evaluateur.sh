#!/bin/bash

while [ 0 ]
do
    printf "\n*******************************\n"
    printf "Saissez une commande, commande <Q> pour quitter."
    printf "\n*******************************\n"
    printf "\n"

    read cmd || break
    
    if [ "$cmd" == "Q" ] ; then
        break
    fi
   
    eval "$cmd" 2> /dev/null; ret=$?
    
    if [ "$ret" -ne 0 ] ; then
        printf "*** cette commande a généré une erreur ***\n"
    fi

    printf "\n"
done
