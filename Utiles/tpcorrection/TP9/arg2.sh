#!/bin/bash
chemin=/
if [ $1 -z ]; then
    for fichier in `ls`; do
	if [ -d "$(pwd)/'fichier'" ]; then
	    echo "[$arg]"
	else if [ -x "$(pwd)/'fichier'" ]; then
		 echo "*[$arg]"
	     else echo $arg
	     fi
	fi
    done
fi
