#!/bin/bash

printf 'entrez le nom du fichier \n'
read nom
ls $nom -l | (cut -d "w" -f 1 || cut -d "-" -f 1) | grep -q r
if [ $? -eq 0 ]; then
	printf 'Le fichier est accessible en lecture \n'
	cat $nom
else printf 'erreur \n' >&2
fi

