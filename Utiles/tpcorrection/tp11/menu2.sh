#!/bin/bash

select choix in 'Afficher la date' 'Afficher le nombre de personnes connectées' 'Afficher la liste des processus' 'Quitter'
do
    case $choix in
        'Afficher la date') date ;;
	'Afficher le nombre de personnes connectées') who | wc -l ;;
	'Afficher la liste des processus') ps -axjf ;;
	'Quitter') exit 0 ;;
	*) echo "Choix invalide" ; exit 1 ;;
    esac
done

