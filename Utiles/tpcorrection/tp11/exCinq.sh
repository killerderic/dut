#!/bin/bash

processOneUser() {
	ps h -eo user | grep "^$1$" | wc -l
}


processAllUsers() {
	local L
	L="$(ps h -eo user | sort | uniq)"
	local user
	for user in $L; do
		nb=$(processOneUser "$user")
		echo "$user : $nb"
	done 
}

processAllUsers
