#!/bin/bash

if [ "$#" -ne 2 ]; then
	echo "erreur dans les arguments"
	exit 1
fi

option=$1
user=$2
if [ "$option" != '-ident' ]; then
	while [ "$option" != '-ident' ]; do
		echo "retappez l'argument 1"
		read option	
	done
	echo "donnez le nom de l'utilisateur"
	read user
fi
while [ 0 ]
	do
	for utilisateur in $(ps -aux | cut -d " " -f1 | sort | uniq); do
		if [ $utilisateur = $user ]; then
			echo "On a trouvé l'utilisateur"
			exit 0
		fi
	done
	printf "on cherche encore "
	echo $user
	sleep 1
done

