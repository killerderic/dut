#!/bin/bash

jour=$(date "+%d")
mois=$(date "+%m")
annee=$(date "+%y")
time=""
lang="fr"
fullYear="false"
addTime="false"

# *OPTIONS*
while [ "$#" -ne 0 ] ; do
    case "$1" in
        '-lang')
	    if [ "$2" != "" ] ; then
	    	lang="$2"
	    	shift 2
	    else
	        echo "Argument inexistant" >&2
		exit 1
	    fi
	    ;;
	'-fullYear')
            fullYear="true"
	    shift
	    ;;
	'-addTime')
            addTime="true"
	    shift
	    ;;
        *)
	    echo "Argument $1 inconnu" >&2
	    exit 1 
	    ;;
    esac
done

# *FULLYEAR*
if [ "$fullYear" = "true" ] ; then
    annee=$(date "+%Y")
fi

# *ADDTIME*
if [ "$addTime" = "true" ] ; then
    time=$(date "+%H:%M:%S")
fi

# *LANG*
if [ "$lang" = "fr" ] ; then
    echo "$jour/$mois/$annee $time"
elif [ "$lang" = "en" ] ; then
    echo "$annee/$mois/$jour $time"
else
    echo "Argument invalide" >&2
    exit 1
fi
